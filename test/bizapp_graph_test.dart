import 'dart:async';
import 'dart:math' show Random;

import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:bizapp/src/bizapp/common/orientDB.dart';
import 'package:bizapp/src/bizapp/controllers/OCategory.dart';
import 'package:bizapp/src/bizapp/controllers/OComment.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
import 'package:bizapp/src/bizapp/controllers/OLocation.dart';
import 'package:bizapp/src/bizapp/controllers/OMessage.dart';
import 'package:bizapp/src/bizapp/controllers/OPayment.dart';
import 'package:bizapp/src/bizapp/controllers/OPost.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
import 'package:bizapp/src/bizapp/controllers/OReview.dart';
import 'package:bizapp/src/bizapp/controllers/OStore.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/reducers/reducer.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:logging/logging.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import "package:test_api/test_api.dart";
//import 'dart:convert';

Future<Null> main() async {
  group("bizapp tests", () {
    var rn = new Random().nextInt(20000);

    StreamSubscription logSubscription;

    setUpAll(() async {
      Logger.root.level = Level.ALL;
      logSubscription = Logger.root.onRecord.listen(print);

      var app = new AppBase(className: "bizApp");

      /*var db = new OrientDB(
          appKey: "bizapp",
          appSecret: "kiazi",
          port: 80,
          url: "http://sleepy-gorge-79790.herokuapp.com");*/

       var db = new OrientDB(
          appKey: "bizapp",
          appSecret: "kiazi",
          port: 2480,
          url: "http://localhost",
          database: "bizapp");

      var initialState = new AppState();
      var store = new Store<AppState>(reducer,
          initialState: initialState,
          middleware: [LoggingMiddleware.printer()]);
      app.config(db: db, store: store);
      OrientDB.useApp = true;
      await AppBase.db.connect().then((val) {
        OrientDB.useApp = false;
      });
    });

    tearDownAll(() async {
      AppBase.db.close();
      AppBase.db.closeApp();
      await logSubscription.cancel();
    });

    group("vertex crude", () {
      var v = new V("V");
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");

        // user.logInfo(list);
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var list = await v.save(fields: {"field": "some value"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("update", () async {
        var updates = {
          "content": "new lorem ipsum here",
          "metadata": ["four"],
        };
        var list = await v.save(fields: updates);
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await v.reload();
      
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        print("record id after update is ${v.rid} with version ${v.version}");
        expect(v.getField("field"), equals("some value"));
        expect(v.getField("content"), equals("new lorem ipsum here"));
        expect(v.getField("metadata"), equals(["four"]));
      });

      test("delete", () async {
        var list = await v.delete();
        print(list);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(list.result[0]["count"], equals(1));
      });
    });
    group("edges crude", () {
      var e = new E("E")
        ..from = "(SELECT FROM OUser WHERE name='admin')"
        ..to = "(SELECT FROM OUser WHERE name='bizapp')";
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");

        // user.logInfo(list);
        print(list);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var list = await e.save(fields: {"field": "some value"});
        print(list);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("update", () async {
        var updates = {
          "content": "new lorem ipsum here",
          "metadata": ["four"],
          
        };
        var res = await e.save(fields: updates);
        
        
        print(res.result);
        expect(res.hasError, isFalse);
        expect(res.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await e.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);

        print("record id after update is ${e.rid} with version ${e.version}");
        expect(e.getField("content"), equals("new lorem ipsum here"));
        expect(e.getField("metadata"), equals(["four"]));
      });

      test("delete", () async {
        var list = await e.delete();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(list.result[0]["count"], equals(1));
      });
    });
    group("document crude", () {});
    group("index crude", () {});
    group("users actions", () {
      OUser user = new OUser();
      OProduct product = new OProduct();
      OStore store = new OStore();
      OPost post = new OPost();
      OComment comment = new OComment();
      OLocation location = new OLocation();
      OReview review = new OReview();
      OPayment payment = new OPayment();
      OMessage message = new OMessage();

      test("register", () async {
        var list = await user.register(
            username: "user_$rn", password: "kiazi", role: "admin");
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(user.rid, isNotNull);
      });
      test("login", () async {
        var list = await user.login(username: "user_$rn", password: "kiazi");
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(user.rid, isNotNull);
      });

      test("follow", () async {
        var list = await user.follow(username: "admin");

       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getfollowing", () async {
        var list = await user.getfollowing();

     print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getfollowers", () async {
        var list = await user
            .getfollowers(where: {"name": "admin", "status": "ACTIVE"});

        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("unfollow", () async {
        var list = await user.unfollow(username: "admin");
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(list.result[0]["count"], equals(1));
      });
      test("sell_product", () async {
        product.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user.sell_product(
            product: product, metadata: {"comment": "first sell"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getMyProducts", () async {
        var list = await user.getMyProducts();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("like_product", () async {
        var list =
            await user.like(item: product, metadata: {"comment": "first like"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getProducts", () async {
        var list = await user.getProducts();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getLikedProducts", () async {
        var list = await user.getLikedProducts();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("buy_product", () async {
        var list = await user
            .buy_product(product: product, metadata: {"comment": "first buy"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getPurchases", () async {
        var list = await user.getPurchases();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("view_product", () async {
        var list =
            await user.view(item: product, metadata: {"comment": "first buy"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getViewedProduct", () async {
        var list = await user.getViewedProducts();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("own_store", () async {
        store.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user
            .owns(store: store, metadata: {"comment": "first own store"});
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });

      test("like_store", () async {
        var list =
            await user.like(item: store, metadata: {"comment": "first like"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getLikedStores", () async {
        var list = await user.getLikedStores();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("view_store", () async {
        var list =
            await user.view(item: store, metadata: {"comment": "first buy"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getViewedStores", () async {
        var list = await user.getViewedStores();
    print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      
      test("post", () async {
        post.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user
            .post(item: post, metadata: {"comment": "first own store"});
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getMyPosts", () async {
        var list = await user.getMyPosts();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("like_post", () async {
        var list =
            await user.like(item: post, metadata: {"comment": "first like"});
     print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getLikedPosts", () async {
        var list = await user.getLikedPosts();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("view_post", () async {
        var list =
            await user.view(item: post, metadata: {"comment": "first buy"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getViewedPosts", () async {
        var list = await user.getViewedPosts();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("comment", () async {
        comment.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user.comment(
            comment: comment, metadata: {"comment": "first own store"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("like_comment", () async {
        var list =
            await user.like(item: comment, metadata: {"comment": "first like"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      //TODO get liked comments here
      test("view_comment", () async {
        var list =
            await user.view(item: comment, metadata: {"comment": "first like"});
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      //TODO get viewed comments here
      test("lives_in", () async {
        location.rawData = {
          "title": "xxxx",
          "content": "some content",
          "lat": 50.00,
          "lon": 12.58
        };
        var list = await user.lives_in(
            location: location, metadata: {"comment": "first own store"});
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getUserLocation", () async {
        var list = await user.getLocation();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("send_message", () async {
        message.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user.send_message(receiver:"writer",
            message: message, metadata: {"comment": "first own store"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getMessages", () async {
        var list = await user.getMessages();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("pay_for_product", () async {
        payment.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user
            .pay(payment: payment,item:product, metadata: {"comment": "first buy store"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("pay_for_store", () async {
        payment.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user
            .pay(payment: payment,item:store, metadata: {"comment": "first subscription store"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("review_store", () async {
        review.rawData = {"title": "xxxx", "content": "some content"};
        var list = await user
            .review(rid:store.rid,review: review, metadata: {"comment": "first own store"});
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("view_review", () async {
        var list = await user
            .view(item: review, metadata: {"comment": "first review view "});
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("like_review", () async {
        var list =
            await user.like(item: review, metadata: {"comment": "first review like"});
      print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getReviews", () async {
        var list = await user.getReviews(className: store.className,rid: store.rid);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
       test("getViewedReviews", () async {
        var list = await user.getViewedReviews();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("getLikedReviews", () async {
        var list = await user.getLikedReviews();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("cleanup", () async {
        var list = await Future.wait([
          product.delete(),
          store.delete(),
          post.delete(),
          comment.delete(),
          location.delete(),
          review.delete(),
          payment.delete(),
          message.delete(),
          user.delete(),
        ]);
       list.map((item){
        print(item.result);
        print(item.errors);
        expect(item.hasError, isFalse);
        expect(item.hasResult, isTrue);
        expect(item.result[0]["count"],equals(1));
        }
      );
      
      });

      test("logout", () async {
        var list = await user.logout();
        print(list);

        expect(list, isTrue);
        //expect(user.rid, isNotNull);
        //expect(user.getField("name"), equals("bizapp"));
      });
    });
    group("products actions", () {
      OProduct product = new OProduct();
      OCategory category = new OCategory(fields: {"categoryName": "electronics"});
      OLocation location =
          new OLocation(fields: {"name": "kibaha", "lat": 69.45, "lon": 89.43});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");

       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });

      test("create", () async {
        var body = {
          "name": "some product name",
          "price": rn * 1000,
          "author": "gUser.rid",
          "description": "lorem ipsum test"
        };
        product.rawData = body;

        var list = await product.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("update", () async {
        var list = await product.save(
            fields: {"description": "edited", "inStock": 500, "sold": 300});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await product.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);

        print(
            "product id after update is ${product.rid} with version ${product.version}");
        expect(product.getField("inStock"), equals(500));
        expect(product.getField("sold"), equals(300));
      });
      test("belong to", () async {
        var belongto = new OBelong();

        var list = await product
            .belongTo(category: category, metadata: {"field": "field1"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        belongto.rawData = list.result[0];
        expect(belongto.getField("field"), equals("field1"));
        print("product category added with rid ${belongto.rid}");
      });
      test("located at", () async {
        var loc = await location.save();
        print(loc.result);
        print(loc.errors);
        expect(loc.hasError, isFalse);
        expect(loc.hasResult, isTrue);

        var locatedAt = new OLocatedAt();
        //  ..from=product.rid
        // ..to=location.rid;
        var list = await product
            .locatedAt(location: location, metadata: {"field": "field1"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        locatedAt.rawData = list.result[0];
        expect(locatedAt.getField("field"), equals("field1"));
        print("product location added with rid ${locatedAt.rid}");
      });
      test("cleanup", () async {
        var list = await Future.wait([
          product.delete(),
          location.delete(),
          category.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("stores actions", () {
      OStore store = new OStore();
      OProduct product = new OProduct(fields: {"name": "some product name"});
      OCategory category = new OCategory(fields: {"categoryName": "electronics"});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "name": "joh store",
          "logo": "some link",
          "stafs": ["frank", "john", "asha"],
          "created": "2016-8-31"
        };
        store.rawData = fields;
        var list = await store.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(store.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "name": "new shop name",
          "products": 600,
          "sales": 7000,
        };
        var list = await store.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await store.reload();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        print(
            "record id after update is ${store.rid} with version ${store.version}");
        expect(store.getField("name"), equals("new shop name"));
        expect(store.getField("products"), equals(600));
        expect(store.getField("sales"), equals(7000));
      });
      test("stoesell", () async {
        var list = await store
            .sell_product(product: product, metadata: {"field": "field1"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        var sell = new OSell(fields: list.result[0]);
        expect(sell.getField("field"), equals("field1"));
        print(
            "Store sell product with rid ${product.rid}  with sell edge rid ${sell.rid}");
      });
      test("has category", () async {
        var cat = await category.save();
        print(cat.result);
        print(cat.errors);
        expect(cat.hasError, isFalse);
        expect(cat.hasResult, isTrue);

        var list =
            await store.has(category: category, metadata: {"field": "field1"});
      print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        var has = new OHas(fields: list.result[0]);
        expect(has.getField("field"), equals("field1"));
        print(
            "Store has category with rid ${category.rid}  with ohas edge rid ${has.rid}");
      });
      test("clean up", () async {
        var list = await Future.wait(
            [store.delete(), product.delete(), category.delete()]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("posts actions", () {
      OPost post = new OPost();
      OProduct product = new OProduct(fields: {"name": "some product name"});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "title": "some post name",
          "image": "some link",
          "metadata": ["one", "two", "three"],
          "created": "2016-10-31"
        };
        post.rawData = fields;
        var list = await post.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(post.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "title": "new post title",
          "featured": 400,
          "sold": 9000,
        };
        var list = await post.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await post.reload();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        print(
            "record id after update is ${post.rid} with version ${post.version}");
        expect(post.getField("title"), equals("new post title"));
        expect(post.getField("featured"), equals(400));
        expect(post.getField("sold"), equals(9000));
      });
      test("post of", () async {
        var prod = await product.save();
        print(prod);
        expect(prod, isNot(contains("errors")));

        var list =
            await post.postOf(item: product, metadata: {"field": "field1"});
        print(list);
        expect(list, isNot(contains("errors")));
        var postof = new OOf(fields: list.result[0]);
        expect(postof.getField("field"), equals("field1"));
        print(
            "post of product with rid ${product.rid}  with postof edge rid ${postof.rid}");
      });
      test("clean up", () async {
        var list = await Future.wait([
          post.delete(),
          product.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("comments actions", () {
      OComment comment = new OComment();
      OProduct product = new OProduct(fields: {"name": "some product name"});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "subject": "some comment subject",
          "image": "some link",
          "metadata": ["one", "two", "three"],
          "created": "2016-10-31"
        };
        comment.rawData = fields;
        var list = await comment.save();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(comment.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "subject": "new comment subject",
          "commented_on": "tuesday august",
        };
        var list = await comment.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await comment.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        comment.rawData = (list.result[0]);
        print(
            "record id after update is ${comment.rid} with version ${comment.version}");
        expect(comment.getField("subject"), equals("new comment subject"));
        expect(comment.getField("commented_on"), equals("tuesday august"));
        expect(comment.getField("created"), equals("2016-10-31"));
      });

      test("comment of", () async {
        var prod = await product.save();
        print(prod);
        expect(prod, isNot(contains("errors")));

        var list = await comment
            .commentOf(item: product, metadata: {"field": "field1"});
        print(list);
        expect(list, isNot(contains("errors")));
        var postof = new OOf(fields: list.result[0]);
        expect(postof.getField("field"), equals("field1"));
        print(
            "comment of product with rid ${product.rid}  with postof edge rid ${postof.rid}");
      });
      test("clean up", () async {
        var list = await Future.wait([
          comment.delete(),
          product.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });

    group("reviews actions", () {
      OReview review = new OReview();
      OProduct product = new OProduct(fields: {"name": "some product name"});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "name": "some location subject",
          "lat": 56.00,
          "lon": -24.78,
          "metadata": ["one", "two", "three"],
          "created": "2016-1-31"
        };
        review.rawData = fields;
        var list = await review.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        expect(review.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "lon": 0.00,
          "metadata": ["four"],
        };
        var list = await review.save(fields: updates);
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await review.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        print(
            "record id after update is ${review.rid} with version ${review.version}");
        expect(review.getField("lon"), equals(0.00));
        expect(review.getField("lat"), equals(56.00));
        expect(review.getField("metadata"), equals(["four"]));
      });

      test("review of", () async {
        var prod = await product.save();
        print(prod);
        expect(prod, isNot(contains("errors")));
        var list =
            await review.reviewOf(item: product, metadata: {"field": "field1"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        var postof = new OOf(fields: list.result[0]);
        expect(postof.getField("field"), equals("field1"));
        print(
            "comment of product with rid ${product.rid}  with postof edge rid ${postof.rid}");
      });
      test("clean up", () async {
        var list = await Future.wait([
          review.delete(),
          product.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("location actions", () {
      OLocation location = new OLocation();
      OProduct product = new OProduct(fields: {"name": "some product name"});
      setUpAll(() async {
        OUser user = new OUser();
        var list = await user.login(username: "admin", password: "admin");

        // user.logInfo(list);
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        OUser user = new OUser();
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "name": "some location subject",
          "lat": 56.00,
          "lon": -24.78,
          "metadata": ["one", "two", "three"],
          "created": "2016-1-31"
        };
        location.rawData = fields;
        var list = await location.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        location.rawData = (list.result[0]);
        expect(location.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "lon": 0.00,
          "metadata": ["four"],
        };
        var list = await location.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await location.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        location.rawData = (list.result[0]);
        print(
            "record id after update is ${location.rid} with version ${location.version}");
        expect(location.getField("lon"), equals(0.00));
        expect(location.getField("lat"), equals(56.00));
        expect(location.getField("metadata"), equals(["four"]));
      });

      test("location of", () async {
        var prod = await product.save();
        print(prod);
        expect(prod, isNot(contains("errors")));
        product.rawData = (prod.result[0]);
        var list = await location
            .locationOf(item: product, metadata: {"field": "field1"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        var postof = new OOf(fields: list.result[0]);
        expect(postof.getField("field"), equals("field1"));
        print(
            "location of product with rid ${product.rid}  with postof edge rid ${postof.rid}");
      });
      test("clean up", () async {
        var list = await Future.wait([
          location.delete(),
          product.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("message actions", () {
      OMessage message = new OMessage();

      setUpAll(() async {
        OUser user = new OUser();
        var list = await user.login(username: "admin", password: "admin");

        // user.logInfo(list);
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        OUser user = new OUser();
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "subject": "some message subject",
          "content": "lorem ipsum text here",
          "metadata": ["one", "two", "three"],
          "created": "2016-1-31"
        };
        message.rawData = fields;
        var list = await message.save();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        message.rawData = (list.result[0]);
        expect(message.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "content": "new lorem ipsum here",
          "metadata": ["four"],
        };
        var list = await message.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await message.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        message.rawData = (list.result[0]);
        print(
            "record id after update is ${message.rid} with version ${message.version}");
        expect(message.getField("subject"), equals("some message subject"));
        expect(message.getField("content"), equals("new lorem ipsum here"));
        expect(message.getField("metadata"), equals(["four"]));
      });

      test("message of", () async {
        var list = await message
            .messageOf(username: "writer", metadata: {"field": "field1"});
      print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        var postof = new OOf(fields: list.result[0]);
        expect(postof.getField("field"), equals("field1"));
        print(
            "user received a message with rid ${message.rid}  with postof edge rid ${postof.rid}");
      });
      test("clean up", () async {
        var list = await Future.wait([
          message.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("payments actions", () {
      OPayment payment = new OPayment();
      OStore store = new OStore(fields: {"name": "store name"});
      OProduct product = new OProduct(fields: {"name": "store name"});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");
        // user.logInfo(list);
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "subject": "some payment subject",
          "description": "lorem ipsum text here",
          "metadata": ["one", "two", "three"],
          "created": "2016-1-31"
        };
        payment.rawData = fields;
        var list = await payment.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        payment.rawData = (list.result[0]);
        expect(payment.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "description": "new lorem ipsum here",
          "metadata": ["four"],
        };
        var list = await payment.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await payment.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        payment.rawData = (list.result[0]);
        print(
            "record id after update is ${payment.rid} with version ${payment.version}");
        expect(payment.getField("description"), equals("new lorem ipsum here"));
        expect(payment.getField("metadata"), equals(["four"]));
      });

      test("payment of", () async {
        var prod = await product.save();
        print(prod);
        expect(prod, isNot(contains("errors")));
        var list = await payment.paymentOf(
            product: product, metadata: {"description": "payment description"});
        print(list);
        expect(list, isNot(contains("errors")));
        var paymentOf = new OPaymentOf(fields: list.result[0]);
        expect(
            paymentOf.getField("description"), equals("payment description"));
        print(
            "payment of product with rid ${product.rid}  with paymentOf edge rid ${paymentOf.rid}");
      });
      test("subscription of", () async {
        var prod = await store.save();
        print(prod);
        expect(prod, isNot(contains("errors")));
        store.rawData = (prod.result[0]);
        var list = await payment.subscriptionOf(
            store: store,
            metadata: {"description": "subscription description"});
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        var subscriptionOf = new OSubscriptionOf(fields: list.result[0]);
        expect(subscriptionOf.getField("description"),
            equals("subscription description"));
        print(
            "subscription of store with rid ${store.rid}  with subscriptionOf edge rid ${subscriptionOf.rid}");
      });

      test("clean up", () async {
        var list = await Future.wait([
          store.delete(),
          product.delete(),
          payment.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });
    group("categories actions", () {
      OCategory category = new OCategory(fields: {"categoryName": "electronics"});
      OCategory childCategory = new OCategory(fields: {"categoryName": "phones"});
      OUser user = new OUser();
      setUpAll(() async {
        var list = await user.login(username: "admin", password: "admin");

        // user.logInfo(list);
        print(list);
        expect(list, isNot(contains("errors")));
      });
      tearDownAll(() async {
        var list = await user.logout();
        print(list);
        // user.logInfo(list);
        expect(list, isTrue);
      });
      test("create", () async {
        var fields = {
          "categoryName": "some category subject",
          "description": "lorem ipsum text here",
          "metadata": ["one", "two", "three"],
          "created": "2016-1-31"
        };
        category.rawData = fields;
        var list = await category.save();
       print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        category.rawData = (list.result[0]);
        expect(category.rid, isNotNull);
      });
      test("update", () async {
        var updates = {
          "description": "new lorem ipsum here",
          "metadata": ["four"],
        };
        var list = await category.save(fields: updates);
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
      });
      test("reload record", () async {
        var list = await category.reload();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        category.rawData = (list.result[0]);
        print(
            "record id after update is ${category.rid} with version ${category.version}");
        expect(category.getField("categoryName"), equals("some category subject"));
        expect(
            category.getField("description"), equals("new lorem ipsum here"));
        expect(category.getField("metadata"), equals(["four"]));
      });

      test("child of", () async {
        var list = await childCategory.save();
        print(list.result);
        print(list.errors);
        expect(list.hasError, isFalse);
        expect(list.hasResult, isTrue);
        list = await childCategory.childOf(
            category: category,
            metadata: {"description": "description of relationships"});
        print(list);
        expect(list, isNot(contains("errors")));
        var childOf = new OChildOf(fields: list.result[0]);
        expect(childOf.getField("description"),
            equals("description of relationships"));
        print(
            "child category of category with rid ${category.rid}  with child of edge rid ${childOf.rid}");
      });

      test("clean up", () async {
        var list = await Future.wait([
          category.delete(),
          childCategory.delete(),
        ]);
        print(list);
        expect(list[0], isNot(contains("errors")));
      });
    });


  });


}

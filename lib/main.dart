//import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:bizapp/src/components/appComponent.dart';
//import 'package:flutter_redux/flutter_redux.dart';
import 'package:logging/logging.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:bizapp/src/bizapp/reducers/reducer.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:bizapp/src/bizapp/common/orientDB.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';

void main() async{
  final persistor = Persistor<AppState>(
     // version: 1,
      storage: FlutterStorage(key:"bizapp"),
      serializer: JsonSerializer<AppState>(AppState.fromJson),
      debug: true);

    final  log = Logger("Bizapp");
  

    var app = AppBase(className: "bizApp");
    
    var persistorState =AppState();// await persistor.load().catchError((e){print(e); return AppState();});
    OUser user=persistorState?.user;
    //var initialState =persistorState is AppState?persistorState: AppState();
    AppBase.store = Store<AppState>(reducer, initialState: persistorState, middleware: [
     // persistor.createMiddleware(),
      LoggingMiddleware<AppState>.printer(
          logger: log,
          formatter: (appState, action, time) => jsonEncode({
                "action":
                    action is Action ? action?.toJson() : action.toString(),
                "state": appState?.toJson(),
                "time": time?.toString()
              }))
    ]);

     /* var db = new OrientDB(
        appKey: "bizapp",
        appSecret: "kiazi",
        port: 80,
        url: "http://sleepy-gorge-79790.herokuapp.com");*/

        var db = new OrientDB(
        appKey: "bizapp",
        appSecret: "kiazi",
        database:"zbizapp",
       // port: 80,
       // url: "http://192.168.137.1",
       url:"http://178.128.161.9"
        );
    
   
    if(user.rid!=null){
      bool res=await db.addCredentials(username: user.getField("name"),password: persistorState.authString);
       //print("result is $res");
       
       if(res!=true){
         await user.logout();
       }
       else if(AppBase.store.state.products.length<1){
        await user.initialDataLoad();
       }
    }

    app.config(db: db, store: AppBase.store);

    
  runApp(new AppComponent());
}


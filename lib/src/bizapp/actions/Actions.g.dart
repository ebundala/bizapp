// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Actions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Action _$ActionFromJson(Map<String, dynamic> json) {
  return Action(type: json['type'] as String, data: json['data']);
}

Map<String, dynamic> _$ActionToJson(Action instance) =>
    <String, dynamic>{'type': instance.type, 'data': instance.data};

AppError _$AppErrorFromJson(Map<String, dynamic> json) {
  return AppError(
      type: json['type'],
      payload: json['payload'],
      message: json['message'] as String);
}

Map<String, dynamic> _$AppErrorToJson(AppError instance) => <String, dynamic>{
      'type': instance.type,
      'payload': instance.payload,
      'message': instance.message
    };

APP_ACTIONS _$APP_ACTIONSFromJson(Map<String, dynamic> json) {
  return APP_ACTIONS();
}

Map<String, dynamic> _$APP_ACTIONSToJson(APP_ACTIONS instance) =>
    <String, dynamic>{};

USER_ACTIONS _$USER_ACTIONSFromJson(Map<String, dynamic> json) {
  return USER_ACTIONS();
}

Map<String, dynamic> _$USER_ACTIONSToJson(USER_ACTIONS instance) =>
    <String, dynamic>{};

POST_ACTIONS _$POST_ACTIONSFromJson(Map<String, dynamic> json) {
  return POST_ACTIONS();
}

Map<String, dynamic> _$POST_ACTIONSToJson(POST_ACTIONS instance) =>
    <String, dynamic>{};

COMMENT_ACTIONS _$COMMENT_ACTIONSFromJson(Map<String, dynamic> json) {
  return COMMENT_ACTIONS();
}

Map<String, dynamic> _$COMMENT_ACTIONSToJson(COMMENT_ACTIONS instance) =>
    <String, dynamic>{};

LOCATION_ACTIONS _$LOCATION_ACTIONSFromJson(Map<String, dynamic> json) {
  return LOCATION_ACTIONS();
}

Map<String, dynamic> _$LOCATION_ACTIONSToJson(LOCATION_ACTIONS instance) =>
    <String, dynamic>{};

MESSAGE_ACTIONS _$MESSAGE_ACTIONSFromJson(Map<String, dynamic> json) {
  return MESSAGE_ACTIONS();
}

Map<String, dynamic> _$MESSAGE_ACTIONSToJson(MESSAGE_ACTIONS instance) =>
    <String, dynamic>{};

PAYMENT_ACTIONS _$PAYMENT_ACTIONSFromJson(Map<String, dynamic> json) {
  return PAYMENT_ACTIONS();
}

Map<String, dynamic> _$PAYMENT_ACTIONSToJson(PAYMENT_ACTIONS instance) =>
    <String, dynamic>{};

CATEGORY_ACTIONS _$CATEGORY_ACTIONSFromJson(Map<String, dynamic> json) {
  return CATEGORY_ACTIONS();
}

Map<String, dynamic> _$CATEGORY_ACTIONSToJson(CATEGORY_ACTIONS instance) =>
    <String, dynamic>{};

STORE_ACTIONS _$STORE_ACTIONSFromJson(Map<String, dynamic> json) {
  return STORE_ACTIONS();
}

Map<String, dynamic> _$STORE_ACTIONSToJson(STORE_ACTIONS instance) =>
    <String, dynamic>{};

REVIEW_ACTIONS _$REVIEW_ACTIONSFromJson(Map<String, dynamic> json) {
  return REVIEW_ACTIONS();
}

Map<String, dynamic> _$REVIEW_ACTIONSToJson(REVIEW_ACTIONS instance) =>
    <String, dynamic>{};

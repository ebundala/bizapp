// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AppBase.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppBase _$AppBaseFromJson(Map<String, dynamic> json) {
  return AppBase(className: json['className'] as String);
}

Map<String, dynamic> _$AppBaseToJson(AppBase instance) =>
    <String, dynamic>{'className': instance.className};

D _$DFromJson(Map<String, dynamic> json) {
  return D(json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$DToJson(D instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'rawData': instance.rawData,
      'data': instance.data
    };

V _$VFromJson(Map<String, dynamic> json) {
  return V(json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..rawData = json['rawData']
    ..query = (json['query'] as List)?.map((e) => e as String)?.toList()
    ..conditions =
        (json['conditions'] as List)?.map((e) => e as String)?.toList()
    ..from = json['from'] as String
    ..action = json['action'] as String
    ..arrWhere = json['arrWhere'] as Map<String, dynamic>
    ..arrOrWhere = json['arrOrWhere'] as Map<String, dynamic>
    ..arrLike = json['arrLike'] as Map<String, dynamic>
    ..first = json['first'] as int
    ..offset = json['offset'] as int
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$VToJson(V instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'rawData': instance.rawData,
      'query': instance.query,
      'conditions': instance.conditions,
      'from': instance.from,
      'action': instance.action,
      'arrWhere': instance.arrWhere,
      'arrOrWhere': instance.arrOrWhere,
      'arrLike': instance.arrLike,
      'first': instance.first,
      'offset': instance.offset,
      'data': instance.data
    };

E _$EFromJson(Map<String, dynamic> json) {
  return E(json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$EToJson(E instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

I _$IFromJson(Map<String, dynamic> json) {
  return I(json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$IToJson(I instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'rawData': instance.rawData,
      'data': instance.data
    };

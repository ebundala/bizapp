import 'dart:async';
import 'dart:convert';
import 'package:logging/logging.dart';
import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:redux/redux.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/bizapp/common/orientDB.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part './AppBase.g.dart';



@JsonSerializable()
class AppBase {
  factory AppBase.fromJson(Map<String, dynamic> json) =>
      _$AppBaseFromJson(json);

  Map<String, dynamic> toJson() => _$AppBaseToJson(this);
  static Logger log;
  final String className;
  static Store<AppState> store;
  static OrientDB db;
  AppBase({this.className}) {
    //AppBase.log = new Logger(className);
  }
  void config(
      {OrientDB db,
      Store<AppState> store,
      Logger logger,
      String appKey,
      String appSecret}) {
    if (db is OrientDB && db != null) AppBase.db = db;
    if (store != null) AppBase.store = store;
    if (logger != null) AppBase.log = logger;
  }

  void loading(bool state,{String msg}){
    dispatch(Action(
      type: state?APP_ACTIONS.LOADING:APP_ACTIONS.LOADING_COMPLETE,
      data: msg
    ));
  }

  void dispatch(Action action) {
    return AppBase.store.dispatch(action);
  }

  void logInfo(dynamic info) {
    log.info(info);
  }

  void logError(dynamic e, [actionType, stackTrace]) {
    log.severe('error encountered!', e, stackTrace);
   
  }

  bool useApp(bool state){
    OrientDB.useApp=state;
  }
  Future<ApiResponse> sqlCommand(Map cmd)async{
    return (await _command(cmd)).toList().then((res)=>ApiResponse(res))
    .then((res) {
          if (res.hasError) {
            throw res;
          }         
          return res;
        }
        );
  }
  Future<Stream> _command(Map cmd) async {
    return await AppBase.db.executeCommand(cmd: cmd).then((response) {
      return response.transform(json.decoder);
    });
  }
}

//DOCUMENT BASE CLASS
@JsonSerializable()
class D extends AppBase {
  set rid(rid) {
    data["@rid"] = rid;
  }

  get rid {
    return data["@rid"];
  }

  set type(type) {
    data["@type"] = type;
  }

  get type {
    return data["@type"];
  }

  set version(type) {
    data["@type"] = type;
  }

  get version {
    return data["@type"];
  }

  set className(name) {
    data["@class"] = name;
  }

  get className {
    return data["@class"];
  }

  set rawData(Map fields) {
    data.addAll(fields);
  }

  get rawData {
    return data;
  }

  Map data = new Map();
  D(className, {Map fields}) : super(className: className) {
    if (fields != null) data.addAll(fields);
    this.className = className;
  }

  Future<ApiResponse> delete({Map where}) async {
    var keys;
    var params = where;
    if (where != null) {
      keys = params.keys.map((key) => "$key=:$key");
    } else {
      params = {"rid": rid};
      keys = params.keys.map((key) => "@$key=:$key");
    }

    var paraNames = "${keys.join(' AND ')}";
    Map cmd = {
      "command": "DELETE $className WHERE $paraNames",
      "parameters": params
    };
    return sqlCommand(cmd);
    
  }

  Future<ApiResponse> save({Map fields}) async {
    if (fields != null) data?.addAll(fields);
    var keys = data.keys.map((key) => key != "@class" ? "$key=:$key" : "");
    var params = {};
    params.addAll(data);
    params.remove("@class");
    //print("$params\n${keys.join(" ")}");
    var paraNames = "${keys.join(',')}";
    paraNames = paraNames.substring(0, paraNames.length - 1);
    var par = "${keys.length > 0 ? "SET $paraNames" : ""}";
    Map cmd = {"command": "INSERT INTO $className $par", "parameters": params};
    return sqlCommand(cmd);
  }

  getField(String name) {
    return data ?? data[name];
  }

  String toJSON() {
    return json.encode(rawData);
  }

  D.fromJSON(Map json)
      : data = json,
        super(className: json['@class']);
}

//VERTEX BASE CLASS
@JsonSerializable()
class V extends AppBase {
  set rid(rid) {
    data["@rid"] = rid;
  }

  get rid {
    return data["@rid"];
  }

  set type(type) {
    data["@type"] = type;
  }

  get type {
    return data["@type"];
  }

  set version(type) {
    data["@version"] = type;
  }

  get version {
    return data["@version"];
  }

  set className(name) {
    data["@class"] = name;
  }

  get className {
    return data["@class"];
  }

  set rawData(fields) {
    data.addAll(fields);
  }

  get rawData {
    return data;
  }

  List<String> query = [];
  List<String> conditions = [];
  String from = "";
  String action = "";
  Map arrWhere = {}, arrOrWhere = {}, arrLike = {};
  int first = 0, offset = 10;
  Map data = new Map();

  V(className, {Map fields}) : super(className: className) {
    if (fields != null) data.addAll(fields);
    this.className = className;
  }
  getField(String name) {
    return rawData[name];
  }

  addFields(fields) {
    if(fields!=null)
    data.addAll(fields);
  }

  removeField(String name) {
    return data.remove(name);
  }

  String toJSON() {
    return json.encode(rawData);
  }

  V.fromJSON(Map json)
      : data = json,
        super(className: json['@class']);

  Future<ApiResponse> delete({Map where}) async {
    var keys;
    var params = where;
    if (where != null) {
      keys = params.keys.map((key) => "$key=:$key");
    } else {
      params = {"rid": rid};
      keys = params.keys.map((key) => "@$key=:$key");
    }

    var paraNames = "${keys.join(' AND ')}";
    Map cmd = {
      "command": "DELETE VERTEX $className WHERE $paraNames",
      "parameters": params
    };
    return sqlCommand(cmd).then((res) {
      if (res.hasResult) {
        rawData = res.result[0];
      }
      return res;
    });
  }

  Future<ApiResponse> save({Map fields}) async {
    if (fields != null) rawData = fields;
    var params = new Map();
    params.addAll(rawData);
    params.remove("@type");
    params.remove("@version");
    params.remove("@rid");
    params.remove("@class");
    Map cmd = new Map();
    String action = "CREATE VERTEX ";
    String operation = "SET";
    var keys = (params.keys.map((key) => "$key=:$key")).toList();

    if (rid != null && version != null) {
      action = "UPDATE ";
      operation = "MERGE";
      keys.add("updatedAt=sysdate('yyyy-MM-dd HH:mm:ss')");
    }else{
      keys.add("createdAt=sysdate('yyyy-MM-dd HH:mm:ss')");
    }

    var paraNames = "${keys.join(',')}";
    if (rid != null && version != null) {
      paraNames =
          "${json.encode(params)} WHERE @rid = $rid AND @version = $version";
    }
    var par = "${keys.length > 0 ? "$operation $paraNames" : ""}";
    cmd = {"command": "$action $className $par", "parameters": params};
    print("$cmd\n");
    return sqlCommand(cmd).then((res) {
      if (rid != null && version != null) {
        return reload();
      }
      if(res.hasResult){
      rawData = res.result[0];
      }
      return res;
    });
  }

  
  
 
 
  Future<ApiResponse> reload() async {
    Map cmd, params = {"rid": rid};
    cmd = {
      "command": "SELECT FROM $className WHERE @rid=:rid",
      "parameters": params
    };
    return sqlCommand(cmd).then((res) {
      if (res.hasResult) {
      rawData = res.result[0];
      }
      return res;
    });
  }

  V inE({String what}) {
    query.add("in($what)");

    return this;
  }

  V selectV() {
    action = "SELECT";
    return this;
  }

  V fromV({String what}) {
    from = what ?? className;
    return this;
  }

  V outE({String what}) {
    query.add("out($what)");
    return this;
  }

  V bothE({String what = ""}) {
    query.add("out($what)");
    return this;
  }

  V where(Map fields) {
    arrWhere = fields;
    return this;
  }

  V orWhere(Map fields) {
    arrOrWhere = fields;
    return this;
  }

  V likeV(Map fields) {
    arrLike = fields;
    return this;
  }

  V skip({int i = 0}) {
    first = i;
    return this;
  }

  V limit({int i = 10}) {
    offset = i;
    return this;
  }

  Future<ApiResponse> commit({bool expand = true}) async {
    String traversal = "";
    String sql = "", sqlWhere = "";
    Map cmd, params = {};
    List keys = [], orKeys = [], likeKeys = [];

    if (expand) {
      traversal = "expand(${query.join(".")})";
    } else {
      traversal = query.join(".");
    }
    if (arrWhere.isNotEmpty) {
      keys = (arrWhere.keys.map((key) => "$key=:$key")).toList();
      params.addAll(arrWhere);
    }
    if (arrOrWhere.isNotEmpty) {
      orKeys = (arrOrWhere.keys.map((key) => "$key=:$key")).toList();
      params.addAll(arrOrWhere);
    }
    if (arrLike.isNotEmpty) {
      likeKeys = (arrLike.keys.map((key) => "$key LIKE :$key")).toList();
      params.addAll(arrLike);
    }

    sqlWhere = orKeys.isNotEmpty
        ? orKeys.join(" OR ")
        : (keys.isNotEmpty
            ? keys.join(" AND ")
            : likeKeys.isNotEmpty ? likeKeys.join(" OR ") : "@rid=:rid");

    sql = "SELECT $traversal FROM ${from ?? className} WHERE $sqlWhere ";

    cmd = {"command": sql, "parameters": params};
    print(cmd);
    clear();
    return sqlCommand(cmd);
  }

  void clear() {
    query.clear();
    arrLike.clear();
    arrOrWhere.clear();
    arrWhere.clear();
    conditions.clear();
    first = 0;
    offset = 10;
  }

  

  Future<ApiResponse> match(String resultAs,
      {bool expand = true, String orderBy = ""}) async {
    var sql = "MATCH";
    // "{class: OUser, where:(name = 'admin')}.out('OFollow'){as: friends}.out('OFollow'){}.out('OLike'){as:likes} return EXPAND(likes) ORDER BY name SKIP 0 LIMIT 5";
    if (conditions.isNotEmpty) {
      var len = conditions.length;
      var qlen = query.length;
      for (var i = 0; i < len; i++) {
        var link = i < qlen ? '.${query[i]}' : "";
        sql = "$sql ${conditions[i]}$link";
      }
      resultAs = expand ? "EXPAND($resultAs) " : resultAs;
      var _orderBy = orderBy.isNotEmpty ? "ORDER BY $orderBy " : "";

      sql = "$sql return $resultAs $_orderBy SKIP $first LIMIT $offset";
      Map cmd = {"command": sql, "parameters": {}};
      print(cmd);
      clear();
      return sqlCommand(cmd).then((res) {        
        return res;
      });
    }
    return Future.value(throw ApiResponse([{"errors":{"content":"no conditions provided"}}]));
  }

  V condition(
      {String className = "",
      Map<String,String> where,
      String whileCondition = "",
      String asName = "",bool and=true}) {
    Map<String, String> condition = {};
    if(className.isNotEmpty) condition["class"] = className;
    if(where!=null){
      var keyValue=[];
      where.forEach((key,val){
          keyValue.add("$key = '$val'");
      });
     condition["where"]='( ${keyValue.join( "${and?' AND ':' OR '}" )})';
    }
    
    if(whileCondition.isNotEmpty)condition["while"] = whileCondition;
    if(asName.isNotEmpty)condition["as"] = asName;

    conditions.add(condition.toString());
    return this;
  }
}

//EDGE BASE CLASS
@JsonSerializable()
class E extends AppBase {
  set rid(rid) {
    data["@rid"] = rid;
  }

  get rid {
    return data["@rid"];
  }

  set type(type) {
    data["@type"] = type;
  }

  get type {
    return data["@type"];
  }

  set version(type) {
    data["@version"] = type;
  }

  get version {
    return data["@version"];
  }

  set className(name) {
    data["@class"] = name;
  }

  get className {
    return data["@class"];
  }

  set from(rid) {
    data["out"] = rid;
  }

  get from {
    return data["out"];
  }

  get to {
    return data["in"];
  }

  set to(rid) {
    data["in"] = rid;
  }

  set rawData(fields) {
    if(fields!=null)
    data.addAll(fields);
  }

  get rawData {
    return data;
  }

  Map data = new Map();
  E(className, {Map fields}) : super(className: className) {
    if (fields != null)
     data.addAll(fields);
    this.className = className;
  }

  Future<ApiResponse> delete({Map where}) async {
    List keys = new List();
    var params = new Map();
    if (where != null) {
      params.addAll(where);
      keys = (params.keys.map((key) => "$key=:$key")).toList();
    } else if (rid != null) {
      params = {"rid": rid};
      keys = (params.keys.map((key) => "@$key=:$key")).toList();
    }
    var paraNames = "${keys.join(' AND ')}";
    var par = "${keys.length > 0 ? "WHERE  $paraNames" : ""}";
    Map cmd = {
      "command": "DELETE EDGE $className FROM $from TO $to $par",
      "parameters": params
    };
    print(cmd);
    return sqlCommand(cmd).then((res) {
       if(res.hasResult){
      rawData = res.result[0];
       }

      return res;
    });
  }

  Future<ApiResponse> save({Map fields}) async {
    if (fields != null) rawData = fields;
    var params = new Map();
    params.addAll(rawData);
    params.remove("in");
    params.remove("out");
    params.remove("@type");
    params.remove("@version");
    params.remove("@rid");
    params.remove("@class");
    params.remove("@fieldTypes");

    Map cmd = new Map();
    String action = "CREATE EDGE ";
    String operation = "SET";
    var keys = (params.keys.map((key) => "$key=:$key")).toList();
    if (rid != null && version != null) {
      action = "UPDATE EDGE";
      operation = "MERGE";
     keys.add("updatedAt=sysdate('yyyy-MM-dd HH:mm:ss')");
    }else{
      keys.add("createdAt=sysdate('yyyy-MM-dd HH:mm:ss')");
    }

    var paraNames = "${keys.join(',')}";
    if (rid != null && version != null) {
      paraNames =
          "${json.encode(params)} WHERE  @version = $version"; // WHERE @rid = $rid AND @version = $version
    }
    var par = "${keys.length > 0 ? "$operation $paraNames" : ""}";
    cmd = {
      "command":
          "$action ${rid != null ? "$rid" : "$className FROM $from TO $to"} $par",
      "parameters": params
    };
    print("$cmd\n");
    return sqlCommand(cmd).then((res) {
      if (rid != null && version != null) {
        return reload();
      }
      if(res.hasResult){
      rawData = res.result[0];
      }
      return res;
    });
  }

  Future<ApiResponse> reload() async {
    Map cmd, params = {"rid": rid};
    cmd = {
      "command": "SELECT FROM $className WHERE @rid=:rid",
      "parameters": params
    };
    return sqlCommand(cmd).then((res) {
      if (res.hasResult) {
       
      rawData = res.result[0];
      }
      return res;
    });
  }

  getField(String name) {
    return rawData[name];
  }

  addFields(fields) {
    if(fields!=null)
    rawData.addAll(fields);
  }

  removeField(String name) {
    rawData.remove(name);
  }

  String toJSON() {
    return json.encode(rawData);
  }

  E.fromJSON(Map json)
      : data = json,
        super(className: json['@class']);
}

//INDEX BASE CLASS
@JsonSerializable()
class I extends AppBase {
  set rid(rid) {
    data["@rid"] = rid;
  }

  get rid {
    return data["@rid"];
  }

  set type(type) {
    data["@type"] = type;
  }

  get type {
    return data["@type"];
  }

  set version(type) {
    data["@version"] = type;
  }

  get version {
    return data["@version"];
  }

  set className(name) {
    data["@class"] = name;
  }

  get className {
    return data["@class"];
  }

  set rawData(fields) {
    data.addAll(fields);
  }

  get rawData {
    return data;
  }

  Map data = new Map();
  I(className, {Map fields}) : super(className: className) {
    if (fields != null) data.addAll(fields);
    this.className = className;
  }
  Future<ApiResponse> delete({Map where}) async {
    var keys;
    var params = where;
    if (where != null) {
      keys = params.keys.map((key) => "$key=:$key");
    } else {
      params = {"rid": rid};
      keys = params.keys.map((key) => "@$key=:$key");
    }

    var paraNames = "${keys.join(' AND ')}";
    Map cmd = {
      "command": "DELETE VERTEX $className WHERE $paraNames",
      "parameters": params
    };
    return sqlCommand(cmd);
    
  }

  Future<ApiResponse> save({Map fields}) async {
    if (fields != null) data?.addAll(fields);
    var keys = data.keys.map((key) => key != "@class" ? "$key=:$key" : "");
    var params = {};
    params.addAll(data);
    params.remove("@class");
    //print("$params\n${keys.join(" ")}");
    var paraNames = "${keys.join(',')}";
    paraNames = paraNames.substring(0, paraNames.length - 1);
    var par = "${keys.length > 0 ? "SET $paraNames" : ""}";
    Map cmd = {
      "command": "CREATE VERTEX $className $par",
      "parameters": params
    };
    return sqlCommand(cmd);
  }

  

  getField(String name) {
    return rawData[name];
  }

  addFields(fields) {
    data.addAll(fields);
  }

  removeField(String name) {
    return data.remove(name);
  }

  String toJSON() {
    return json.encode(rawData);
  }

  I.fromJSON(Map json)
      : data = json,
        super(className: json['@class']);
}

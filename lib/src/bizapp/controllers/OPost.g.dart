// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OPost.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OPost _$OPostFromJson(Map<String, dynamic> json) {
  return OPost(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..rawData = json['rawData']
    ..query = (json['query'] as List)?.map((e) => e as String)?.toList()
    ..conditions =
        (json['conditions'] as List)?.map((e) => e as String)?.toList()
    ..from = json['from'] as String
    ..action = json['action'] as String
    ..arrWhere = json['arrWhere'] as Map<String, dynamic>
    ..arrOrWhere = json['arrOrWhere'] as Map<String, dynamic>
    ..arrLike = json['arrLike'] as Map<String, dynamic>
    ..first = json['first'] as int
    ..offset = json['offset'] as int
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OPostToJson(OPost instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'rawData': instance.rawData,
      'query': instance.query,
      'conditions': instance.conditions,
      'from': instance.from,
      'action': instance.action,
      'arrWhere': instance.arrWhere,
      'arrOrWhere': instance.arrOrWhere,
      'arrLike': instance.arrLike,
      'first': instance.first,
      'offset': instance.offset,
      'data': instance.data
    };

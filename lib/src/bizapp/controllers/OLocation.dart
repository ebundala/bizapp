import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OLocation.g.dart';

@JsonSerializable()
class OLocation extends V{
  factory OLocation.fromJson(Map<String, dynamic> json) => _$OLocationFromJson(json);

    Map<String, dynamic> toJson() => _$OLocationToJson(this);
    OLocation({className:"OLocation",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> locationOf({@required V item ,Map metadata})async{
         if(item.rid==null){       
           var res= await item.save();
             
        }
        var post=(new OOf(fields: metadata))
                    ..from=rid
                    ..to=item.rid;
       return post.save();
      }
   
}
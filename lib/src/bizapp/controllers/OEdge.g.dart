// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OEdge.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OFollow _$OFollowFromJson(Map<String, dynamic> json) {
  return OFollow(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OFollowToJson(OFollow instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OLike _$OLikeFromJson(Map<String, dynamic> json) {
  return OLike(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OLikeToJson(OLike instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OReviewed _$OReviewedFromJson(Map<String, dynamic> json) {
  return OReviewed(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OReviewedToJson(OReviewed instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OView _$OViewFromJson(Map<String, dynamic> json) {
  return OView(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OViewToJson(OView instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OPublish _$OPublishFromJson(Map<String, dynamic> json) {
  return OPublish(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OPublishToJson(OPublish instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OOf _$OOfFromJson(Map<String, dynamic> json) {
  return OOf(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OOfToJson(OOf instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OLiveIn _$OLiveInFromJson(Map<String, dynamic> json) {
  return OLiveIn(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OLiveInToJson(OLiveIn instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OLocatedAt _$OLocatedAtFromJson(Map<String, dynamic> json) {
  return OLocatedAt(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OLocatedAtToJson(OLocatedAt instance) =>
    <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OOwn _$OOwnFromJson(Map<String, dynamic> json) {
  return OOwn(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OOwnToJson(OOwn instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OSell _$OSellFromJson(Map<String, dynamic> json) {
  return OSell(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OSellToJson(OSell instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OBuy _$OBuyFromJson(Map<String, dynamic> json) {
  return OBuy(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OBuyToJson(OBuy instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OPay _$OPayFromJson(Map<String, dynamic> json) {
  return OPay(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OPayToJson(OPay instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OPaymentOf _$OPaymentOfFromJson(Map<String, dynamic> json) {
  return OPaymentOf(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OPaymentOfToJson(OPaymentOf instance) =>
    <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OSubscriptionOf _$OSubscriptionOfFromJson(Map<String, dynamic> json) {
  return OSubscriptionOf(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OSubscriptionOfToJson(OSubscriptionOf instance) =>
    <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OBelong _$OBelongFromJson(Map<String, dynamic> json) {
  return OBelong(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OBelongToJson(OBelong instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OChildOf _$OChildOfFromJson(Map<String, dynamic> json) {
  return OChildOf(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OChildOfToJson(OChildOf instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OPartOf _$OPartOfFromJson(Map<String, dynamic> json) {
  return OPartOf(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OPartOfToJson(OPartOf instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

OHas _$OHasFromJson(Map<String, dynamic> json) {
  return OHas(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..from = json['from']
    ..to = json['to']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OHasToJson(OHas instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'from': instance.from,
      'to': instance.to,
      'rawData': instance.rawData,
      'data': instance.data
    };

//import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';

import 'package:json_annotation/json_annotation.dart';

part 'ODocument.g.dart';

@JsonSerializable()
class ODocument extends D{
  factory ODocument.fromJson(Map<String, dynamic> json) => _$ODocumentFromJson(json);

    Map<String, dynamic> toJson() => _$ODocumentToJson(this);
    ODocument({@required className,Map fields}):super(className,fields:fields);  

}
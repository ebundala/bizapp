import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OPost.g.dart';

@JsonSerializable()
class OPost extends V{
  factory OPost.fromJson(Map<String, dynamic> json) => _$OPostFromJson(json);

    Map<String, dynamic> toJson() => _$OPostToJson(this);

    OPost({className:"OPost",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> postOf({@required V item ,Map metadata})async{
         if(item.rid==null){       
           var res= await item.save();
             
        }
        var post=(new OOf(fields: metadata))
                    ..from=rid
                    ..to=item.rid;
       return post.save();
      }


}
//import 'dart:async';
//import 'dart:convert';
//import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';

import 'package:json_annotation/json_annotation.dart';

part 'OIndex.g.dart';

@JsonSerializable()
class OIndex extends I{
  factory OIndex.fromJson(Map<String, dynamic> json) => _$OIndexFromJson(json);

    Map<String, dynamic> toJson() => _$OIndexToJson(this);
    OIndex({className:"OIndex",Map fields}):super(className,fields:fields);  
}
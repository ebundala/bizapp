import 'dart:async';

import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
//import 'package:uuid/aes.dart';
import './OCategory.dart';
import './OComment.dart';
import './OEdge.dart';
import './OLocation.dart';
import './OMessage.dart';
import './OPayment.dart';
import './OProduct.dart';
import './OReview.dart';
import './OStore.dart';
import './OPost.dart';
import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:bizapp/src/bizapp/common/orientDB.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OUser.g.dart';
//import 'dart:convert';

@JsonSerializable()
class OUser extends V {
  factory OUser.fromJson(Map<String, dynamic> json) => _$OUserFromJson(json);

  Map<String, dynamic> toJson() => _$OUserToJson(this);

  OUser({className: "OUser", Map<String, dynamic> fields})
      : super(className, fields: fields);

  Future<ApiResponse> register(
      {@required String username,
      @required String password,
      String role}) async {
    Map cmd = {
      "command":
          "CREATE USER $username IDENTIFIED BY $password ROLE ${role.isNotEmpty ? role : 'reader'}",
    };
    OrientDB.useApp = true;
    return sqlCommand(cmd).then((res) {
      OrientDB.useApp = false;      
      if(res.hasResult){
      rawData = res.result[0];
      }
      dispatch(Action(
          type: USER_ACTIONS.REGISTER,
          data: {"user": this, "authkey": password}));
      return res;
    }).catchError((e){
       OrientDB.useApp = false;  
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.REGISTER,
                message: e is ApiResponse?e.hasError?e.errorMessage:"unknown error":"failed to register",
                payload: e is ApiResponse?e.toString():null
                )));
        throw e;
      
    });
  }

  Future<ApiResponse> delete_user({Map where}) async {
    return delete(where: where).then((value) {
      
      dispatch(Action(type: USER_ACTIONS.DELETE_USER, data: value.result[0]));

      return value;
    }).catchError((e){
      if (e.hasError) {
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.DELETE_USER,
                message: e is ApiResponse?e.hasError?e.errorMessage:"unknown error":"failed to register",
                payload: e is ApiResponse?e.toString():null
                )));
        return e;
      }
    });
  }

  Future<ApiResponse> login(
      {@required String username,
      @required String password,
      }) async {
    return AppBase.db.login(username: username, password: password)        
        .then((status) async {
          
      if (status.hasError) {        
        throw status;
      }
      var sql= """
         select *, 
          null as password,
          roles.name as roles,
          out_OLiveIn.in.country[0] as country,
          out_OLiveIn.in.region[0] as region,
          out_OLiveIn.in.district[0] as district,
          out_OLiveIn.in.ward[0] as ward,
          out_OLiveIn.in.street[0] as street,
          out_OLiveIn.in.lat[0] as latitude,
          out_OLiveIn.in.lon[0] as longitude,
          out_OLiveIn.in.alt[0] as altitude,
          out_OLiveIn.in.zip[0] as zip
          from OUser where name=:username
        """;

      var cmd = {
        "command":sql,
        "parameters": {"username":username}
      };
      return sqlCommand(cmd).then((res) {
        
        if (res.hasResult) {
        rawData = res.result[0];
        }
        else{
          throw res;
        }
        
        dispatch(Action(
            type: USER_ACTIONS.LOGIN,
            data: {"user": this, "authkey": password}));
        return res;
      });
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.LOGIN,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"Invalid password or Username",
                payload: e is ApiResponse?e.toString():null
                )));

                throw e;
    });
  }

  Future<bool> logout() async {
    var val = await AppBase.db.logout();
    if (val) dispatch(Action(type: USER_ACTIONS.LOGOUT));
    return Future.value(val);
  }

  Future<ApiResponse> follow({@required String username, Map fields}) async {
    var follow = (new OFollow(fields: fields))
      ..from = rid
      ..to = "(SELECT * FROM OUser WHERE name='$username')";
    return follow.save().then((value) {
      dispatch(Action(type: USER_ACTIONS.FOLLOW, data: value.result[0]));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.FOLLOW,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to follow $username",
                payload: e is ApiResponse? e:null
                )));
                throw e;
    });
  }

  Future<ApiResponse> unfollow({@required String username}) {
    var follow = (new OFollow())
      ..from = rid
      ..to = "(SELECT FROM OUser WHERE name='$username')";
    print("rid=$rid from=${follow.from} to=${follow.to}");
    return follow.delete().then((value) {
      dispatch(Action(type: USER_ACTIONS.UNFOLLOW, data: value.result[0]));
      return value;
    }).catchError((e){
       dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.UNFOLLOW,
                message:e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"): "failed to unfollow $username",
                payload:  e is ApiResponse? e:null
                )));
                throw e;
    });
  }

  Future<ApiResponse> like({@required V item, Map metadata}) async {
    if (item.rid == null) {
      throw (item);
    }
    var like = (new OLike(fields: metadata))
      ..from = rid
      ..to = item.rid;
    return like.save().then((value) {
      return value;
    });
  }

  Future<ApiResponse> view({@required V item, Map metadata}) async {
   return Future((){ 
     if (item.rid == null) {
      throw (item);
    }
    var view = (new OView(fields: metadata))
      ..from = rid
      ..to = item.rid;
    return view.save();
    }).then((value) {
      
      dispatch(Action(type: USER_ACTIONS.VIEW, data: value));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.REGISTER,
                message:e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"): "failed to view ${item.className}",
                payload: e is ApiResponse?e.toString():null)));
        throw e;
      
    });
  }

  Future<ApiResponse> sell_product({@required OProduct product, Map metadata}) async {
    var res;
    if (product.rid == null) {
      res = await product.save();
    }
    return Future((){
      if (res!=null&&res.hasError) {
        throw (res);
      }

    var sell = (new OSell(fields: metadata))
      ..from = rid
      ..to = product.rid;
    return sell.save();
    })
    .then((value) {
      
      dispatch(Action(type: USER_ACTIONS.SELL_PRODUCT, data: value));
      return value;
    }).catchError((e){
    dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.SELL_PRODUCT,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to sell product",
                payload: e is ApiResponse? e: null
                )));
        throw e;
    });
  }

  Future<ApiResponse> buy_product({@required OProduct product, Map metadata}) async {
   
   return Future((){
     if (product.rid == null) {
      throw (product);
    }
    var buy = (new OBuy(fields: metadata))
      ..from = rid
      ..to = product.rid;
    return buy.save();
   }).then((value) {
     
     dispatch(Action(type: USER_ACTIONS.BUY, data: value));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.BUY,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to buy",
                payload: e is ApiResponse?e.toString():null,)));
                throw e;
    });
    
  }

  Future<ApiResponse> owns({@required OStore store, Map metadata}) async {
    if (store.rid == null) {
      var res = await store.save();
      if (res.hasError) {
        throw (res);
      }
    }
    var own = (new OOwn(fields: metadata))
      ..from = rid
      ..to = store.rid;
    return own.save().then((value) {
      
      dispatch(Action(type: USER_ACTIONS.OWN_STORE, data: value));

      return value;
    }).catchError((e){

        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.OWN_STORE,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to create store",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
    });
  }

  Future<ApiResponse> post({@required V item, Map metadata}) async {
    if (item.rid == null) {
      var res = await item.save();
      if (res.hasError) {
        throw (res);
      }
    }
    var post = (new OPublish(fields: metadata))
      ..from = rid
      ..to = item.rid;
    return post.save().then((value) {      
      dispatch(Action(type: USER_ACTIONS.POST, data: value));
      return value;
    }).catchError((e){    
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.POST,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to post",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
    });
  }

  Future<ApiResponse> comment({@required OComment comment, Map metadata}) async {
    if (comment.rid == null) {
      var res = await comment.save();
      if (res.hasError) {
        throw (res);
      }
    }
    var post = (new OPublish(fields: metadata))
      ..from = rid
      ..to = comment.rid;
    return post.save().then((value) {      
      dispatch(Action(type: USER_ACTIONS.COMMENT, data: value));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.COMMENT,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to comment",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
    });
  }

  Future<ApiResponse> lives_in({@required OLocation location, Map metadata}) async {
    if (location.rid == null) {
      var res = await location.save();
      if (res.hasError) {
        throw (res);
      }
    }
    var livein = (new OLiveIn(fields: metadata))
      ..from = rid
      ..to = location.rid;
    return livein.save().then((value) {
     
      dispatch(Action(type: USER_ACTIONS.LIVES_IN, data: value));
      return value;
    }).catchError((e){
       
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.LIVES_IN,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to set location",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
      
    });
  }

  Future<ApiResponse> send_message({@required String receiver,@required OMessage message, Map metadata}) async {
    
    return message.save().then((value){
      return value;
     }).then((value){
    var post = (new OOf(fields: metadata))
      ..from = rid
      ..to = message.rid;
    return post.save();
      }).then((value){
        var send = (new OOf(fields: metadata))
        ..from=message.rid
        ..to="(SELECT FROM OUser WHERE name='$receiver')";
       return send.save();
      
    }).then((value){
      dispatch(Action(type: USER_ACTIONS.SEND_MESSAGE, data: value));
      return value;
    }).catchError((e){
      print(e);
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.SEND_MESSAGE,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to send message",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
    });
    
    
  }

  Future<ApiResponse> pay({@required OPayment payment,@required item, Map metadata}) async {
    
    return payment.save().then((res){
     
    var pay = (new OPay(fields: metadata))
      ..from = rid
      ..to = payment.rid;
    return pay.save();
    }).then((value) {
      if(item is OProduct){
    var productPayment= (new OPaymentOf(fields: metadata))..from=payment.rid..to=item.rid;
           return productPayment.save().then((res){
            
             return value;
           });
      }
      if(item is OStore){
       var storeSubscription= (new OSubscriptionOf(fields: metadata))..from=payment.rid..to=item.rid;
           return storeSubscription.save().then((sub){
            
             return value;
           });
      }
      else{
       throw value;
      }
      
    }).then((value){
     
      dispatch(Action(type: USER_ACTIONS.PAY, data: value));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.PAY,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to pay",
                payload: e is ApiResponse?e.toString():null,)));
                //clean up after ourselves
          return payment.delete().then((res){
             return e; 
        }).catchError((res)=>throw res);
    });
  }

  Future<ApiResponse> review({@required OReview review,@required String rid, Map metadata}) async {
   
 return review.save().then((res){
 
      return res;
 }).then((value){
   var post = (new OPublish(fields: metadata))
      ..from = this.rid
      ..to = review.rid;
      return post.save();
 }).then((value){
   
   var oof=(new OOf(fields: metadata))..from=review.rid..to=rid;
   return oof.save();
 }).then((value) {
      
      dispatch(Action(type: USER_ACTIONS.REVIEW_STORE, data: value));
      return value;
 }).catchError((e){
    dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.REVIEW_STORE,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to review ",
                payload: e is ApiResponse?e.toString():null,)));
                throw e;
 });
     
   
  }

  Future<ApiResponse> getfollowers({Map<String,String> where}) async {

    var condition = where ?? {"name": getField("name")};
    
    this.clear();
    return this
        .condition(
            className: className, where: condition)
        .inE(what: "OFollow")
        .condition(asName: "followers")
        .match("followers")
        .then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_FOLLOWERS,
          data: value.result.map<OUser>((item) => OUser(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_FOLLOWERS,
                message:e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"): "failed to get follower",
                payload: e is ApiResponse?e.toString():null,)));
        return e;
      
    });
  }

  Future<ApiResponse> getfollowing({Map<String,String> where}) async {
    var condition = where ?? {"name": getField("name")};
    this.clear();
    return this
        .condition(
            className: className, where: condition)
        .outE(what: "OFollow")
        .condition(asName: "following")
        .match("following")
        .then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_FOLLOWINGS,
          data: value.result.map<OUser>((item) => OUser(fields:item)).toList()));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_FOLLOWINGS,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get following",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
    });
  }

  Future<List>initialDataLoad()async{
    //todo switch to future wait;
   return  Future.wait([getCategories(),
     getProducts(),
     getMessages(),
     //getLocation(),
     getPeopleToFollow(),    
     getLikesFeed(),
     getFollowsFeed()
     ]);
    
    
  }
   /**
    * method to get trending products amongsts people am following
    */
  Future<ApiResponse> getProducts({int skip=0,limit=5,bool isInitial=true}) async {
    var sql="""select 
        Distinct(@rid) as rid,*
        from
        ( select *,
        in_OSell.out.name[0] as username,
        in_OSell.out.avator[0] as avator,
        in_OSell.out.roles[0] as roles,
        in_OSell.out.status[0] as status,
          out_OBelong.in.@rid as categoryId,
          out_OBelong.in.categoryName as categoryName,
        in_OLike.out.name as likes,
        in_OLike.out[@rid=:rid].size() as liked,
        in_OView.out[@rid=:rid].size() as viewed,
        in_OBuy.out[@rid=:rid].size() as bought,
        in_OSell.out[@rid=:rid].size() as selling,
        out_OLocatedAt.in.region[0] as region,
        out_OLocatedAt.in.district[0] as district,
        out_OLocatedAt.in.street[0] as street,
        out_OLocatedAt.in.ward[0] as ward,
        out_OLocatedAt.in.alt[0] as altitude,
        out_OLocatedAt.in.lat[0] as latitude,
        out_OLocatedAt.in.lon[0] as longitude
        from
        (select expand(inE("OSell").in)  from OProduct ORDER BY in_OLike.size() desc )
        )
        SKIP :skip LIMIT :limit""";
    return  sqlCommand(
      {
        "command":sql,
        "parameters":{"rid":rid,"skip":skip,"limit":limit}
    }).then((value) {
      dispatch(Action(
          type: isInitial?USER_ACTIONS.GET_INITIAL_PRODUCTS:USER_ACTIONS.GET_PRODUCTS,
          data: value.result.map<OProduct>((item) => OProduct(fields:item)).toList()));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: isInitial?USER_ACTIONS.GET_INITIAL_PRODUCTS:USER_ACTIONS.GET_PRODUCTS,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get products",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
     
    });
  }

/**
 * @Description  get products i am selling
 */

Future<ApiResponse> getMyProducts({Map<String,String> where,int skip=0,limit=10}) async {
    
   
    return 
    sqlCommand(
      {
        "command":"select expand(inE('OSell')[out=$rid].in) from OProduct skip:skip limit:limit",
        "parameters":{"skip":skip,"limit":limit}

    }).then((value) {      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_PRODUCTS,
          data: value.result.map<OProduct>((item) => OProduct(fields:item)).toList()));
      return value;
    }).catchError((e){      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_PRODUCTS,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get my products",
                payload: e is ApiResponse?e.toString():null,)));
        throw e;
     
    });
  }


/**
 * @description get categories and order them by views
 * 
 */
  Future<ApiResponse> getCategories({Map<String,String> where,int skip=0,int limit=10,bool isInitial=true}) async {
    var condition = where ?? {};
    this.clear();
    
    return this.condition(className:"OCategory",asName:"categories")
    .skip(i:skip).limit(i:limit)
    .match("categories",orderBy: "categoryName")
        .then((value) {
      print(value);
     
      dispatch(Action(
          type:isInitial?USER_ACTIONS.GET_INITIAL_CATEGORIES: USER_ACTIONS.GET_CATEGORIES,
          data: value.result.map<OCategory>((item) => OCategory(fields:item)).toList()));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type:isInitial?USER_ACTIONS.GET_INITIAL_CATEGORIES: USER_ACTIONS.GET_CATEGORIES,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get categories",
                payload: e is ApiResponse?e.toString():null,)));
        return e;
    });
  }


/**
 * @description get users incomming messages 
 * 
 */
  Future<ApiResponse> getMessages({Map<String,String> where,int skip=0,int limit=250,bool isInitial=true}) async {
    var condition = {"name":getField("name") as String,"status":"ACTIVE"};
    //if(where!=null)condition.addAll(where);
    this.clear();
    return this.condition(className: className,where: condition)
    .inE(what:"OOf").condition(className: "OMessage",asName: "messages")
    .match("messages",orderBy:"createdAt").then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MESSAGES,
          data: value.result.map<OMessage>((item) => OMessage(fields:item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MESSAGES,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get messages",
                payload: e is ApiResponse?e.toString():null,)));
        return e;
      
    });
  }

  Future<ApiResponse> getSentMessages({Map<String,String> where}) async {
    var condition = {"name":getField("name"),"status":"ACTIVE"}
    ..addAll(where);
    this.clear();
    return this.condition(className: className,where: condition)
    .inE(what:"OPublish").condition(className: "OMessage",asName: "messages")
    .match("messages",orderBy:"createdAt").then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MESSAGES,
          data: value.result.map<OMessage>((item) => OMessage(fields:item)).toList()));
      return value;
    }).catchError((e){ 
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MESSAGES,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get items",
                payload: e is ApiResponse?e.toString():null,)));
        return e;
    });
  }
  //Future<List> getShoppingCart({Map where}) async {}

  /**
   * @description get recommended users to follow based on interests
   * 
   */
  Future<ApiResponse> getPeopleToFollow(
      { int skip=0,int limit=250,bool isInitial=true}) async {
  
    var sql=
    """
    select 
    distinct(@rid),
    @rid,
    name,
    null as password,
          roles.name as roles,
          out_OLiveIn.in.country[0] as country,
          out_OLiveIn.in.region[0] as region,
          out_OLiveIn.in.district[0] as district,
          out_OLiveIn.in.ward[0] as ward,
          out_OLiveIn.in.street[0] as street,
          out_OLiveIn.in.lat[0] as latitude,
          out_OLiveIn.in.lon[0] as longitude,
          out_OLiveIn.in.alt[0] as altitude,
          out_OLiveIn.in.zip[0] as zip
    from (          
    select expand(outE("OFollow")[out=:rid].in.inE("OFollow")[out<>:rid].out[name NOT IN \$f]) 
    from OUser
    LET \$f= (select name from (select expand(outE("OFollow")[out=:rid].in) from OUser))
    ) SKIP :skip LIMIT :limit
    """;
    Map cmd={
      "command":sql,
      "parameters":{"skip":skip,"limit":limit,"rid":rid}
    };
   return
    sqlCommand(cmd)
    .then((value) {
      dispatch(Action(
          type: isInitial?USER_ACTIONS.GET_INITIAL_PEOPLE_TO_FOLLOW:USER_ACTIONS.GET_PEOPLE_TO_FOLLOW,
          data: value.result.map<OUser>((item) => OUser(fields:item)).toList()));
      return value;
    }).catchError((e){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
          type: isInitial?USER_ACTIONS.GET_INITIAL_PEOPLE_TO_FOLLOW:USER_ACTIONS.GET_PEOPLE_TO_FOLLOW,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get people to follow",
                payload: e is ApiResponse?e.toString():null,)));
                throw e;
    });
  }


/**
 * @description get popular posts 
 * 
 * 
 */
  Future<ApiResponse> getPosts({int skip=0,int limit=10}) async {
    this.clear();
    return this.condition(className: className,where:{"status":"ACTIVE"})
    .outE(what:"OPublish").condition(asName:"posts")
    .skip(i:skip)
    .limit(i:limit)
    .match("posts",orderBy: "createdAt").then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_POSTS,
          data: value.result.map<OPost>((item) => OPost(fields:item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_POSTS,
                message: e is ApiResponse?(e.hasError?e.errors[0].content:"Unkown error"):"failed to get posts",
                payload: e is ApiResponse?e.toString():null,)));
        return e;
      
    });
  }
  Future<ApiResponse> getMyPosts({int skip=0,int limit=10}) async {
    this.clear();
    return this.condition(className: className,where:{"name":"${getField('name')}","status":"ACTIVE"})
    .outE(what:"OPublish").condition(asName:"posts",className:"OPost")
    .skip(i:skip)
    .limit(i:limit)
    .match("posts",orderBy: "createdAt").then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_POSTS,
          data: value.result.map<OPost>((item) => OPost(fields: item)).toList()));
      return value;
    });
  }

  Future<ApiResponse> getViewedPosts({int skip=0,int limit=10}) async {
    this.clear();
    return getViewedNodes("OPost",skip: skip,limit: limit).then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_VIEWS_POSTS,
          data: value.result.map<OPost>((item) => OPost(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_VIEWS_POSTS,
                message: "failed to get posts",
                payload: e is ApiResponse?e.toString():null,)));
        return e;
      
    });
  }
  

  Future<ApiResponse> getLocation({String rid}) async {
    var recId=rid??this.rid;
    this.clear();
    return this.condition(className:className,where:{"@rid":'$recId'})
    .outE(what:"OLiveIn").condition(asName: "location").match("location").then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_LOCATION,
          data: value.result.map<OLocation>((item) => OLocation(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_LOCATION,
                message: "failed to get locations",
                payload: e is ApiResponse?e.toString():null,
                )));
        return e;
      
    });
    
  }
   
  Future<ApiResponse> getViewedNodes(objectClass,{int skip=0,int limit=10}){
    this.clear();
     return this.condition(className: className,where:{"name":"${getField('name')}","status":"ACTIVE"})
    .outE(what:"OView").condition(className:objectClass,asName:"posts")
    .skip(i:skip)
    .limit(i:limit)
    .match("posts",orderBy: "createdAt");
   }
  Future<ApiResponse> getlikedNodes(objectClass,{int skip=0,int limit=10}){
    this.clear();
    return this.condition(className: className,where:{"name":"${getField('name')}","status":"ACTIVE"})
    .outE(what:"Olike").condition(className:objectClass,asName:"posts")
    .skip(i:skip)
    .limit(i:limit)
    .match("posts",orderBy: "createdAt");
  }
  Future<ApiResponse> getViewedProducts({int skip=0,int limit=10}) async {
    this.clear();
    return getViewedNodes("OProduct",skip:skip,limit: limit).then((value) {
     
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_VIEWS_PRODUCTS,
          data: value.result.map<OProduct>((item) => OProduct(fields: item)).toList()));
      return value;
    }).catchError((e){
       
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_VIEWS_PRODUCTS,
                message: "failed to get products",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
  }

  Future<ApiResponse> getViewedStores({int skip=0,int limit=10}) async {
    
    return getViewedNodes("OStore",skip:skip,limit: limit).then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_VIEWS_STORES,
          data: value.result.map<OStore>((item) => OStore(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_VIEWS_STORES,
                message: "failed to get Stores",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
  }
  
  Future<ApiResponse> getViewedProfiles({int skip=0,int limit=10}) async {
     
    return getViewedNodes("OUser",skip:skip,limit: limit).then((value) {
     
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_VIEWS_PEOPLE,
          data: value.result.map<OStore>((item) => OStore(fields: item)).toList()));
      return value;
    }).catchError((e){
       
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_VIEWS_PEOPLE,
                message: "failed to get Stores",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
    
  }

  Future<ApiResponse> getLikedPosts({int skip=0,int limit=10}) async {
    return getlikedNodes("OPost",skip:skip,limit: limit).then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_LIKED_POSTS,
          data: value.result.map<OStore>((item) => OStore(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_LIKED_POSTS,
                message: "failed to get posts",
                payload: e is ApiResponse?e.toString():null,
                )));
        return e;
      
    });
    
  }

  Future<ApiResponse> getLikedStores({int skip=0,int limit=10}) async {
    return getlikedNodes("OStore",skip:skip,limit: limit).then((value) {
     
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_LIKED_STORES,
          data: value.result.map<OStore>((item) => OStore(fields: item)).toList()));
      return value;
    }).catchError((e){
       
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_LIKED_STORES,
                message: "failed to get Stores",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
    
  }

  Future<ApiResponse> getLikedProducts({int skip=0,int limit=10}) async {
    return getlikedNodes("OProduct",skip:skip,limit: limit).then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_LIKED_PRODUCTS,
          data: value.result.map<OProduct>((item) => OProduct(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_LIKED_PRODUCTS,
                message: "failed to get Stores",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
    
  }

  Future<List> getMyProfile() async {
    return [];
  }

  Future<ApiResponse> getReviews({@required String className,@required String rid}) async {

    this.clear();
    return this.condition(className: className ,where:{"@rid":rid})
    .inE(what:"OOf").condition(asName: "reviews",className: "OReview")
    .match("reviews",orderBy: "createdAt")
    .then((value) {      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_REVIEWS,
          data: value.result.map<OReview>((item) => OReview(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_REVIEWS,
                message: "failed to get reviews",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
  }

  Future<ApiResponse> getViewedReviews({int skip=0,int limit=10}) async {
    return getViewedNodes("OReview",skip:skip,limit:limit).then((value) {
     
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_REVIEWS,
          data: value.result.map<OReview>((item) => OReview(fields: item)).toList()));
      return value;
    }).catchError((e){
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_REVIEWS,
                message: "failed to get reviews",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
  }

  Future<ApiResponse> getLikedReviews({int skip=0,int limit=10}) async {
    return getlikedNodes("OReview",skip:skip,limit:limit).then((value) {
      
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_REVIEWS,
          data: value.result.map<OReview>((item) => OReview(fields: item)).toList()));
      return value;
    }).catchError((e){
      
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_REVIEWS,
                message: "failed to get reviews",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
    });
  }

  Future<ApiResponse> getPurchases({Map<String,String>where,int skip=0,int limit=10}) async {
    return this.condition(className: className,where:{"name":getField("name")})
    .outE(what:"OBuy")
    .condition(asName:"products",where:where).skip(i:skip).limit(i:limit)
    .match("products",orderBy: "createdAt").then((value) {
     
      dispatch(Action(
          type: USER_ACTIONS.GET_PURCHASES,
          data: value.result.map<OProduct>((item) => OProduct(fields: item)).toList()));
      return value;
    }).catchError((e){
       
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_PURCHASES,
                message: "failed to get payments",
                payload: e is ApiResponse?e.toString():null,
                )));
        return e;
      
    });
    
  }

  Future<ApiResponse> getPayments({Map<String,String>where,int skip=0,int limit=10}) async {
    return this.condition(className: className,where:{"name":getField("name")})
    .outE(what:"OPay")
    .condition(asName:"payment",where:where).skip(i:skip).limit(i:limit)
    .match("payment",orderBy: "createdAt").then((value) {
     
      dispatch(Action(
          type: USER_ACTIONS.GET_MY_PAYMENT_HISTORY,
          data: value.result.map<OPayment>((item) => OPayment(fields: item)).toList()));
      return value;
    }).catchError((e){
       
        dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: USER_ACTIONS.GET_MY_PAYMENT_HISTORY,
                message: "failed to get payments",
                payload: e is ApiResponse?e.toString():null,
                )));
        throw e;
      
    });
    
  }


Future<ApiResponse> getLikesFeed({int skip=0,int limit=10,bool isInitial=true}){
  var sql="""
  select *,
in.name as likedItem,
in.@rid as likedItemId,
in.@class as likedItemClass,
out.name as likerName,
out.@rid as likerId,
out.avator as likerAvator,
in.in_OLike as likes,
in.in_OLike.out[@rid=:rid].size() as liked,
in.in_OView.out[@rid=:rid].size()  as viewed
FROM 
(select expand(outE("OFollow")[out=:rid].inV().outE("OLike")) from OUser)
order by createdAt desc
SKIP :skip limit :limit 
  """;

Map cmd={
  "command":sql,
  "parameters":{"skip":skip,"limit":limit,"rid":rid}
};
return sqlCommand(cmd).then((value){
  dispatch(Action(
    type: isInitial?USER_ACTIONS.GET_INITIAL_LIKES_FEED:USER_ACTIONS.GET_LIKES_FEED,
    data:value.result.map<OLike>((item) => OLike(fields: item)).toList()
     ));
return value;
}).catchError((e){
dispatch(Action(
  data: AppError(
    type: isInitial?USER_ACTIONS.GET_INITIAL_LIKES_FEED:USER_ACTIONS.GET_LIKES_FEED,
    payload: e is ApiResponse?e.toString():null,
    message: "Failed to get likes of my follows"
  ),
  type: APP_ACTIONS.ON_ERROR
));
throw e;
  }
);

}


Future<ApiResponse> reloadLikeFeed({@required int i,@required feedId}){
  var sql="""
  select *,
in.name as likedItem,
in.@rid as likedItemId,
in.@class as likedItemClass,
out.name as likerName,
out.@rid as likerId,
out.avator as likerAvator,
in.in_OLike as likes,
in.in_OLike.out[@rid=:rid].size() as liked,
in.in_OView.out[@rid=:rid].size()  as viewed
FROM 
(select expand(outE("OFollow")[out=:rid].inV().outE("OLike")) from OUser)
where @rid=:frid
  """;

Map cmd={
  "command":sql,
  "parameters":{"frid":feedId,"rid":rid}
};
return sqlCommand(cmd).then((value){
  dispatch(Action(
    type: USER_ACTIONS.RELOAD_LIKES_FEED,
    data:{
    "data":value.result.map<OLike>((item) => OLike(fields: item)).toList(),
    "index":i}
     ));
return value;
}).catchError((e){
dispatch(Action(
  data: AppError(
    type: USER_ACTIONS.RELOAD_LIKES_FEED,
    payload: e is ApiResponse?e.toString():null,
    message: "Failed to get likes of my follows"
  ),
  type: APP_ACTIONS.ON_ERROR
));
throw e;
  }
);

}


Future<ApiResponse> getFollowsFeed({int skip=0,int limit=10,bool isInitial=true}){
  var sql="""
 select *,
in.name as followedItem,
in.@rid as followItemId,
in.@class as followItemClass,
out.name as followerName,
out.@rid as followerId,
out.avator as followerAvator,
in.in_OFollow as follows,
in.in_OFollow.out[@rid=:rid].size() as following,
in.in_OView.out[@rid=:rid].size()  as viewed,
in.in_OLike.out[@rid=:rid].size()  as liked,
out.in_OFollow.out[@rid= :rid].size() as followedBack
FROM 
(select expand(bothE("OFollow")) from OUser where @rid=:rid order by createdAt desc)
order by createdAt desc
SKIP :skip limit :limit
  """;

Map cmd={
  "command":sql,
  "parameters":{"skip":skip,"limit":limit,"rid":rid}
};
return sqlCommand(cmd).then((value){
  dispatch(Action(
    type: isInitial?USER_ACTIONS.GET_INITIAL_FOLLOWS_FEED:USER_ACTIONS.GET_FOLLOWS_FEED,
    data:value.result.map<OFollow>((item) => OFollow(fields: item)).toList()
     ));
return value;
}).catchError((e){
dispatch(Action(
  data: AppError(
    type: isInitial?USER_ACTIONS.GET_INITIAL_FOLLOWS_FEED:USER_ACTIONS.GET_FOLLOWS_FEED,
    payload: e is ApiResponse?e.toString():null,
    message: "Failed to get follow feed"
  ),
  type: APP_ACTIONS.ON_ERROR
));
throw e;
  }
);

}

Future<ApiResponse> reloadFollowFeed({@required int i,@required feedId}){
  var sql="""
 select *,
in.name as followedItem,
in.@rid as followItemId,
in.@class as followItemClass,
out.name as followerName,
out.@rid as followerId,
out.avator as followerAvator,
in.in_OFollow as follows,
in.in_OFollow.out[@rid=:rid].size() as following,
in.in_OView.out[@rid=:rid].size()  as viewed,
in.in_OLike.out[@rid=:rid].size()  as liked,
out.in_OFollow.out[@rid= :rid].size() as followedBack
FROM 
(select expand(bothE("OFollow")) from OUser where @rid=:rid )
where @rid=:frid
  """;

Map cmd={
  "command":sql,
  "parameters":{"frid":feedId,"rid":rid}
};
return sqlCommand(cmd).then((value){
  dispatch(Action(
    type: USER_ACTIONS.RELOAD_FOLLOWS_FEED,
    data:
    {
      "data":value.result.map<OFollow>((item) => OFollow(fields: item)).toList(),
    "index":i}
     ));
return value;
}).catchError((e){
dispatch(Action(
  data: AppError(
    type: USER_ACTIONS.RELOAD_FOLLOWS_FEED,
    payload: e is ApiResponse?e.toString():null,
    message: "Failed to reload follow feed"
  ),
  type: APP_ACTIONS.ON_ERROR
));
throw e;
  }
);

}
}

import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
import './OEdge.dart';
import './OLocation.dart';
import './OCategory.dart';
import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

import 'package:json_annotation/json_annotation.dart';

part 'OProduct.g.dart';

@JsonSerializable()
class OProduct extends V{
  factory OProduct.fromJson(Map<String, dynamic> json) => _$OProductFromJson(json);

    Map<String, dynamic> toJson() => _$OProductToJson(this);

    OProduct({className:"OProduct",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> belongTo({@required OCategory category,Map metadata})async{
         if(category.rid==null){       
           var res= await category.save();
            
        }
        var post=(new OBelong(fields: metadata))
                    ..from=rid
                    ..to=category.rid;
       return post.save();
      }
      Future<ApiResponse> locatedAt({@required OLocation location,Map metadata})async{
         if(location.rid==null){       
           var res= await location.save();
             
        }
        var locatedAt=(new OLocatedAt(fields: metadata))
                    ..from=rid
                    ..to=location.rid;
       return locatedAt.save();
      }
    Future<bool> reflesh({@required int i,@required String urid}){

      var sql="""select 
        Distinct(@rid) as rid,*
        from
        ( select *,
        in_OSell.out.name[0] as username,
        in_OSell.out.avator[0] as avator,
        in_OSell.out.roles[0] as roles,
        in_OSell.out.status[0] as status,
          out_OBelong.in.@rid as categoryId,
          out_OBelong.in.categoryName as categoryName,
        in_OLike.out.name as likes,
        in_OLike.out[@rid=:rid].size() as liked,
        in_OView.out[@rid=:rid].size() as viewed,
        in_OBuy.out[@rid=:rid].size() as bought,
        in_OSell.out[@rid=:rid].size() as selling,
        out_OLocatedAt.in.region[0] as region,
        out_OLocatedAt.in.district[0] as district,
        out_OLocatedAt.in.street[0] as street,
        out_OLocatedAt.in.ward[0] as ward,
        out_OLocatedAt.in.alt[0] as altitude,
        out_OLocatedAt.in.lat[0] as latitude,
        out_OLocatedAt.in.lon[0] as longitude
        from
        (select expand(inE("OSell").in)  from OProduct where @rid=:prid )
        )
        """;
    return  sqlCommand(
      {
        "command":sql,
        "parameters":{"prid":rid,"rid":urid}
    }).then((value) {
    
      rawData = value.result[0];
      dispatch(Action(
          type:PRODUCT_ACTIONS.RELOAD_PRODUCT,
          data:{"product":this,"index":i}
        ));
        return true;
    }).catchError((value){
      dispatch(Action(
            type: APP_ACTIONS.ON_ERROR,
            data: AppError(
                type: PRODUCT_ACTIONS.RELOAD_PRODUCT,
                message: "failed to refresh product",
                payload: value is ApiResponse?value:null)));
        throw value;
     
    });






      
    }
}
// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'OIndex.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OIndex _$OIndexFromJson(Map<String, dynamic> json) {
  return OIndex(className: json['className'])
    ..rid = json['rid']
    ..type = json['type']
    ..version = json['version']
    ..rawData = json['rawData']
    ..data = json['data'] as Map<String, dynamic>;
}

Map<String, dynamic> _$OIndexToJson(OIndex instance) => <String, dynamic>{
      'rid': instance.rid,
      'type': instance.type,
      'version': instance.version,
      'className': instance.className,
      'rawData': instance.rawData,
      'data': instance.data
    };

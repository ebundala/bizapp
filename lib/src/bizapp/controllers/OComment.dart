import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OComment.g.dart';

@JsonSerializable()
class OComment extends V{
  factory OComment.fromJson(Map<String, dynamic> json) => _$OCommentFromJson(json);

    Map<String, dynamic> toJson() => _$OCommentToJson(this);

    OComment({className:"OComment",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> commentOf({@required V item ,Map metadata})async{
         if(item.rid==null){       
           var res= await item.save();
          
        }
        var post=(new OOf(fields: metadata))
                    ..from=rid
                    ..to=item.rid;
       return post.save();
      }
      
}
import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';
//import './OUser.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OMessage.g.dart';

@JsonSerializable()
class OMessage extends V{
  factory OMessage.fromJson(Map<String, dynamic> json) => _$OMessageFromJson(json);

    Map<String, dynamic> toJson() => _$OMessageToJson(this);
    OMessage({className:"OMessage",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> messageOf({@required String username ,Map metadata})async{
         if(username==null){       
           //var res= await user.save();
        //if(res[0].containsKey("errors")){
          throw(username);
       // }      
        }
        var post=(new OOf(fields: metadata))
                    ..from=rid
                    ..to="(SELECT FROM OUser WHERE name='$username')";
       return post.save();
      }
    
}
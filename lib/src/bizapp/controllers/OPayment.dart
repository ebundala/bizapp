import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';
import './OProduct.dart';
import './OStore.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OPayment.g.dart';

@JsonSerializable()
class OPayment extends V{
  factory OPayment.fromJson(Map<String, dynamic> json) => _$OPaymentFromJson(json);

    Map<String, dynamic> toJson() => _$OPaymentToJson(this);
    OPayment({className:"OPayment",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> paymentOf({@required OProduct product ,Map metadata})async{
         if(product.rid==null){      
       
          throw(product);
      
        }
        var payment=(new OPaymentOf(fields: metadata))
                    ..from=rid
                    ..to=product.rid;
       return payment.save();
      }
      Future<ApiResponse> subscriptionOf({@required OStore store ,Map metadata})async{
         if(store.rid==null){     
         
          throw(store);
          
        }
        var payment=(new OSubscriptionOf(fields: metadata))
                    ..from=rid
                    ..to=store.rid;
       return payment.save();
      }


}

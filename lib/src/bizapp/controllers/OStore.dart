import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OProduct.dart';
import './OEdge.dart';
import './OCategory.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';
import 'package:json_annotation/json_annotation.dart';

part 'OStore.g.dart';

@JsonSerializable()
class OStore extends V{
  factory OStore.fromJson(Map<String, dynamic> json) => _$OStoreFromJson(json);

    Map<String, dynamic> toJson() => _$OStoreToJson(this);
    OStore({className:"OStore",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> sell_product({@required OProduct product,Map metadata})async{
            if(product.rid==null){
            var res= await product.save();
            }
            var sell=(new OSell(fields: metadata))
                        ..from=rid
                        ..to=product.rid;
          return sell.save();
          }
  Future<ApiResponse> has({@required OCategory category,Map metadata})async{
         if(category.rid==null){       
           var res= await category.save();
             
        }
        var has=(new OHas(fields: metadata))
                    ..from=rid
                    ..to=category.rid;
       return has.save();
      }
  

}
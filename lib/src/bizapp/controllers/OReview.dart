import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OReview.g.dart';

@JsonSerializable()
class OReview extends V{
  factory OReview.fromJson(Map<String, dynamic> json) => _$OReviewFromJson(json);

    Map<String, dynamic> toJson() => _$OReviewToJson(this);
    OReview({className:"OReview",Map fields}):super(className,fields:fields);  
    Future<ApiResponse> reviewOf({@required V item ,Map metadata})async{
         if(item.rid==null){       
           var res= await item.save();
             
        }
        var post=(new OOf(fields: metadata))
                    ..from=rid
                    ..to=item.rid;
       return post.save();
      }
  
      
}
import 'dart:async';
//import 'dart:convert';
import 'package:meta/meta.dart';
//import '../actions/Actions.dart';
import '../base/AppBase.dart';
//import '../common/orientDB.dart';
import './OEdge.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';

part 'OCategory.g.dart';

@JsonSerializable()
class OCategory extends V{
  factory OCategory.fromJson(Map<String, dynamic> json) => _$OCategoryFromJson(json);

    Map<String, dynamic> toJson() => _$OCategoryToJson(this);
  
    OCategory({className:"OCategory",Map fields}):super(className,fields:fields);
    Future<ApiResponse> childOf({@required OCategory category,Map metadata}){
      if(category.rid==null){
        throw(category);
      }
      var childof=new OChildOf(fields:metadata)
      ..from=rid
      ..to=category.rid;
      return childof.save();
    }
   
}
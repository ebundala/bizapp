//import 'dart:async';
//import '../common/orientDB.dart';
import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:json_annotation/json_annotation.dart';

part 'OEdge.g.dart';
//like edge class
@JsonSerializable()
class OFollow extends E{
  OFollow({className="OFollow",fields}):super(className,fields:fields);
      factory OFollow.fromJson(Map<String, dynamic> json) => _$OFollowFromJson(json);

}

//like edge class
@JsonSerializable()
class OLike extends E{
  OLike({className="OLike",fields}):super(className,fields:fields);
    factory OLike.fromJson(Map<String, dynamic> json) => _$OLikeFromJson(json);

}

// Review edge class
@JsonSerializable()
class OReviewed extends E{
  OReviewed({className="OReviewed",fields}):super(className,fields:fields);
}

// View  edge class
@JsonSerializable()
class OView extends E{
  OView({className="OView",fields}):super(className,fields:fields);
      factory OView.fromJson(Map<String, dynamic> json) => _$OViewFromJson(json);

}

//Publish edge class
@JsonSerializable()
class OPublish extends E{
  OPublish({className="OPublish",fields}):super(className,fields:fields);
}

//Of edge class
@JsonSerializable()
class OOf extends E{
  OOf({className="OOf",fields}):super(className,fields:fields);
}

//liveIn edge class
@JsonSerializable()
class OLiveIn extends E{
  OLiveIn({className="OLiveIn",fields}):super(className,fields:fields);
}

//locatedAt edge class
@JsonSerializable()
class OLocatedAt extends E{
  OLocatedAt({className="OLocatedAt",fields}):super(className,fields:fields);
}

//Owns edge class
@JsonSerializable()
class OOwn extends E{
  OOwn({className="OOwn",fields}):super(className,fields:fields);
}

//Sell edge class
@JsonSerializable()
class OSell extends E{
  OSell({className="OSell",fields}):super(className,fields:fields);
}

//Buy edge class
@JsonSerializable()
class OBuy extends E{
  OBuy({className="OBuy",fields}):super(className,fields:fields);
}

//Pay edge class
@JsonSerializable()
class OPay extends E{
  OPay({className="OPay",fields}):super(className,fields:fields);
}

//paymentOf edge class
@JsonSerializable()
class OPaymentOf extends E{
  OPaymentOf({className="OPaymentOf",fields}):super(className,fields:fields);
}

//subscriptionOf edge class
@JsonSerializable()
class OSubscriptionOf extends E{
  OSubscriptionOf({className="OSubscriptionOf",fields}):super(className,fields:fields);
}

//belong edge class
@JsonSerializable()
class OBelong extends E{
  OBelong({className="OBelong",fields}):super(className,fields:fields);
}

//childOf edge class
@JsonSerializable()
class OChildOf extends E{
  OChildOf({className="OChildOf",fields}):super(className,fields:fields);
}

//PartOf edge class
@JsonSerializable()
class OPartOf extends E{
  OPartOf({className="OPartOf",fields}):super(className,fields:fields);
}

//Has edge class
@JsonSerializable()
class OHas extends E{
  OHas({className="OHas",fields}):super(className,fields:fields);
}
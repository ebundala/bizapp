import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
import 'package:bizapp/src/bizapp/controllers/OCategory.dart';
import 'package:bizapp/src/bizapp/controllers/OLocation.dart';
import 'package:bizapp/src/bizapp/controllers/OMessage.dart';
import 'package:bizapp/src/bizapp/controllers/OStore.dart';
import 'package:bizapp/src/bizapp/controllers/OPost.dart';
import 'package:bizapp/src/bizapp/controllers/OPayment.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
//import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'state.g.dart';

@JsonSerializable()
class AppState {

String authString;
String title="Shop";
bool loading=false;
int pageTabIndex=0;

List<AppError> errors=[];
OUser user=OUser();//current user 
List<OProduct> products=[];//trending globally
List<OProduct> selling=[];//my item i sell
List<OProduct> trendingProducts=[];//trending on users cycle
List<OProduct> boughtProducts=[];//y items i have bou
List<OPost> myPosts=[];//posts i have posted
List<OPost> trendingPosts=[];// trending on users cycle
List<OPost>  posts=[]; // global trending posts
List<OStore> stores;//trending stores globaly
List<OStore> trendingStores=[]; //trending store on users cycle
List<OStore> myStore=[]; //my stores
List<OStore> storeIfollow=[]; // stores
List<OPayment> payments=[];//my payment history
List<OUser> followers=[];
List<OUser> following=[];
List<OUser> peopleTofollow=[];
List<OCategory> categories=[];
List<OMessage> messages=[];
List<OLike>  likesFeed=[];
List<OView>  viewsFeed=[];
List<OFollow> followsFeed=[];
OLocation myLocation;

//AppState();

static AppState fromJson(json) => _$AppStateFromJson(json);

    Map<String, dynamic> toJson() => _$AppStateToJson(this);
}
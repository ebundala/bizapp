// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppState _$AppStateFromJson(Map<String, dynamic> json) {
  return AppState()
    ..authString = json['authString'] as String
    ..title = json['title'] as String
    ..loading = json['loading'] as bool
    ..pageTabIndex = json['pageTabIndex'] as int
    ..errors = (json['errors'] as List)
        ?.map((e) =>
            e == null ? null : AppError.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..user = json['user'] == null
        ? null
        : OUser.fromJson(json['user'] as Map<String, dynamic>)
    ..products = (json['products'] as List)
        ?.map((e) =>
            e == null ? null : OProduct.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..selling = (json['selling'] as List)
        ?.map((e) =>
            e == null ? null : OProduct.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..trendingProducts = (json['trendingProducts'] as List)
        ?.map((e) =>
            e == null ? null : OProduct.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..boughtProducts = (json['boughtProducts'] as List)
        ?.map((e) =>
            e == null ? null : OProduct.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..myPosts = (json['myPosts'] as List)
        ?.map(
            (e) => e == null ? null : OPost.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..trendingPosts = (json['trendingPosts'] as List)
        ?.map(
            (e) => e == null ? null : OPost.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..posts = (json['posts'] as List)
        ?.map(
            (e) => e == null ? null : OPost.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..stores = (json['stores'] as List)
        ?.map((e) =>
            e == null ? null : OStore.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..trendingStores = (json['trendingStores'] as List)
        ?.map((e) =>
            e == null ? null : OStore.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..myStore = (json['myStore'] as List)
        ?.map((e) =>
            e == null ? null : OStore.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..storeIfollow = (json['storeIfollow'] as List)
        ?.map((e) =>
            e == null ? null : OStore.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..payments = (json['payments'] as List)
        ?.map((e) =>
            e == null ? null : OPayment.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..followers = (json['followers'] as List)
        ?.map(
            (e) => e == null ? null : OUser.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..following = (json['following'] as List)
        ?.map((e) => e == null ? null : OUser.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..peopleTofollow = (json['peopleTofollow'] as List)?.map((e) => e == null ? null : OUser.fromJson(e as Map<String, dynamic>))?.toList()
    ..categories = (json['categories'] as List)?.map((e) => e == null ? null : OCategory.fromJson(e as Map<String, dynamic>))?.toList()
    ..messages = (json['messages'] as List)?.map((e) => e == null ? null : OMessage.fromJson(e as Map<String, dynamic>))?.toList()
    ..likesFeed = (json['likesFeed'] as List)?.map((e) => e == null ? null : OLike.fromJson(e as Map<String, dynamic>))?.toList()
    ..viewsFeed = (json['viewsFeed'] as List)?.map((e) => e == null ? null : OView.fromJson(e as Map<String, dynamic>))?.toList()
    ..followsFeed = (json['followsFeed'] as List)?.map((e) => e == null ? null : OFollow.fromJson(e as Map<String, dynamic>))?.toList()
    ..myLocation = json['myLocation'] == null ? null : OLocation.fromJson(json['myLocation'] as Map<String, dynamic>);
}

Map<String, dynamic> _$AppStateToJson(AppState instance) => <String, dynamic>{
      'authString': instance.authString,
      'title': instance.title,
      'loading': instance.loading,
      'pageTabIndex': instance.pageTabIndex,
      'errors': instance.errors,
      'user': instance.user,
      'products': instance.products,
      'selling': instance.selling,
      'trendingProducts': instance.trendingProducts,
      'boughtProducts': instance.boughtProducts,
      'myPosts': instance.myPosts,
      'trendingPosts': instance.trendingPosts,
      'posts': instance.posts,
      'stores': instance.stores,
      'trendingStores': instance.trendingStores,
      'myStore': instance.myStore,
      'storeIfollow': instance.storeIfollow,
      'payments': instance.payments,
      'followers': instance.followers,
      'following': instance.following,
      'peopleTofollow': instance.peopleTofollow,
      'categories': instance.categories,
      'messages': instance.messages,
      'likesFeed': instance.likesFeed,
      'viewsFeed': instance.viewsFeed,
      'followsFeed': instance.followsFeed,
      'myLocation': instance.myLocation
    };

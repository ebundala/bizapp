//@JsonSerializable()
class ApiError {
  final int reason;
  final int code;
 final String content;
 
  ApiError(Map data):assert(data!=null),
  reason=data["reason"],
  code=data["code"],
  content=data["content"];
@override
  String toString() {
    // TODO: implement toString
    return "{reason:$reason code:$code ,content:$content}";
  }
}

//@JsonSerializable()
class ApiResponse {
  final List<ApiError> errors;
  final List result;
  final  excutionPlan;

  ApiResponse(List<dynamic> data):assert(data!=null),
  this.errors=data[0]["errors"]?.map<ApiError>((e)=>ApiError(e))?.toList(),
  this.excutionPlan=data[0]["excutionPlan"],
  this.result=data[0]["result"];//?.map((e)=>e)?.toList();

  get hasError{
    return errors!=null;
  }
  get hasResult{
    return result!=null;

  }
  get hasExcutionPlan{
    return excutionPlan!=null;
  } 
  get errorMessage{
    return hasError?errors[0].content:null;
  }
  get errorCode{
        return hasError?errors[0].code:null;

  }
  get errorReason{
        return hasError?errors[0].reason:null;

  }
  
}

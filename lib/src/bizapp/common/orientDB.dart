import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import './responses.dart';

part './orientDB.g.dart';


@JsonSerializable()
class OrientTypes {
  static final String STRING = 'STRING';
  static final String FlOAT = 'FlOAT'; //for float
  static final String DECIMAL = 'DECIMAL'; //for decimal
  static final String LONG = 'LONG'; //for long
  static final String DOUBLE = 'DOUBLE'; //for double
  static final String BINARY = 'BINARY'; //for byte and binary
  static final String DATE = 'DATE'; //for date
  static final String DATETIME = 'DATETIME'; //for datetime
  static final String SHORT = 'SHORT'; //for short
  static final String SET ='SET'; //for Set, because arrays and List are serialized as arrays like [3,4,5]
  static final String LINKS = 'LINKS'; //for links
  static final String LINKSET = 'LINKSET'; //for linksets
  static final String LINKLIST = 'LINKLIST'; //for linklist
  static final String LINKMAP = 'LINKMAP'; //for linkmap
  static final String LINKBAG = 'LINKBAG'; //for linkbag
  static final String CUSTOM = 'CUSTOM'; //for custom

}

@JsonSerializable()
class OrientJsonTypes {
  static final String FlOAT = 'f'; //for float
  static final String DECIMAL = 'c'; //for decimal
  static final String LONG = 'l'; //for long
  static final String DOUBLE = 'd'; //for double
  static final String BINARY = 'b'; //for byte and binary
  static final String DATE = 'a'; //for date
  static final String DATETIME = 't'; //for datetime
  static final String SHORT = 's'; //for short
  static final String SET =
      'e'; //for Set, because arrays and List are serialized as arrays like [3,4,5]
  static final String LINKS = 'x'; //for links
  static final String LINKSET = 'n'; //for linksets
  static final String LINKLIST = 'z'; //for linklist
  static final String LINKMAP = 'm'; //for linkmap
  static final String LINKBAG = 'g'; //for linkbag
  static final String CUSTOM = 'u'; //for custom

}

@JsonSerializable()
class OrientCommands {
  static final String CONNECT = 'connect';
  static final String DISCONECT = 'disconnect';
  static final String DATABASE = 'database';
  static final String CLASS = 'class';
  static final String PROPERY = 'property';
  static final String CLUSTER = 'cluster';
  static final String COMMAND = 'command';
  static final String BATCH = 'batch';
  static final String FUNCTION = 'function';
  static final String EXPORT = 'export';
  static final String IMPORT = 'import';
  static final String LIST_DATABASES = 'listDatabases';
  static final String DOCUMENT = 'document';
  static final String DOCUMENT_BY_CLASS = 'documentbyclass';
  static final String ALLOCATION = 'allocation';
  static final String INDEX = 'index';
  static final String QUERY = 'query';
  static final String SERVER = 'server';
  static final String CONNECTION = 'connection';
  static final String TOKEN = 'token';
}

@JsonSerializable()
class OrientStatusCodes {
  static final int SUCCESS = 200;
  static final int CONNECTED = 204;
  static final int CREATED = 201;
  static final int FAILED = 401;
  static final int NOT_FOUND = 404;
  static final int COMMAND_NOT_FOUND = 405;
}

@JsonSerializable()
class OrientLanguage {
  static final String SQL = "sql";
  static final String GREMLIN = "gremlin";
  static final String JAVASCRIPT = "javascript";
  static final String JAVA = "java";
}

@JsonSerializable()
class OrientBatchType {
  static final String CREATE = "c";
  static final String UPDATE = "u";
  static final String DELETE = "d";
  static final String COMMAND = "cmd";
  static final String SCRIPT = "script";
}

@JsonSerializable()
class OrientUpdateMode {
  static final String FULL = "full";
  static final String PARTIAL = "partial";
}




@JsonSerializable()
class OrientDB {
  factory OrientDB.fromJson(Map<String, dynamic> json) =>
      _$OrientDBFromJson(json);

  Map<String, dynamic> toJson() => _$OrientDBToJson(this);
  final String userAgent = 'OrientDB';
  static String url;
  static String appKey;
  static String appSecret;
  static String username;
  static String password;
  static int port;
  static String database;
  static bool useApp = false;
  static HttpClientBasicCredentials credentials;
  static HttpClientBasicCredentials appCredentials;

  static String OSSESIONID;
  static bool isToken;
  static String token;
  static int expire;
  Duration timeLimit=Duration(seconds: 30);
  // static int timeout;
  //static int lastSendOn = timestamp();
  static HttpClient userClient = new HttpClient();
  static HttpClient appClient = new HttpClient();
  HttpClient client() {
    return (OrientDB.useApp ? OrientDB.appClient : OrientDB.userClient);
  }

  OrientDB(
      {url = "http://localhost",
      port = 2480,
      database: "bizapp",
      isToken = false,
       appKey,
       appSecret}) {
    OrientDB.userClient.authenticate = authenticate;
    OrientDB.appClient.authenticate = authenticateApp;
    OrientDB.database = database;
    OrientDB.password = appSecret;
    OrientDB.username = appKey;
    OrientDB.appSecret = appSecret;
    OrientDB.appKey = appKey;
    OrientDB.url = url;
    OrientDB.port = port;

    authenticateApp(Uri.parse('$url:$port'));
    
  }
  Future<bool> addCredentials({@required String username,@required String password,String dbName})async{
    close();
    OrientDB.username = username;
    OrientDB.password = password;
    OrientDB.userClient = new HttpClient();
    OrientDB.userClient.authenticate = authenticate;
    OrientDB.database = dbName ?? OrientDB.database;
      return authenticate(Uri.parse('${OrientDB.url}:${OrientDB.port}'));
  }
  Future<ApiResponse> login(
      {@required String username,
      @required String password,
      }) async {
       close();
      OrientDB.userClient = new HttpClient();
      OrientDB.userClient.authenticate = authenticate;
      OrientDB.username = username;
      OrientDB.password = password;
      await authenticate(Uri.parse('${OrientDB.url}:${OrientDB.port}'));
     return connect().then((status){
      return ApiResponse([
        {"${status?"result":"errors"}":[{"${status?"connected":"content"}": status?status:"invalid password or username"}]}
      ]);
      });
    
  }

  Future<bool> logout() async {
    close();
    OrientDB.userClient = new HttpClient();
    OrientDB.userClient.authenticate = authenticate;
    OrientDB.username = null;
    OrientDB.password = null;
    return Future.value(true);
  }

  Future<bool> authenticateApp(Uri uri, [String realm, String ts]) {
    print("authenticate\n ${realm ?? ''}");
    OrientDB.appCredentials =
        new HttpClientBasicCredentials(OrientDB.appKey, OrientDB.appSecret);
    OrientDB.appClient.addCredentials(
        Uri.parse('${OrientDB.url}:${OrientDB.port}/'),
        realm ?? "OrientDB db-${OrientDB.database}",
        OrientDB.appCredentials);
    return new Future.value(true);
  }

  Future<bool> authenticate(Uri uri, [String realm, String ts]) {
    print("authenticate\n ${realm ?? ''}");
    OrientDB.credentials =
        new HttpClientBasicCredentials(OrientDB.username, OrientDB.password);
    OrientDB.userClient.addCredentials(
        Uri.parse('${OrientDB.url}:${OrientDB.port}/'),
        realm ?? "OrientDB db-${OrientDB.database}",
        OrientDB.credentials);
    return new Future.value(true);
  }

  //TODO refactor database name to enable using multiple databases
  Future<HttpClientResponse> postUrl(
      {@required String cmd,
      String args = "",
      Map body,
      String content_type = "application/json"}) {
    return client()
        .postUrl(Uri.parse(
            '${OrientDB.url}:${OrientDB.port}/${cmd ?? OrientCommands.CONNECT}${OrientDB.database.isNotEmpty ? "/${OrientDB.database}" : ""}${args.isNotEmpty ? "/$args" : ""}'))
        .then((HttpClientRequest request) async {
      print("connected");
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      //request.

      //request.contentLength;
      request.headers.set(HttpHeaders.ACCEPT_ENCODING, 'gzip,deflate');
      request.headers.set(HttpHeaders.CONTENT_TYPE, content_type);
      //request.headers.set(HttpHeaders.TRANSFER_ENCODING, 'identity');
      if (body != null) {
        //var jsonMap=json.decode(body);
        var content;
        var encodedData = "";
        if (content_type == "application/json") {
          encodedData = json.encode(body);
        } else {
          encodedData = _encodeMapToUrl(body);
         // print("body data \n$encodedData");
        }
        content = utf8.encode(encodedData);
        request.contentLength = content.length;
        print("map length ${content.length}");
        request.add(content);
      }

      print("${request.uri.path}\n${request.uri.query}\nlength ${request.contentLength} ${request.headers}");
      return request.close();
    }).then((HttpClientResponse response) {
      // Process the response.
      print("responded with ${response.statusCode} ${response.contentLength}");

      return response;
    }).catchError((e) {
      print("error occored ${e.message}");
      if(e is HttpException){
        throw ApiResponse([{"errors":[{"content":e.message}]}]);
      }
       
      throw e;
    });
  }

  String _encodeMapToUrl(Map data) {
    return data.keys
        .map((key) =>
            "${Uri.encodeComponent(key)}=${Uri.encodeComponent(data[key])}")
        .join("&");
  }
   void _handleHttpException(e){
     
     if(e is HttpException || e is OSError||e is SocketException||e is TimeoutException){
        throw ApiResponse(
          [{"errors":[{"content":e is TimeoutException?"connection timeout":e.message}]}
          ]);
      }
   }
  Future<HttpClientResponse> getUrl(
      {@required String cmd, String args = ""}) {
    return client()
        .getUrl(Uri.parse(
            '${OrientDB.url}:${OrientDB.port}/${cmd ?? OrientCommands.CONNECT}${OrientDB.database.isNotEmpty ? "/${OrientDB.database}" : ""}${args.isNotEmpty ? "/$args" : ""}'))
        .timeout(timeLimit)
        .then((HttpClientRequest request) {
      print("connected");
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      request.headers.set(HttpHeaders.ACCEPT_ENCODING, 'gzip,deflate');
      //request.headers.set(HttpHeaders.COOKIE, OSSESIONID);
     // print("${request.uri.path}\n${request.uri.query}\nlength ${request.contentLength} ${request.headers}");

      return request.close().timeout(timeLimit);
    }).then((HttpClientResponse response) {
      // Process the response.
      print("responded with ${response.statusCode} ${response.contentLength}");

      return response;
    }).catchError((e) {
      print("error occured ${e.message}");
      _handleHttpException(e);
      throw e;
    });
    // res;
  }

  Future<HttpClientResponse> headUrl(
      {@required String cmd, String args = ""}) {
    return client()
        .headUrl(Uri.parse(
            '${OrientDB.url}:${OrientDB.port}/${cmd ?? OrientCommands.DOCUMENT}${OrientDB.database.isNotEmpty ? "/${OrientDB.database}" : ""}${args.isNotEmpty ? "/$args" : ""}'))
        .timeout(timeLimit)
        .then((HttpClientRequest request) {
      print("connected");
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      request.headers.add(HttpHeaders.ACCEPT_ENCODING, 'gzip,deflate');
      print(
          "${request.uri.path}\n${request.uri.query}\nlength ${request.contentLength} ${request.headers}");

      return request.close().timeout(timeLimit);
    }).then((HttpClientResponse response) {
      // Process the response.
      print("responded with ${response.statusCode} ${response.contentLength}");

      return response;
    }).catchError((e) {
      print("error occured ${e.message}");
      _handleHttpException(e);
      throw e;
    });
    // res;
  }

  Future<HttpClientResponse> putUrl(
      {@required String cmd,
      String args = "",
      Map body}) {
    return client()
        .putUrl(Uri.parse(
            '${OrientDB.url}:${OrientDB.port}/${cmd ?? OrientCommands.DOCUMENT}${OrientDB.database.isNotEmpty ? "/${OrientDB.database}" : ""}${args.isNotEmpty ? "/$args" : ""}'))
        .timeout(timeLimit)
        .then((HttpClientRequest request) async {
      print("connected");
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      //request.
      //request.contentLength;
      request.headers.set(HttpHeaders.ACCEPT_ENCODING, 'gzip,deflate');
      request.headers.set(HttpHeaders.CONTENT_TYPE, 'application/json');
      //request.headers.set(HttpHeaders.TRANSFER_ENCODING, 'identity');
      if (body != null) {
        //var jsonMap=json.decode(body);
        var content = utf8.encode(json.encode(body));
        request.contentLength = content.length;
        print("mapp length ${content.length}");
        request.add(content);
      }

      print("\nlength ${request.contentLength} ${request.headers}");
      return request.close().timeout(timeLimit);
    }).then((HttpClientResponse response) {
      // Process the response.
      print("responded with ${response.statusCode} ${response.contentLength}");

      return response;
    }).catchError((e) {
      print("error occored ${e.message}");
      _handleHttpException(e);
      throw e;
    });
  }

  Future<HttpClientResponse> patchUrl(
      {@required String cmd,
      String args = "",
      Map body}) {
    return client()
        .patchUrl(Uri.parse(
            '${OrientDB.url}:${OrientDB.port}/${cmd ?? OrientCommands.DOCUMENT}${OrientDB.database.isNotEmpty ? "/${OrientDB.database}" : ""}${args.isNotEmpty ? "/$args" : ""}'))
        .timeout(timeLimit)
        .then((HttpClientRequest request) async {
      print("connected");
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      //request.
      //request.contentLength;
      request.headers.set(HttpHeaders.ACCEPT_ENCODING, 'gzip,deflate');
      request.headers.set(HttpHeaders.CONTENT_TYPE, 'application/json');
      //request.headers.set(HttpHeaders.TRANSFER_ENCODING, 'identity');
      if (body != null) {
        //var jsonMap=json.decode(body);
        var content = utf8.encode(json.encode(body));
        request.contentLength = content.length;
        print("mapp length ${content.length}");
        request.add(content);
      }

      print("\nlength ${request.contentLength} ${request.headers}");
      return request.close().timeout(timeLimit);
    }).then((HttpClientResponse response) {
      // Process the response.
      print("responded with ${response.statusCode} ${response.contentLength}");

      return response;
    }).catchError((e) {
      print("error occored ${e.message}");
      _handleHttpException(e);
      throw e;
    });
  }

  Future<HttpClientResponse> deleteUrl(
      {@required String cmd,
      String args = "",
      Map body}) {
    return client()
        .deleteUrl(Uri.parse(
            '${OrientDB.url}:${OrientDB.port}/${cmd ?? OrientCommands.DOCUMENT}${OrientDB.database.isNotEmpty ? "/${OrientDB.database}" : ""}${args.isNotEmpty ? "/$args" : ""}'))
        .timeout(timeLimit)
        .then((HttpClientRequest request) async {
      print("connected");
      // Optionally set up headers...
      // Optionally write to the request object...
      // Then call close.
      //request.
      //request.contentLength;
      request.headers.set(HttpHeaders.ACCEPT_ENCODING, 'gzip,deflate');
      request.headers.set(HttpHeaders.CONTENT_TYPE, 'application/json');
      //request.headers.set(HttpHeaders.TRANSFER_ENCODING, 'identity');
      if (body != null) {
        //var jsonMap=json.decode(body);
        var content = utf8.encode(json.encode(body));
        request.contentLength = content.length;
        print("mapp length ${content.length}");
        request.add(content);
      }

      print("\nlength ${request.contentLength} ${request.headers}");
      return request.close().timeout(timeLimit);
    }).then((HttpClientResponse response) {
      // Process the response.
      print("responded with ${response.statusCode} ${response.contentLength}");

      return response;
    }).catchError((e) {
      print("error occored ${e.message}");
      _handleHttpException(e);
      throw e;
    });
  }

  Future<bool> connect() async {
    return getUrl(cmd: OrientCommands.CONNECT).then((res){
    var headers = res?.headers;

    if (res?.statusCode == OrientStatusCodes.CONNECTED) {
      print("ossessionid ${headers?.value("set-cookie")}");
      OSSESIONID = headers?.value("set-cookie");
      return true;
    } else
      return false;
      });
  }

  Future<Stream> getDatabase() async {
    var response = await getUrl(cmd: OrientCommands.DATABASE);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder).transform(json.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]).transform(json.decoder);
  }

  Future<Stream> getClass({@required String className}) async {
    var response = await getUrl(cmd: OrientCommands.CLASS, args: className);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder).transform(json.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]).transform(json.decoder);
  }

  Future<Stream> createClass(String className) async {
    var response = await postUrl(cmd: OrientCommands.CLASS, args: className);
    if (response?.statusCode == OrientStatusCodes.CREATED)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<Stream> addProperty(
      {String type,
      @required String className,
      @required String propertyName}) async {
    var args = "$className/$propertyName/$type";
    var response = await postUrl(cmd: OrientCommands.PROPERY, args: args);
    if (response?.statusCode == OrientStatusCodes.CREATED)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> deleteProperty(
      {@required String className, @required String propertyName}) async {
    var args = "$className/$propertyName";
    var response = await deleteUrl(cmd: OrientCommands.PROPERY, args: args);
    if (response?.statusCode == OrientStatusCodes.CONNECTED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<Stream> addProperties(
      {@required String className, @required Map properties}) async {
    var args = "$className/";
    var response = await postUrl(
        cmd: OrientCommands.PROPERY, args: args, body: properties);
    if (response?.statusCode == OrientStatusCodes.CREATED)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response?.statusCode},"message":"${response?.reasonPhrase}"}'
      ]);
  }

  Future<Stream> executeCommand({@required Map cmd, String language}) async {
    var args = "${language ?? OrientLanguage.SQL}";
    var response =
        await postUrl(cmd: OrientCommands.COMMAND, args: args, body: cmd);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response?.transform(utf8.decoder);
    else
      return response?.transform(utf8
          .decoder); //new Stream.fromIterable(['{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}']);
  }

  Future<Stream> executeCommandUrl(
      {@required String cmd,
      int limit = 20,
      String fetchPlan = "1:1",
      String language}) async {
    var args = "${language ?? OrientLanguage.SQL}/$cmd/$limit/$fetchPlan";
    var response = await postUrl(cmd: OrientCommands.COMMAND, args: args);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<Stream> batch(Map cmd) async {
    var response = await postUrl(cmd: OrientCommands.BATCH, body: cmd);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response?.statusCode},"message":"${response?.reasonPhrase}"}'
      ]);
  }

  Future<Stream> callFunction({@required String fn, List args}) async {
    var argsjoin = args.join("/");
    var parameters = "$fn/$argsjoin";
    var response =
        await postUrl(cmd: OrientCommands.FUNCTION, args: parameters);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> disconnect() async {
    var res = await getUrl(cmd: OrientCommands.DISCONECT);
    if (res.statusCode == OrientStatusCodes.FAILED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<Stream> getDocument({@required String rid, String fetchPlan}) async {
    var args = "$rid/$fetchPlan";
    var response = await getUrl(cmd: OrientCommands.DOCUMENT, args: args);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> headDocument({@required String rid}) async {
    var args = "$rid";
    var response = await headUrl(cmd: OrientCommands.DOCUMENT, args: args);
    if (response?.statusCode == OrientStatusCodes.CONNECTED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<Stream> putDocument(
      {@required String rid, @required Map body, String mode}) async {
    var args = "$rid?updateMode=${mode ?? OrientUpdateMode.PARTIAL}";
    var response =
        await putUrl(cmd: OrientCommands.DOCUMENT, args: args, body: body);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<Stream> createDocument({@required Map body}) async {
    //var args="$rid";
    var response = await postUrl(cmd: OrientCommands.DOCUMENT, body: body);
    if (response?.statusCode == OrientStatusCodes.CREATED)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<Stream> patchDocument(
      {@required String rid, @required Map body}) async {
    var args = "$rid";
    var response =
        await patchUrl(cmd: OrientCommands.DOCUMENT, args: args, body: body);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> deleteDocument({@required String rid}) async {
    var args = "$rid";
    var response = await deleteUrl(cmd: OrientCommands.DOCUMENT, args: args);
    if (response?.statusCode == OrientStatusCodes.CONNECTED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<Stream> getDocumentByClass(
      {@required int position,
      @required String className,
      String fetchPlan}) async {
    var args =
        "$className/$position${fetchPlan.isNotEmpty ? "/$fetchPlan" : ""}";
    var response =
        await getUrl(cmd: OrientCommands.DOCUMENT_BY_CLASS, args: args);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> headDocumentByClass({
    @required int position,
    @required String className,
  }) async {
    var args = "$className/$position";
    var response =
        await headUrl(cmd: OrientCommands.DOCUMENT_BY_CLASS, args: args);
    if (response?.statusCode == OrientStatusCodes.CONNECTED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<Stream> getDocumentByIndex(
      {@required String indexName, @required String key}) async {
    var args = "$indexName/$key";
    var response = await getUrl(cmd: OrientCommands.INDEX, args: args);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> createIndex(
      {@required String indexName,
      @required String key,
      @required Map body}) async {
    var args = "$indexName/$key";
    var response =
        await putUrl(cmd: OrientCommands.INDEX, args: args, body: body);
    if (response?.statusCode == OrientStatusCodes.CREATED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<bool> deleteIndex(
      {@required String indexName, @required String key}) async {
    var args = "$indexName/$key";
    var response = await deleteUrl(cmd: OrientCommands.INDEX, args: args);
    if (response?.statusCode == OrientStatusCodes.CONNECTED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  Future<Stream> query(
      {@required String query,
      String language,
      int limit,
      String fetchPlan}) async {
    var args =
        "${language ?? OrientLanguage.SQL}/$query${limit > 0 ? "/$limit" : ""}";
    var response = await getUrl(cmd: OrientCommands.QUERY, args: args);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"'
      ]);
  }

  Future<Stream> classInfo(String className) async {
    var args = "$className";
    var response = await getUrl(cmd: OrientCommands.CLASS, args: args);
    if (response?.statusCode == OrientStatusCodes.SUCCESS)
      return response.transform(utf8.decoder);
    else
      return new Stream.fromIterable([
        '{"statusCode":${response.statusCode},"message":"${response.reasonPhrase}"}'
      ]);
  }

  Future<bool> deleteClass(String className) async {
    var args = "$className";
    var response = await deleteUrl(cmd: OrientCommands.CLASS, args: args);
    if (response?.statusCode == OrientStatusCodes.CONNECTED)
      return new Future.value(true);
    else
      return new Future.value(false);
  }

  void load() {}

  void browseCluster() {}
  void serverInfo() {}
  void schema() {}
  void security() {}
  void close() {
     OrientDB.userClient.close(force: true);
  }

  void closeApp() {
     OrientDB.appClient.close(force: true);
  }

  void changeServer() {}
  void changeDatabaseName() {}
}

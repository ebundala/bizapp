// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'orientDB.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrientTypes _$OrientTypesFromJson(Map<String, dynamic> json) {
  return OrientTypes();
}

Map<String, dynamic> _$OrientTypesToJson(OrientTypes instance) =>
    <String, dynamic>{};

OrientJsonTypes _$OrientJsonTypesFromJson(Map<String, dynamic> json) {
  return OrientJsonTypes();
}

Map<String, dynamic> _$OrientJsonTypesToJson(OrientJsonTypes instance) =>
    <String, dynamic>{};

OrientCommands _$OrientCommandsFromJson(Map<String, dynamic> json) {
  return OrientCommands();
}

Map<String, dynamic> _$OrientCommandsToJson(OrientCommands instance) =>
    <String, dynamic>{};

OrientStatusCodes _$OrientStatusCodesFromJson(Map<String, dynamic> json) {
  return OrientStatusCodes();
}

Map<String, dynamic> _$OrientStatusCodesToJson(OrientStatusCodes instance) =>
    <String, dynamic>{};

OrientLanguage _$OrientLanguageFromJson(Map<String, dynamic> json) {
  return OrientLanguage();
}

Map<String, dynamic> _$OrientLanguageToJson(OrientLanguage instance) =>
    <String, dynamic>{};

OrientBatchType _$OrientBatchTypeFromJson(Map<String, dynamic> json) {
  return OrientBatchType();
}

Map<String, dynamic> _$OrientBatchTypeToJson(OrientBatchType instance) =>
    <String, dynamic>{};

OrientUpdateMode _$OrientUpdateModeFromJson(Map<String, dynamic> json) {
  return OrientUpdateMode();
}

Map<String, dynamic> _$OrientUpdateModeToJson(OrientUpdateMode instance) =>
    <String, dynamic>{};

OrientDB _$OrientDBFromJson(Map<String, dynamic> json) {
  return OrientDB();
}

Map<String, dynamic> _$OrientDBToJson(OrientDB instance) => <String, dynamic>{};

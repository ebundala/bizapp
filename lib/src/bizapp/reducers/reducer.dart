import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/states/state.dart';

AppState reducer(AppState prevState, action){
  
   if(action is Action){
  switch(action.type){
    case APP_ACTIONS.CLEAR_ERROR:
    var newState=prevState;
    newState.errors.clear();
    return newState;
    case APP_ACTIONS.ON_ERROR:
    var newState=prevState;
    newState.errors.add(action.data);
    if(newState.errors.length>5)//only last five errors
    {
     newState.errors.removeAt(0);
    }
    return newState;
    case USER_ACTIONS.LOADING:
    var newState=prevState;
    newState.loading=true;
    return newState;
    case USER_ACTIONS.LOADING_COMPLETE:
    var newState=prevState;
    newState.loading=false;
    return newState;
    case USER_ACTIONS.REGISTER:
    case USER_ACTIONS.LOGIN:
    var newState=prevState;
    newState.user=action.data["user"];
    newState.authString=action.data["authkey"];
    return newState;   
    case USER_ACTIONS.LOGOUT:
    //var newState=prevState;
    //newState.user=null;
    //newState.authString=null;
    return AppState(); 
    case USER_ACTIONS.GET_INITIAL_CATEGORIES:
    var newState=prevState;
    if(action.data!=null){
      newState.categories.clear();
      newState.categories.addAll(action.data); 
    }
    return newState; 
    case USER_ACTIONS.GET_CATEGORIES:
    var newState=prevState;
    if(action.data!=null){
      
      newState.categories.addAll(action.data); 
    }
    return newState;
    
    case USER_ACTIONS.GET_INITIAL_PRODUCTS:
    var newState=prevState;
    if(action.data!=null){
      newState.products.clear();
      newState.products.addAll(action.data); 
    }
    return newState; 
    case USER_ACTIONS.GET_PRODUCTS:
    var newState=prevState;
    if(action.data!=null){
      
      newState.products.addAll(action.data); 
    }
    return newState;
    case PRODUCT_ACTIONS.RELOAD_PRODUCT:
    var newState=prevState;
    if(action.data!=null&&action.data["index"]!=null&&action.data["product"]!=null){
       var i=action.data["index"];
      if(i<newState.products.length){       
         newState.products.replaceRange(i, i+1,[action.data["product"]]);
         return newState;
      }
    }
    return prevState;

    case USER_ACTIONS.GET_INITIAL_PEOPLE_TO_FOLLOW:
    var newState=prevState;
    if(action.data!=null){
      newState.peopleTofollow.clear();
      newState.peopleTofollow.addAll(action.data); 
    }
    return newState; 
    case USER_ACTIONS.GET_PEOPLE_TO_FOLLOW:
    var newState=prevState;
    if(action.data!=null){
      
      newState.peopleTofollow.addAll(action.data); 
    }
    return newState;
    case USER_ACTIONS.RELOAD_PEOPLE_TO_FOLLOW:
    var newState=prevState;
    if(action.data!=null&&action.data["index"]!=null&&action.data["product"]!=null){
       var i=action.data["index"];
      if(i<newState.peopleTofollow.length){       
         newState.peopleTofollow.replaceRange(i, i+1,action.data["product"]);
         return newState;
      }
    }
    return prevState;

    case USER_ACTIONS.GET_INITIAL_LIKES_FEED:
    var newState=prevState;
    if(action.data!=null){
      newState.likesFeed.clear();
      newState.likesFeed.addAll(action.data); 
    }
    return newState; 
    case USER_ACTIONS.GET_LIKES_FEED:
    var newState=prevState;
    if(action.data!=null){      
      newState.likesFeed.addAll(action.data); 
    }
    return newState;
    case USER_ACTIONS.RELOAD_LIKES_FEED:
    var newState=prevState;
    if(action.data!=null&&action.data["index"]!=null&&action.data["product"]!=null){
       var i=action.data["index"];
      if(i<newState.likesFeed.length){       
         newState.likesFeed.replaceRange(i, i+1,action.data["product"]);
         return newState;
      }
    }
    return prevState;

    case USER_ACTIONS.GET_INITIAL_FOLLOWS_FEED:
    var newState=prevState;
    if(action.data!=null){
      newState.followsFeed.clear();
      newState.followsFeed.addAll(action.data); 
    }
    return newState; 
    case USER_ACTIONS.GET_FOLLOWS_FEED:
    var newState=prevState;
    if(action.data!=null){      
      newState.followsFeed.addAll(action.data); 
    }
    return newState;
case USER_ACTIONS.RELOAD_FOLLOWS_FEED:
    var newState=prevState;
    if(action.data!=null&&action.data["index"]!=null&&action.data["product"]!=null){
       var i=action.data["index"];
      if(i<newState.followsFeed.length){       
         newState.followsFeed.replaceRange(i, i+1,action.data["product"]);
         return newState;
      }
    }
    return prevState;

    case USER_ACTIONS.BUY:
    case USER_ACTIONS.COMMENT:
    case USER_ACTIONS.LIKE_COMMENT:
    case USER_ACTIONS.POST:
    case USER_ACTIONS.LIKE_POST:
    case USER_ACTIONS.REVIEW_STORE:
    case USER_ACTIONS.SELL_PRODUCT:
    case USER_ACTIONS.LIKE_PRODUCT:
    
    case USER_ACTIONS.OWN_STORE:
    case USER_ACTIONS.FOLLOW:
    case USER_ACTIONS.UNFOLLOW:
     break;
    case APP_ACTIONS.TITLE_CHANGED:
      var newState=prevState;
      newState.title=action.data;
      return newState;
    case APP_ACTIONS.PAGE_INDEX_CHANGED:
      var newState=prevState;
      newState.pageTabIndex=action.data;
      return newState;
    default:
    return prevState;
  }
   
  }
  return prevState;
}
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class EbIconButton extends StatelessBizappWidget {
  EbIconButton(
      {this.child,
      this.text = "",
      this.name = "",
      this.textColor,
      this.axis = Axis.vertical});
  final IconButton child;
  final String text;
  final String name;
  final Color textColor;
  final Axis axis;
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context).textTheme;
    var horizontal = Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(name,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: theme.caption.fontSize * 0.75,
                color: textColor ?? theme.caption.color)),
        child,
        Text(text,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: theme.caption.fontSize * 0.75,
                color: textColor ?? theme.caption.color)),
      ],
    );

    var vertical = Stack(
      children: <Widget>[
        child,
        Positioned(
          left: 0.0,
          right: 0.0,
          top: 0.0,
          child: Text(text,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: theme.caption.fontSize * 0.75,
                  color: textColor ?? theme.caption.color)),
        ),
        Positioned(
          left: 0.0,
          right: 0.0,
          bottom: 0.0,
          child: Text(name,
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: theme.caption.fontSize * 0.75,
                  color: textColor ?? theme.caption.color)),
        )
      ],
    );

    return axis == Axis.vertical ? vertical : horizontal;
  }
}

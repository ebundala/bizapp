import 'dart:math';

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:bizapp/src/bizapp/states/state.dart';

class UserTile extends StatelessBizappWidget {
  final OUser user;
  final i;
  UserTile({this.user,this.i});

  String getAvator(){
    var images=user.getField("avator");
    return images!=null?images:"http://i.pravatar.cc/150?u=${user.getField("name")}";
  }
 
  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var textTheme = Theme.of(context).textTheme;
    
    return Container(
      width: (screenWidth / 2) - 36,
      height: 60.0,
      child: Card(
          elevation: 3.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                  radius: (screenWidth / 4) - 36,
                  backgroundImage: CachedNetworkImageProvider(getAvator())
                  ),
              Text(
                "${user.getField("name")}",
                style: textTheme.caption,
              ),
              StoreConnector<AppState,AppState>(
                converter: (store)=>store.state,
                builder:(context,state){
              return RaisedButton(
                child: Text("Follow"),
                onPressed: () async{
                 await getUser().follow(username: user.getField("name")).then((res)async{
                    await getUser().getPeopleToFollow();
                    await getUser().getFollowsFeed();
                    await getUser().getLikesFeed();
                  });

                },
              );
              })
            ],
          )),
    );
  }
}

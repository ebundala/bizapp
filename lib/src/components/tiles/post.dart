import 'dart:math';

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PostTile extends StatelessBizappWidget {
  final OLike post;
  final int i;
  PostTile({this.post,this.i});
  String getAvator(){
    var images=post.getField("likerAvator");
    return images!=null?images:"http://i.pravatar.cc/150?u=${post.getField("likerName")}";
  }
  String getPostTitle(){
    var liker=post.getField("likerName");
    var item = post.getField("likedItem");
    var itemType=post.getField("likedItemClass");
    var word;
    switch(itemType){
      case "OUser":
      word="$item's profile";
      break;
      case "OProduct":
      word ="a product '$item'";
      break;
      case "OStore":
      word ="a Store '$item'";
      break;
      default:
      word=" $item";
    }
    return "@$liker liked $word";
  }
  @override
  Widget build(BuildContext context) {
    var textTheme = Theme.of(context).textTheme;
    //int rn = Random().nextInt(5) + 1;
    return Card(
        elevation: 3.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              fit: FlexFit.tight,
              flex: 4,
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: CachedNetworkImage(
                  imageUrl:getAvator(),
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            Flexible(
              flex: 8,
              fit: FlexFit.tight,
              child: Container(
                  padding: EdgeInsets.only(top: 8.0, bottom: 8.0, right: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${getPostTitle()}",
                        style: textTheme.title,
                      ),
                      /*style:TextStyle(fontSize: textTheme.headline.fontSize*0.75),),*/
                      SizedBox(
                        height: 4.0,
                      ),
                      Text(
                        "message tag here message tag here message tag here message tag here message tag here ",
                        style: textTheme.body1,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        height: 8.0,
                      ),
                      Text(
                        "2 days ago",
                        style: textTheme.caption,
                      )
                    ],
                  )),
            )
          ],
        ));
  }
}

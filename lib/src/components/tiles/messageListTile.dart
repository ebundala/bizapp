
import 'dart:math';

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';


class MessageListTile extends StatelessBizappWidget{


 void menuItemSelected(String value){
   switch(value){
     case "mute":
       print(value);
     break;
     case "archive":
       print(value);
       break;
     case "delete":
       print(value);
       break;
     default:
       print(value);

   }

 }
  @override
  Widget build(BuildContext context){
    return ListTile(
      leading: CircleAvatar(
          backgroundColor: Colors.grey,
          radius: 24.0,
          backgroundImage: AssetImage('images/portfolio_${Random().nextInt(5)+1}.jpeg')),
      title: Text("conversation name"),
      subtitle: Text("some hint text here..."),
      trailing: PopupMenuButton(icon:Icon(Icons.more_vert),
          onSelected: menuItemSelected,
          itemBuilder: (context){
        return [
          PopupMenuItem(
            value: "mute",
            child: Text("Delete"),
          ),
          PopupMenuItem(
            value:"archive",
            child: Text("Archive"),
          ),
          PopupMenuItem(
            value:"mute",
            child: Text("Delete"),
          ),

        ];
      }),
    );
  }
}
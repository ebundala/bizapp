//import 'dart:math';

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/components/tiles/ActionButton.dart';
import 'package:flutter/material.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ProductTile extends StatefulWidget {
  final OProduct product;
  final int i;
  final Future<bool> likeItem;
  final VoidCallback addTocart;
  final VoidCallback viewed; 
 ProductTile(this.product,{this.i,this.addTocart,this.likeItem,this.viewed});
  
  @override
  ProductTileState createState() => ProductTileState();
}

class ProductTileState extends BizappState<ProductTile> {
  
  
  
@override
void initState(){
  super.initState();
  print("liked ${widget.product.getField("liked")} item ${widget.i}");
}
  String getImage(){
    var images= widget.product.getField("photos");
    return images!=null?images[0]:"https://via.placeholder.com/150.png";
  }
  String getPrice(){
    var price=widget.product.getField("price");
    var currency=widget.product.getField("currency");
    
    return "${price??0.00} ${currency??"Tsh"}";
  }
  String getLikes(){
    var likes=widget.product.getField("in_OLike");
    return likes!=null?"${likes.length}":"0";
  }
  String getViews(){
    var views=widget.product.getField("in_OView");
    return views!=null?"${views.length}":"";
  }
  String getLocation(){
    var region=widget.product.getField("region");
    var district=widget.product.getField("district");
    return "$region $district";
  }
  bool isLiked(OProduct product,int i){
    return product.getField("liked")==0?false:true;

  }
  bool isViewed(){
    return widget.product.getField("viewed")==0?false:true;
  }
  bool isInCart(){
    
  }
  bool isIsell(){
        return widget.product.getField("selling")==0?false:true;
  }
  bool isBought(){
    return widget.product.getField("bought")==0?false:true;

  }
  String getAvator(){
    var images=widget.product.getField("avator");
    return images!=null?images:"http://i.pravatar.cc/150?u=${widget.product.getField("username")}";
  }
Future<bool>likeItem(OProduct product,i){
    if(product.getField("liked")==0){
    return getUser().like(item: product).then((res){
      
     return product.reflesh(i:i,urid: getUser().rid);
    }).then((res){
      return true;
    });
    }
    else{
      var like= new OLike()
      ..from=getUser().rid
      ..to=product.rid;
      return like.delete().then((res){          
      }).then((res){
     return product.reflesh(i:i,urid: getUser().rid);
    }).then((res){
      return false;
    });
    }
  }
  
  
  addTocart(){}
    viewOnMap()async {
      var lat=widget.product.getField("latitude");
      var lon=widget.product.getField("longitude");
      var tolat=getUser().getField("latitude");
      var tolon=getUser().getField("longitude");
      var loc= await Navigator.of(context).pushNamed("map/$lat/$lon/$tolat/$tolon");
    }
  @override
  build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    //var subheadStyle =textTheme.subhead.copyWith(color: const Color(0xBBFFFFFF));
    var screenWidth = MediaQuery.of(context).size.width;
   
    return Card(
      elevation: 3.0,
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(8.0),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      CircleAvatar(
                        radius: textTheme.subhead.fontSize,
                        backgroundImage:
                            NetworkImage(getAvator()),
                      ),
                      SizedBox(
                        width: 8.00,
                      ),
                      // Container(
                      // padding: EdgeInsets.only(left:4.0, top:0.0, right:8.0, bottom:20.0),
                      //  child:
                      Text(
                        "${widget.product.getField("username")}",
                        style: textTheme.caption,
                      )
                    ],
                  ),

                  //   ,)
                ]),
          ),
          Stack(
            children: <Widget>[
              CachedNetworkImage(
               // placeholder: CircularProgressIndicator(),
                errorWidget: Icon(Icons.error),
                imageUrl:"${getImage()}",
                height: 200.0,
                width: screenWidth - 24,
                fit: BoxFit.fill,
              ),
              Positioned(
                bottom: 0.0,
                left: 0.0,
                child: Opacity(
                  opacity: 0.40,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.black,
                      /*gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [Colors.black, Colors.black])*/
                    ),
                    height: 48.0,
                    width: screenWidth,
                    child: null,
                  ),
                ),
              ),
              Positioned(
                  width: screenWidth - 36,
                  top: 146.0,
                  left: 8.0,
                  // right: 8.0,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                          flex: 8,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "${widget.product.getField("name")}",
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: textTheme.subhead.fontSize,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                "${getPrice()}",
                                style: TextStyle(
                                    fontSize: textTheme.subhead.fontSize * 0.75,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          )),
                      Flexible(
                        flex: 4,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            EbIconButton(
                                axis: Axis.vertical,
                                textColor: Colors.white,
                                name: "${getLocation()}",
                                child: IconButton(
                                  // padding: EdgeInsets.all(0.0),
                                  iconSize: textTheme.subhead.fontSize,
                                  icon: Icon(
                                    Icons.location_on,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                     viewOnMap();
                                  },
                                ))
                          ],
                        ),
                      )
                    ],
                  )),
            ],
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                EbIconButton(
                    text: "${getLikes()}",
                    name: "Like",
                    child: IconButton(
                        icon: Icon(Icons.thumb_up),
                        color: isLiked(widget.product,widget.i)?Theme.of(context).accentColor:Colors.black12,
                        onPressed: () {
                           //widget.likeItem;
                            likeItem(widget.product, widget.i);
                           
                           // setState((){});
                             
                            
                           
                        })),
                EbIconButton(
                  text: "${getViews()}",
                  name: "views",
                  child: IconButton(
                      icon: Icon(Icons.traffic),
                      color: isViewed()?Theme.of(context).accentColor:Colors.black12,
                      onPressed: () {}),
                ),
                EbIconButton(
                  //text: "500",
                  name: "Message",
                  child: IconButton(
                    icon: Icon(Icons.mail_outline),
                    color: Colors.black12,
                    onPressed: () {
                      Navigator.of(context).pushNamed("chats");
                    },
                  ),
                ),
                EbIconButton(
                    //text: "500",
                    name: "Add to cart",
                    child: IconButton(
                      icon: Icon(Icons.add_shopping_cart),
                      color: Colors.black12,
                      onPressed: () {
                        print("add to cart item ${widget.product.rid}");
                      },
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}

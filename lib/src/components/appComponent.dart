import 'dart:async';
import 'dart:convert';

import 'package:bizapp/src//components/screens/WelcomeScreen.dart';
import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:bizapp/src/bizapp/common/orientDB.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/reducers/reducer.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/components/screens/HomeScreen.dart';
import 'package:bizapp/src/components/screens/SplashScreen.dart';
import 'package:bizapp/src/routes/application.dart';
import 'package:bizapp/src/routes/routes.dart';
import 'package:fluro/fluro.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:logging/logging.dart';
import 'package:redux/redux.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
//import 'package:bizapp/src/bizapp/actions/Actions.dart';

final ThemeData kIOSTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.white,
    // primaryColor: Colors.cyan[100],
    primaryColorBrightness: Brightness.light,
    accentColor: Colors.pinkAccent[200]);

final ThemeData kDefaultTheme = ThemeData(
    brightness: Brightness.light,
    primarySwatch: Colors.amber,
    accentColor: Colors.pinkAccent[200]);

class AppComponent extends StatefulWidget {
  @override
  _AppComponentState createState() {
    return _AppComponentState();
  }
}

class _AppComponentState extends State<AppComponent> {
  static MethodChannel platform =
      const MethodChannel('channel:com.ebundala.bizapp/deeplink');
  static Router router;
  StreamSubscription logSubscription;
  Store<AppState> store;

  final persistor = Persistor<AppState>(
     // version: 1,
      storage: FlutterStorage(key:"bizapp"),
      serializer: JsonSerializer<AppState>(AppState.fromJson),
      debug: true);

  _AppComponentState() {
    router = Router();
    Routes.defineRoutes(router);
    Application.router = router;
    configureDeepLinker();
    print("Configured channel receiver in flutter ..");
  }

  void configureDeepLinker() {
    platform.setMethodCallHandler((MethodCall call) async {
      if (call.method == "linkReceived") {
        Map<String, dynamic> passedObjs = call.arguments;
        if (passedObjs != null) {
          var path = passedObjs["path"];
          Application.router.navigateTo(context, path);
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
    /*var log = new Logger("Bizapp");
    logSubscription = log.onRecord.listen((txt) {
      var str;
     
      print(str);
    });

    var app = new AppBase(className: "bizApp");
    var initialState = new AppState();
    AppBase.store =
        new Store<AppState>(reducer, initialState: initialState, middleware: [
      persistor.createMiddleware(),
      LoggingMiddleware<AppState>.printer(
          logger: log,
          formatter: (appState, action, time) => jsonEncode({
                "action":
                    action is Action ? action?.toJson() : action.toString(),
                "state": appState?.toJson(),
                "time": time?.toString()
              }))
    ]);

       var db = new OrientDB(
        appKey: "bizapp",
        appSecret: "kiazi",
        port: 80,
        url: "http://sleepy-gorge-79790.herokuapp.com");

        var db = new OrientDB(
        appKey: "bizapp",
        appSecret: "kiazi",
       // port: 80,
        url: "http://192.168.137.1");
    
    app.config(db: db, store: AppBase.store);*/
  }

  @override
  void dispose() async {
    super.dispose();
    //await logSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: AppBase.store,
        child: MaterialApp(
            title: "BizApp",
            onGenerateRoute: router.generator,
            theme: defaultTargetPlatform == TargetPlatform.iOS
                ? kIOSTheme
                : kDefaultTheme,
            home:/* FutureBuilder(
                future: persistor.load().then((state)async{
                  print(state);
                  OUser user=state?.user;
                  var authString=state?.authString;
                  
                 var res= await user?.login(username: user?.getField("name"), password: authString);
                   await user.getCategories(isInitial: true);
                   await user.getProducts(isInitial: true);
                 return Future.value(res);
                }), *
                builder: (context, snapshot) {
                  
                    return */StoreConnector<AppState, OUser>(
                        converter: (store) => store.state?.user,
                        builder: (context, user) {
                          var username = user?.getField("name");
                          bool logedIn =((username != null) && (username != "bizapp"));

                             // print("user login state $logedIn");
                          return logedIn
                              ? HomeScreen(title: "Shop")
                              : WelcomeScreen(
                                  title: "Sell anything. Try Bizapp");
                        })
                  
                //})

            )

        );
  }
}

import 'package:bizapp/src/bizapp/actions/Actions.dart';
import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:flutter/material.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
import 'package:bizapp/src/bizapp/controllers/OCategory.dart';

class StatelessBizappWidget extends StatelessWidget {

  
  
  void showLoadingDialog(bool state, BuildContext context, {String msg}) {
    
    
    if (state) {
      
      showDialog(
        barrierDismissible: false,
          context: context,
          builder: (context) => Center(
                  child: Container(
                    padding: EdgeInsets.all(24.0),
                    height: 150.00,
                   // 
                    child:Card(
                     // padding: EdgeInsets.all(24.0),
                      //color: Colors.white,
                      child:Row(
                children: <Widget>[
                Padding(padding: EdgeInsets.symmetric(horizontal:16.0),
                child:SizedBox(width: 38.0,height: 38.0 ,child: CircularProgressIndicator(),),),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child:Text(msg,
                  style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.normal,color: Colors.black),) 
                   )
                  ]
                  )
              )
              )
              )
        );
    } else {
      
      goBack(context);
    }
  }
  void changeAppTitle(String title){

    AppBase().dispatch(Action(type: APP_ACTIONS.TITLE_CHANGED,data: title));
   }
  void changePageIndex(int index){

    AppBase().dispatch(Action(type: APP_ACTIONS.PAGE_INDEX_CHANGED,data: index));
  }
  bool goBack(BuildContext context) {
   // if (Navigator.of(context).canPop())
     Navigator.of(context).pop();//popUntil((route)=>!(route.isCurrent&&route.isActive));
    return false;
  }
  AppError getLastError(){
    var error=AppBase.store?.state?.errors?.last;
    if(error!=null){
      return error;
    }
    else{
      return AppError();
    }
  }
  List<OProduct> getProducts(){
    return AppBase.store?.state?.products??[];
  }
  
  List<OCategory>getCategories(){
    return AppBase.store?.state?.categories??[];
  }

  OUser getUser(){
    return AppBase.store?.state?.user;
  }
  void showMessageDialog(bool state, BuildContext context, {String msg}) {
    if (state) {
      showDialog(
          context: context,
          builder: (context) => Center(
                  child: Row(children: <Widget>[
                CircularProgressIndicator(),
                Text("$msg")
              ])));
    } else {
      goBack(context);
    }
  }
  
  @override
  Widget build(BuildContext context) => null;
}

class BizappState<T extends StatefulWidget> extends State<T> {
  bool loadingState = false;
  String loadingText="Loading...";
  @override
  Widget build(BuildContext context) {
    return null;
  }
  void loading(bool state) {
    setState(() {
      loadingState = state;
    });
  }
  void showSnackbar({@required Widget content, SnackBarAction action}){
    Scaffold.of(context).showSnackBar(SnackBar(content: content,action: action,));
  }
  void showAlertDialog({Widget title,Widget content,List<Widget> actions}){
    showDialog(context: context,
                              builder:(ctx)=>                                                               
                                AlertDialog(
                                title: title,
                                content: content,
                                actions: actions
                              )
                              
                              );
  }
  void changeLoadingText(String msg){
    if(msg!=null){
      setState(() {
              loadingText=msg;
            });
    }
  }
  void showLoadingDialog(bool state, BuildContext context, {String msg}) {
    if(msg!=null){
      setState(() {
              loadingText=msg;
            });
    }
    
    if (state) {
      
      showDialog(
        barrierDismissible: false,
          context: context,
          builder: (context) => Center(
                  child: Container(
                    padding: EdgeInsets.all(24.0),
                    height: 150.00,
                   // 
                    child:Card(
                     // padding: EdgeInsets.all(24.0),
                      //color: Colors.white,
                      child:Row(
                children: <Widget>[
                Padding(padding: EdgeInsets.symmetric(horizontal:16.0),
                child:SizedBox(width: 38.0,height: 38.0 ,child: CircularProgressIndicator(),),),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child:Text(loadingText,
                  style: TextStyle(fontSize: 14.0,fontWeight: FontWeight.normal,color: Colors.black),) 
                   )
                  ]
                  )
              )
              )
              )
        );
    } else {
      
      goBack(context);
    }
  }
  void changeAppTitle(String title){

    AppBase().dispatch(Action(type: APP_ACTIONS.TITLE_CHANGED,data: title));
   }
  void changePageIndex(int index){

    AppBase().dispatch(Action(type: APP_ACTIONS.PAGE_INDEX_CHANGED,data: index));
  }
  bool goBack(BuildContext context) {
   // if (Navigator.of(context).canPop())
     Navigator.of(context).pop();//popUntil((route)=>!(route.isCurrent&&route.isActive));
    return false;
  }
  AppError getLastError(){
    var error;
    try{
    error=AppBase.store?.state?.errors?.last;
    }
    catch (e){

    }
    if(error!=null){
      return error;
    }
    else{
      return AppError();
    }
  }
  List<OProduct> getProducts(){
    return AppBase.store?.state?.products??[];
  }
  List<OCategory>getCategories(){
    return AppBase.store?.state?.categories??[];
  }
  OUser getUser(){
    return AppBase.store?.state?.user;
  }
  
}

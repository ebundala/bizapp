import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/components/tiles/messageListTile.dart';
import 'package:bizapp/src/bizapp/controllers/OMessage.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';

class Messages extends StatefulWidget {
  ScrollController scroller;
  static final TabItem = BottomNavigationBarItem(
      icon: Icon(Icons.mail), title: Text("messages"));
  Messages({ScrollController scroller}) : scroller = scroller;
  @override
  MessagesState createState() => MessagesState();
}

class MessagesState extends BizappState<Messages> {
  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    //var textTheme = theme.textTheme;
    //var subheadStyle =textTheme.subhead.copyWith(color: const Color(0xBBFFFFFF));
   // var screenWidth = MediaQuery.of(context).size.width;

    return StoreConnector<AppState,List<OMessage>>(
      converter:(store)=>store.state.messages,
      builder:(context,messages){
     return RefreshIndicator(
          onRefresh: ()async{
             await getUser().getMessages();
             
             },
    child:
     ListView(
      controller: widget.scroller,
      children: <Widget>[
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
        MessageListTile(),
      ],
    ));
      });
  }
}

import 'dart:async';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/components/tiles/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/bizapp/controllers/OCategory.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Shop extends StatefulWidget {
  ScrollController scroller;
  static final TabItem =
      BottomNavigationBarItem(icon: Icon(Icons.shop), title: Text("shop"));
  Shop({ScrollController scroller}) : scroller = scroller;
   
  @override
  ShopState createState() => ShopState();
}

class ShopState extends BizappState<Shop> {
  int activeCategory;
  StreamController<int> loadingCategoryStream=StreamController<int>();
  StreamController<int> loadingProductStream=StreamController<int>();
  bool loadingCategoryState=false;
  bool loadingProductState=false;
  final gridKey= GlobalKey<State>();

  @override
  void initState() {
      super.initState();
      loadingCategoryStream.stream.listen((i){
        print("loading category from $i");
        getUser().getCategories(isInitial: false,skip: i+1,limit: 5).then((res){
          if((!res.hasError)&&(res.result.length!=0)){
          loadingCategoryState=false;
          }
          else{
            loadingCategoryState=true;
          }
        }).catchError((e){
          Future.delayed(Duration(seconds: 10),(){
           loadingCategoryState=false;
          });
          
        });
      });

      loadingProductStream.stream.listen((i){
        print("loading product from $i");
        getUser().getProducts(isInitial: false,skip: i+1,limit: 5).then((res){
          if((!res.hasError)&&(res.result.length!=0)){
          loadingProductState=false;
          }
          else{
            
            loadingProductState=true;
          }
        }).catchError((e){
          Future.delayed(Duration(seconds: 10),(){
             loadingProductState=false;
          });
          
        });
      });
      
    }
    Future<bool>likeItem(OProduct product,i){
    if(product.getField("liked")==0){
    return getUser().like(item: product).then((res){
     return product.reflesh(i:i,urid: getUser().rid);
    }).then((res){
      return true;
    });
    }
    else{
      var like= new OLike()
      ..from=getUser().rid
      ..to=product.rid;
      return like.delete().then((res){          
      }).then((res){
     return product.reflesh(i:i,urid: getUser().rid);
    }).then((res){
      return false;
    });
    }
  }
    @override
    void dispose() {
        loadingCategoryStream.close();
        loadingProductStream.close();
        super.dispose();

      }
  @override
  Widget build(BuildContext context) {
    return  StoreConnector<AppState,List<OProduct>> (
      converter: (store)=>store.state.products,
      builder: (context,products){

      return
      RefreshIndicator(
          onRefresh: ()async{
             await getUser().getProducts(limit:getProducts().length );
             await getUser().getCategories(limit: getCategories().length);
             },
    child:CustomScrollView(
      key: gridKey,
      controller: widget.scroller,
      slivers: <Widget>[
        SliverAppBar(
          primary: false,
          floating: false,
          elevation: 0.0,
          expandedHeight: 50,
          backgroundColor: Theme.of(context).bottomAppBarColor,
          flexibleSpace: StoreConnector<AppState,List<OCategory>>(
            converter: (store)=>store.state.categories,
           builder:(context,categories)=> ListView.custom(
            scrollDirection: Axis.horizontal,
            childrenDelegate: SliverChildBuilderDelegate((context,i){
            if(((i+1)==categories.length)&&(loadingCategoryState==false)){
              //print("reached end of categories at $i");
              loadingCategoryState=true;
             loadingCategoryStream.add(i);
            }

           return Padding(
              padding: EdgeInsets.symmetric(horizontal: 4.0),
              child:ChoiceChip(
                avatar: CircleAvatar(backgroundImage:CachedNetworkImageProvider("http://i.pravatar.cc/150?u=${categories[i].getField("categoryName")}") ),
                padding: EdgeInsets.all(4.0),
              label: Text("${categories[i].getField("categoryName")}"),
              selected: activeCategory==i,
              onSelected:(s){
                 if(s){
                   setState(() {
                              activeCategory=i;
                              });
                 }
              }));
              },childCount: categories.length),
          ),
        ),
        ),
        SliverGrid(
            delegate: SliverChildBuilderDelegate((context,i){
              
              if(((i+1)==getProducts().length)&&(loadingProductState==false)){
              print("reached end of products at $i");
              loadingProductState=true;
             loadingProductStream.add(i);
            }
             return StoreConnector<AppState,OProduct>(
               converter: (store)=>store.state.products[i],
               builder:(context,item)=>ProductTile(item,i: i)
               );
              },childCount: products.length),
            gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 360.0,childAspectRatio: 1.1,))
      ],

    ));
    }
    );

    
  }
}

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/bizapp/common/responses.dart';
import 'package:bizapp/src/components/tiles/user.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Community extends StatefulWidget {
  ScrollController scroller;
  static final TabItem = BottomNavigationBarItem(
      icon: Icon(Icons.people), title: Text("community"));
  Community({ScrollController scroller}) : scroller = scroller;
  @override
  CommunityState createState() => CommunityState();
}

class CommunityState extends BizappState<Community> {
  
  String getFollowFeedTitle(OFollow feed){
    var type=feed.getField("followItemClass");
    var follower=feed.getField("followerName");
    var followed=feed.getField("followedItem");
    var following=feed.getField("following");
    
var word="";
    switch(type){
      case "OUser":
      if(following>0){
        word="You started following '$followed' ";
      }else{
      word="$follower started following you";
      }
      break;
      case "OStore":
      break;
      default:
    }
    return word;
  }
  bool isAlreadyFollowed(OFollow feed){
    var type=feed.getField("followItemClass");
          //var follower=feed.getField("followerName");
        // var followed=feed.getField("followedItem");
          var following=feed.getField("following");
          var followedBack=feed.getField("followedBack");
          
          switch(type){
            case "OUser":
            if(following==0&&followedBack==1){
              
              return true;
            }else{
            return false;
            }
            break;
            case "OStore":
            return true;
            break;
            default:
          }
          return false;
  }
 String getFeedTextAction(OFollow feed){
      var type=feed.getField("followItemClass");
          //var follower=feed.getField("followerName");
        // var followed=feed.getField("followedItem");
          var following=feed.getField("following");
          var followedBack=feed.getField("followedBack");
      var word="";
          switch(type){
            case "OUser":
            if(following>0){
              word="Unfollow";
            }else{
              if(followedBack==1){
                word="Following..";
              }else{
            word="Follow";
              }
            }
            break;
            case "OStore":
            break;
            default:
          }
          return word;
  }
  Future<ApiResponse> performAction(OFollow feed){
    var type=feed.getField("followItemClass");
          var follower=feed.getField("followerName");
         var followed=feed.getField("followedItem");
          var following=feed.getField("following");
      
          switch(type){
            case "OUser":
            if(following>0){
             return getUser().unfollow(username:followed );
            }else{
           return getUser().follow(username:follower);
            }
            break;
            case "OStore":
            break;
            default:
          }
          return Future((){});
          
  }
  
  String getAvator(OFollow feed){
    var images=feed.getField("followerAvator");
    return images!=null?images:"http://i.pravatar.cc/150?u=${feed.getField("followerName")}";
  }
  String getTimeAgo(OFollow feed){
    var tm=feed.getField("createdAt");
    int timeAgo=0;
    String text="";

    if(tm!=null){
    var date=DateTime.parse(tm);
    var diff=date.toLocal().difference(DateTime.now());
      if(-diff.inDays>=364){
        timeAgo= (-(diff.inDays)/364).ceil();
        text="Years ago";
      }
      else if(-diff.inDays>=30)
      {
      timeAgo= (-(diff.inDays)/30).ceil();
      text="Months ago";
      }
      else if(-diff.inDays>=1){
        timeAgo =(-diff.inDays);
        text="Days ago";
      }
      else if(-diff.inHours>=1){
        timeAgo=(-diff.inHours);
         text="Hours ago";
      }
      else if(-diff.inMinutes>=1){
        timeAgo=(-diff.inMinutes);
         text="Minutes ago";
      }
      else if (-diff.inSeconds>=1) {
        timeAgo=(-diff.inSeconds);
         text="Seconds ago";
      }
      else{
        text="just now";
      }
    
    }
    return "${timeAgo>0?timeAgo:""} $text";
  }
 
  @override
  Widget build(BuildContext context) {
   // var theme = Theme.of(context);
    //var textTheme = theme.textTheme;
    //var subheadStyle =textTheme.subhead.copyWith(color: const Color(0xBBFFFFFF));
    var screenWidth = MediaQuery.of(context).size.width;

    return StoreConnector<AppState,AppState>(
      converter:(store)=>store.state,
      builder:(context,state){
        return RefreshIndicator(
          onRefresh: ()async{
            await getUser().getPeopleToFollow();
            await getUser().getFollowsFeed();
            await getUser().getLikesFeed();
          },
    child: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          primary: false,
          backgroundColor: Colors.transparent,
          floating: false,
          elevation: 0.0,
          expandedHeight: 200.0,
          flexibleSpace: Container(
              //width: (screenWidth - 36),
             // height: 200.00,
              child: StoreConnector<AppState,List<OUser>>(
                
                converter: (store)=>store.state.peopleTofollow,
                builder:(context,people)=>ListView.custom(
                scrollDirection: Axis.horizontal,
                childrenDelegate: SliverChildBuilderDelegate((context,i){
                  if(people?.length==(i+i)){
                    //todo load more here
                    print("load more people to follow");
                  }
                  if(i<people?.length){
                  return UserTile(user:people[i],i:i);
                  }

                },childCount: people?.length??0),
              ))
              ) ,
        ),
        SliverList(
            delegate: SliverChildBuilderDelegate((context,i){
              var feed=state.followsFeed;
              if(feed.length==(i+1)){
                //todo load more here
              }
              if(i<feed.length)
              return ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.red,
                  backgroundImage: CachedNetworkImageProvider(getAvator(feed[i])),
                ),
                title:Text("${getFollowFeedTitle(feed[i])}") ,
                subtitle: Text("${getTimeAgo(feed[i])}"),
                onTap: (){
                  print("visit profile of ${feed[i].getField("followerName")}");
                },
                trailing: FlatButton(
                  
                  onPressed:isAlreadyFollowed(feed[i])!=true?()async{
                     await performAction(feed[i]).then((res)async{
                      await getUser().getPeopleToFollow();
                      await getUser().getFollowsFeed();
                     });
                  }:null,
                  child: Text("${getFeedTextAction(feed[i])}"),
                ),
              );
            },childCount: state.followsFeed.length),
           // gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 360.0,childAspectRatio: 1.1)
           ),
        
      ],
    )
    );});
        
        
     /* Container(
        // color:Colors.grey,
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 8.0),
            Text(
              "people to follow",
              style: TextStyle(
                  fontSize: textTheme.title.fontSize * 0.75,
                  fontWeight: textTheme.title.fontWeight),
            ),
            SizedBox(height: 8.0),
            Container(
                width: (screenWidth - 36),
                height: 200.00,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    UserTile(),
                    UserTile(),
                    UserTile(),
                    UserTile(),
                    UserTile(),
                    UserTile(),
                    UserTile(),
                    UserTile(),
                    UserTile()
                  ],
                )),
            // TabBar()
          ],
        ));*/
  }
}

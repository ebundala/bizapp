import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
//import 'package:bizapp/src/components/pages/shop.dart';
import 'package:bizapp/src/bizapp/controllers/OPost.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:bizapp/src/components/tiles/post.dart';
import 'package:bizapp/src/bizapp/controllers/OEdge.dart';
import 'package:flutter/material.dart';

class Trending extends StatefulWidget {
  ScrollController scroller;
  static final TabItem = BottomNavigationBarItem(
      icon: Icon(Icons.trending_up), title: Text("trending"));
  Trending({ScrollController scroller}) : scroller = scroller;

  @override
  TrendingState createState() => TrendingState();
}

class TrendingState extends BizappState<Trending> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState,List<OLike>>(
      converter:(store)=>store.state.likesFeed,
      builder:(context,likes){
        return RefreshIndicator(
          onRefresh: ()async{
            await getUser().getLikesFeed(limit: likes?.length);
          },
          child: ListView.custom(
        controller: widget.scroller,
        padding: const EdgeInsets.only(left: 4.0, right: 8.0, bottom: 4.0),
        childrenDelegate: SliverChildBuilderDelegate((context,i){
          if(likes?.length==(i+1)){
            //todo load more
          }
          if(i<likes.length){
            return PostTile(post:likes[i],i:i);
          }
        },childCount: likes?.length??0),
       ),
        );
        
      }
    );
     
  }
}

import 'package:flutter/material.dart';
import 'package:bizapp/src/bizapp/controllers/OCategory.dart';
import 'package:bizapp/src/bizapp/controllers/OLocation.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
import 'package:bizapp/src/components/forms/productForm.dart';

class CreateProductsScreen extends StatefulWidget {
  CreateProductsScreen({this.title = "Create Product"});
  final String title;
  @override
  State createState() => CreateProductsScreenState();
}

class CreateProductsScreenState extends State<CreateProductsScreen> {
  CreateProductsScreenState();
  //OProduct product=OProduct();
  //OCategory category=OCategory();
  //OLocation location=OLocation();

  final formkey=GlobalKey<FormState>();

  int step=0;

 
  @override
  Widget build(BuildContext context) {

    

   /* final submitButtons=Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        RaisedButton(
            child: Text("Publish"),
            onPressed: step>=4?(){
              if(formkey.currentState.validate()){
                formkey.currentState.save();
                print(product.rawData);
              }
            }:null),
        RaisedButton(
            child: Text("Save"),
            onPressed:step>=4?(){
              if(formkey.currentState.validate()){
                formkey.currentState.save();
                print(product.rawData);
              }
            }:null)
      ],

    );*/
    var productMock =OProduct(fields:{
      "name":"iphone 6 +",
      "price":5000000.00,
      "description":"in very good condition as new",
      "quantity":5000,
      "minimumOrder":1,
      "maxmumOrder":0,
      });
      var locationMock=OLocation(fields:{
        "lat":98.0,
        "lon":67.0,
        "street":"kijiweni kona",
        "ward":"kati",
        "district":"morogoro",
        "region":"morogoro",
        "country":"Tanzania"
      });
      var categoryMock=OCategory(fields:{
        "categoryName":"testing",
        "description":"things that use electricity"
      });

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:ProductForm(product:productMock),
            
      );

  }
}

//import 'dart:async';
//import 'dart:math';

import 'package:bizapp/src/bizapp/base/AppBase.dart';
import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/components/pages/community.dart';
import 'package:bizapp/src/components/pages/messages.dart';
import 'package:bizapp/src/components/pages/shop.dart';
import 'package:bizapp/src/components/pages/trending.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_redux/flutter_redux.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({this.title});
  final String title;
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends BizappState<HomeScreen> {
  /// This controller can be used to programmatically
  /// set the current displayed page
  PageController _pageController;
  ScrollController _scrollController;
  ScrollController _postScrollController;
  ScrollController _messagesScrollController;

 // int _page = 0;
  bool show = true;
  bool showPostFab = true;
  bool showMessageFab=true;

  @override
  void initState() {
    super.initState();
    //page controllers
    _pageController = PageController(initialPage: AppBase?.store?.state?.pageTabIndex??0);

    //scroll controllers
    _scrollController = ScrollController();
    _messagesScrollController=ScrollController();
    _postScrollController = ScrollController();

    //scroll listerners
    _scrollController.addListener(scrollListerner);
    _messagesScrollController.addListener(messageScrollListerner);
    _postScrollController.addListener(postScrollListerner);

  }

  void scrollListerner() {
    var update = true;
    var direction = _scrollController.position.userScrollDirection;
    if (direction == ScrollDirection.forward) {
      if (show) {
        update = false;
      }
      show = true;
    } else {
      if (!show) {
        update = false;
      }
      show = false;
    }
    if (update) setState(() {});
  }

  void postScrollListerner() {
    var update = true;
    var direction = _postScrollController.position.userScrollDirection;
    if (direction == ScrollDirection.forward) {
      if (showPostFab) {
        update = false;
      }
      showPostFab = true;
    } else {
      if (!showPostFab) {
        update = false;
      }
      showPostFab = false;
    }
    if (update) setState(() {});
  }
   void messageScrollListerner(){
     var update = true;
     var direction = _messagesScrollController.position.userScrollDirection;
     if (direction == ScrollDirection.forward) {
       if (showMessageFab) {
         update = false;
       }
       showMessageFab = true;
     } else {
       if (!showMessageFab) {
         update = false;
       }
       showMessageFab = false;
     }
     if (update) setState(() {});
   }
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
    _postScrollController.dispose();
    _scrollController.dispose();
  }

  Widget createFloatingActionButton(index) {
    switch (index) {
      case 1:
        return showPostFab
            ? FloatingActionButton(

                backgroundColor: Colors.greenAccent,
                tooltip: "post status",
                child: Icon(Icons.create),
                onPressed: () {
                  Navigator.of(context).pushNamed("/create");
                },
              )
            : Container(width: 0.0,height: 0.0);
      case 2:
        return Container(width: 0.0,height: 0.0);
      case 3:
        return showMessageFab? FloatingActionButton(
          child: Icon(Icons.chat),
            tooltip: "send message",
            onPressed: (){
          Navigator.of(context).pushNamed("/chats");
        }):Container(width: 0.0,height: 0.0);
      default:
        return show
            ? FloatingActionButton(
                //backgroundColor: Colors.,
                tooltip: "Sell item",
                child: Icon(Icons.add),
                onPressed: () {
                  Navigator.of(context).pushNamed("/create");
                },
              )
            : Container(width: 0.0,height: 0.0);
    }
  }

  @override
  Widget build(BuildContext context) {

    final shoppingCartIcon = IconButton(

      icon: Icon(Icons.shopping_cart),
      onPressed: () {
        Navigator.of(context).pushNamed("cart/id");
      },
    );
    final popUpMenu = StoreConnector<AppState, OUser>(
        converter: (store) => store.state?.user,
        builder: (context, user) => PopupMenuButton(
             // padding: EdgeInsets.symmetric(horizontal:64.0),
              onSelected: (routeName) async {
                print("\n\n$routeName\n\n");
                if (routeName != null) if (routeName.toString() == "logout") {
                  // user?.loading(true);
                  user.logout().then((val){
                    if(val==true){
                      Navigator.of(context).pushNamedAndRemoveUntil("login/welcome", (route)=>true);
                    }
                  });

                  // user.loading(false);
                } else {
                  Navigator.of(context).pushNamed(routeName.toString());
                }
              },
              itemBuilder: (BuildContext context) {
                return [
                  PopupMenuItem(
                    value: "account",
                    child: Text("profile"),
                  ),
                  PopupMenuItem(
                    value: "settings",
                    child: Text("Settings"),
                  ),
                  PopupMenuItem(
                    value: "help",
                    child: Text("Help"),
                  ),
                  PopupMenuItem(value: "credits", child: Text("Credits")),
                  PopupMenuItem(value: "logout", child: Text("Logout")),
                  PopupMenuItem(value: "login/signin", child: Text("Login"))
                ];
              },
            ));

    return Scaffold(
        appBar: AppBar(
            title: StoreConnector<AppState,String>(builder: (context,title)=>Text(title), converter:(store)=> store.state.title),
            actions: <Widget>[shoppingCartIcon, popUpMenu]),
        body: PageView(
              /// Specify the page controller
              onPageChanged: pageChangedHandler,
              controller: _pageController,
              children: [
                Shop(scroller: _scrollController,),
                Trending(scroller: _postScrollController,),
                Community(),
                Messages(scroller: _messagesScrollController,)
              ])
              ,
        floatingActionButton:StoreConnector<AppState,int>(
        converter: (store)=>store.state.pageTabIndex,
        builder:(context,tabIndex)=> createFloatingActionButton(tabIndex)
        ),
         floatingActionButtonLocation:FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar:StoreConnector<AppState,int>(
    converter: (store)=>store.state.pageTabIndex,
    builder:(context,tabIndex){
      //_pageController
      return  BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
            items: [Shop.TabItem, Trending.TabItem, Community.TabItem,Messages.TabItem],
            currentIndex: tabIndex??0,
            onTap: navigationTapped);
      }
        )
    );
  }
  void pageChangedHandler(i) {
    String title;
    switch (i) {

      case 1:
        title = "Trending";
        break;
      case 2:
        title = "Community";
        break;
      case 3:
        title = "Messages";
        break;
      case 0:
      default:
        title = "Shop";

    }
   // onPageChanged(i);
    changePageIndex(i);
    changeAppTitle(title);

  }
  void onPageChanged(int page) {
   // setState(() {
      //this._page = page;
    //});
  }

  void navigationTapped(int index) {
    _pageController
        .animateToPage(index,
            duration: const Duration(milliseconds: 200), curve: Curves.ease)
        .then((ull) {
      //this._page = index;
    });
  }
}



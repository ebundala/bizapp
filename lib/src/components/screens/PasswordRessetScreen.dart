import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/components/screens/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
//controller

//password reseet

class PasswordResetScreen extends StatefulWidget {
  final String title;

  PasswordResetScreen({this.title = "Reset password"});
  @override
  _PasswordResetScreenState createState() => _PasswordResetScreenState();
}

class _PasswordResetScreenState extends BizappState<PasswordResetScreen> {
  final TextEditingController _emailController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String validatePassword(String text) {
    if (text.length < 4) {
      return "Password must be 5 or more characters";
    }
    
  }

  String validateUsername(String text) {
    if (text.length == 0) {
      return "Username cant be empty";
    } else if (text.length < 3) {
      return "Username must be longer than 3 characters";
    }
    
  }

  String validateEmail(text) {
    if (text.contains("@")) {
      return "email must contain @ ";
    }
    
  }

  void resetPassword() async {
    if (formKey.currentState.validate()) {
      //submit request here
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/logo.png'),
      ),
    );

    final email = StoreConnector<AppState, bool>(
        converter: (store) => store.state?.loading,
        builder: (context, loading) => TextFormField(
              enabled: loading != true,
              keyboardType: TextInputType.emailAddress,
              style: Theme.of(context).textTheme.title,
              autofocus: false,
              autocorrect: true,
              controller: _emailController,
              validator: validateEmail,
              //initialValue: 'someone@example.com',
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                hintText: 'Email',
                hintStyle: Theme.of(context).textTheme.subtitle,
                contentPadding:
                    const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(32.0)),
              ),
            ));

    final submitButton = Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
          borderRadius: BorderRadius.circular(30.0),
          shadowColor:
              Theme.of(context).accentColor, //Colors.deepOrange.shade100,
          elevation: 5.0,
          child: StoreConnector<AppState, dynamic>(
            converter: (store) => store.state?.loading,
            builder: (context, loading) => loading == true
                ? LoadingDialog()
                : RaisedButton(
                    padding: const EdgeInsets.fromLTRB(40.0, 16.0, 40.0, 16.0),
                    elevation: 3.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0)),
                    onPressed: loading == true
                        ? null
                        : () {
                            resetPassword();
                          },
                    color: Theme.of(context).primaryColor,
                    child:
                        Text('Submit', style: TextStyle(color: Colors.white)),
                  ),
          )),
    );

    final heading = Center(
        child: Text(
      widget.title,
      style: TextStyle(
          color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
    ));

    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          decoration: BoxDecoration(
              //gradient: Gradient(colors:<Color>[] ),
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("images/profile_header_background.png"))),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            child: Form(
                key: formKey,
                child: ListView(
                 // mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 80.0),
                    logo,
                    heading,
                    SizedBox(height: 24.0),
                    email,
                    //SizedBox(height: 100.0),
                    SizedBox(height: 24.0),
                    submitButton,
                  ],
                )),
          ),
        ));
  }
}


import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  SettingsScreen({this.title = "Settings"});
  final String title;
  @override
  State createState() => SettingsScreenState();
}

class SettingsScreenState extends BizappState<SettingsScreen> {
  SettingsScreenState();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() { 
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),    
      ),
      body: Text("settings")
    );
  }
}


import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

//welcome screen
class WelcomeScreen extends StatelessBizappWidget {
  final String title;
  WelcomeScreen({this.title});
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/logo.png'),
      ),
    );

    final heading = Center(
        child: Text(
      title,
      style: TextStyle(
          color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
    ));

    final signUpButton = Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor:
            Theme.of(context).accentColor, //Colors.deepOrange.shade100,

        child: RaisedButton(
            padding: const EdgeInsets.fromLTRB(40.0, 16.0, 40.0, 16.0),
            elevation: 3.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),

            // minWidth: 120.0,
            // height: 42.0,
            onPressed: () {
              Navigator.of(context).pushNamed("login/signup");
            },
            color: Theme.of(context).primaryColor, //Colors.deepOrange,
            child: Center(
                child: Text('Sign Up', style: TextStyle(color: Colors.white)))),
      ),
    );

    final signInButton = Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor:
            Theme.of(context).accentColor, //Colors.deepOrange.shade100,

        child: RaisedButton(
            padding: const EdgeInsets.fromLTRB(40.0, 16.0, 40.0, 16.0),
            elevation: 3.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0)),
            onPressed: () {
              Navigator.of(context).pushNamed("login/signin");
            },
            color: Theme.of(context).primaryColor, //Colors.deepOrange,
            child: Center(
                child: Text('Log In', style: TextStyle(color: Colors.white)))),
      ),
    );

    final buttons = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[signInButton, signUpButton],
      ),
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        decoration: BoxDecoration(
            //gradient: Gradient(colors:<Color>[] ),
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("images/profile_header_background.png"))),
        child: ListView(
         // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[SizedBox(height: 80.0,),logo, heading, SizedBox(height: 200.0), buttons],
        ),
      ),
    );
  }
}

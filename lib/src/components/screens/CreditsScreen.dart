import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class CreditsScreen extends StatefulWidget {
  CreditsScreen({this.title = "Credits"});
  final String title;
  @override
  State createState() => CreditsScreenState(title: title);
}

class CreditsScreenState extends BizappState<CreditsScreen> {
  CreditsScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class PostsScreen extends StatefulWidget {
  PostsScreen({this.title = "Posts"});
  final String title;
  @override
  State createState() => PostsScreenState(title: title);
}

class PostsScreenState extends BizappState<PostsScreen> {
  PostsScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

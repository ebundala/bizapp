import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class ProductsListScreen extends StatefulWidget {
  ProductsListScreen({this.title = "Products list"});
  final String title;
  @override
  State createState() => ProductsListScreenState(title: title);
}

class ProductsListScreenState extends BizappState<ProductsListScreen> {
  ProductsListScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

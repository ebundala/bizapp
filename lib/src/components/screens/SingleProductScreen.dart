import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class SingleProductScreen extends StatefulWidget {
  SingleProductScreen({this.title = "Single Product"});
  final String title;
  @override
  State createState() => SingleProductScreenState(title: title);
}

class SingleProductScreenState extends BizappState<SingleProductScreen> {
  SingleProductScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

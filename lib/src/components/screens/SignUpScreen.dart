import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
//import 'package:bizapp/src/components/screens/LoadingDialog.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
//controller

//signup screen

class SignUpScreen extends StatefulWidget {
  SignUpScreen({this.title = "Login"});
  final String title;

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends BizappState<SignUpScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final formkey = GlobalKey<FormState>();
  final scaffoldKey=GlobalKey<ScaffoldState>();
  String validatePassword(String text) {
    if (!isLength(text, 6,50)) {
      return "Password must be 6 or more characters";
    }
  }

  String validateUsername(String text) {
    if (!isLength(text, 3,20)) {
      return "Username must be 3 and 50 character long";
    }
     if (!isAlphanumeric(text)) {
      return "Username can contain letters and numbers only";
    }
  }

  String validateEmail(String text) {
    if (!isEmail(text)) {
      return "invalid email";
    }
  }

  void signUserUp(context) async {
    if (formkey.currentState.validate()) {
      OUser user = new OUser();
      loading(true);
      showLoadingDialog(true, context,msg: "Signing up as '${_usernameController.text}'");
      await user.register(
          username: _usernameController.text,
          password: _passwordController.text,
          role: "admin").catchError((e){
            loading(false);
            showLoadingDialog(false,context);
            var error=getLastError().message;
           scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("$error"),));
          });
      loading(false);
      showLoadingDialog(false, context);
      if (user.rid != null) {
        user.initialDataLoad();
        Navigator.of(context)
            .pushNamedAndRemoveUntil("/home", (route) => false);
      }
    }
  }

  Widget buildUsername(){
    return TextFormField(
        style: Theme.of(context).textTheme.title,
        enabled: loadingState != true,
        keyboardType: TextInputType.text,
        autofocus: false,
        autocorrect: true,
        controller: _usernameController,
        validator: validateUsername,
        //initialValue: 'someone@example.com',
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.account_circle),
            hintText: 'Username',
             hintStyle: Theme.of(context).textTheme.subtitle,
            contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))));
  }
  Widget buildEmail(){
    return TextFormField(
      style: Theme.of(context).textTheme.title,
      enabled: loadingState != true,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      autocorrect: true,
      controller: _emailController,
      validator: validateEmail,
      //initialValue: 'someone@example.com',
      textAlign: TextAlign.center,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.mail),
        hintStyle: Theme.of(context).textTheme.subtitle,
        hintText: 'Email',
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

  }
  Widget buildPassword(){
    return TextFormField(
      style: Theme.of(context).textTheme.title,
      enabled: loadingState != true,
      autofocus: false,
      //initialValue: 'some password',
      textAlign: TextAlign.center,
      validator: validatePassword,
      obscureText: true,
      controller: _passwordController,
      decoration: InputDecoration(
        prefixIcon:Icon(Icons.lock),
        hintText: 'Password',
         hintStyle: Theme.of(context).textTheme.subtitle,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
  Widget buildConfirmPassword(){
 return TextFormField(
      style: Theme.of(context).textTheme.title,
      enabled: loadingState != true,
      autofocus: false,
      //initialValue: 'some password',
      textAlign: TextAlign.center,
      validator: (confirmedPass){
        if(!(_passwordController.text.compareTo(confirmedPass)==0)){
          return "Password mismatch ";
        }
      },
      obscureText: true,
      //controller: _passwordController,
      decoration: InputDecoration(
        prefixIcon:Icon(Icons.lock),
        hintText: 'Confirm Password',
         hintStyle: Theme.of(context).textTheme.subtitle,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
  Widget buildSubmit(){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: 
          Material(
              borderRadius: BorderRadius.circular(30.0),
              shadowColor: Theme.of(context).accentColor,
              child: RaisedButton(
                padding: const EdgeInsets.fromLTRB(40.0, 16.0, 40.0, 16.0),
                elevation: 3.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)),
                onPressed: loadingState == true
                    ? null
                    : () {
                        signUserUp(context);
                      },
                color: Theme.of(context).primaryColor,
                child: Text('Sign Up', style: TextStyle(color: Colors.white)),
              ),
            ),
    );

  } 
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/logo.png'),
      ),
    );
    
    final termSLabel = FlatButton(
      child: Text(
        'By signing up you agree to terms of use.',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: loadingState == true
          ? null
          : () {
              Navigator.of(context).pushNamed("login/terms");
            },
    );

    final heading = Center(
        child: Text(
      widget.title,
      style: TextStyle(
          color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
    ));
    
    
    return Scaffold(
      key: scaffoldKey,
        backgroundColor: Colors.white,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage("images/profile_header_background.png"))),
          child: Form(
              key: formkey,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 80.0,),
                  logo,
                  heading,
                  SizedBox(height: 24.0),
                  buildUsername(),
                  SizedBox(height: 8.0),
                  buildEmail(),
                  SizedBox(height: 8.0),
                  buildPassword(),
                  SizedBox(height:8.0),
                  buildConfirmPassword(),
                  SizedBox(height: 24.0),
                  buildSubmit(),
                  termSLabel
                ],
              )),
        ));
  }
}

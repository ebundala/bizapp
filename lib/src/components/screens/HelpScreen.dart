import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class HelpScreen extends StatefulWidget {
  HelpScreen({this.title = "Help"});
  final String title;
  @override
  State createState() => HelpScreenState(title: title);
}

class HelpScreenState extends BizappState<HelpScreen> {
  HelpScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

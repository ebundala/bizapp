import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class SinglePostScreen extends StatefulWidget {
  SinglePostScreen({this.title = "SinglePost"});
  final String title;
  @override
  State createState() => SinglePostScreenState(title: title);
}

class SinglePostScreenState extends BizappState<SinglePostScreen> {
  SinglePostScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

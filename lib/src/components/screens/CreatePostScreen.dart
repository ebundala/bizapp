import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class CreatePostScreen extends StatefulWidget {
  CreatePostScreen({this.title = "Create  Post"});
  final String title;
  @override
  State createState() => CreatePostScreenState(title: title);
}

class CreatePostScreenState extends BizappState<CreatePostScreen> {
  CreatePostScreenState({this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Text(title),
      ),
    );
  }
}

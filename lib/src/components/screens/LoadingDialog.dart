import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class LoadingDialog extends StatelessBizappWidget {
  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator();
  }
}

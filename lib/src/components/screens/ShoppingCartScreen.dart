import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

class ShoppingCartScreen extends StatefulWidget {
  ShoppingCartScreen({this.title = "Shopping Cart"});
  final String title;
  @override
  State createState() => ShoppingCartScreenState();
}

class ShoppingCartScreenState extends BizappState<ShoppingCartScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
            appBar: AppBar(
                title: Text(widget.title),
                bottom: TabBar(tabs: [
                  Tab(child: Text("Cart")),
                  Tab(child: Text("Orders")),
                  Tab(child: Text("History")),
                ])),
            body: TabBarView(
              children: [
                Container(child: Center(child: Text("my Shopping cart"))),
                Container(child: Center(child: Text("my Orders"))),
                Container(child: Center(child: Text("transactions history"))),
              ],
            )));
  }
}

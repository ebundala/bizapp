import 'dart:async';
import 'dart:io';

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';


class MapScreen extends StatefulWidget {
  double lat;
  double lon;
  double toLat;
  double toLon;
  MapScreen({this.lat,this.lon,this.toLat,this.toLon});
  
  @override
  State createState() => MapScreenState();
}

class MapScreenState extends BizappState<MapScreen> {
  MapScreenState();
final Completer<WebViewController> _controller =
      Completer<WebViewController>();
 

  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  
   String selectedUrl;
  HttpServer _server;

  Future<void> initServer() async {
    
    await HttpServer.bind(InternetAddress.loopbackIPv4, 8080).then((server){
      _server=server;
    _server.listen((HttpRequest request) async {
      //_res="<html><h1>You can now close this window</h1></html>";
     await loadHtml().then((html) async {
        print("html content length\n ${html.length}");
        request.response
          ..statusCode = 200
          ..headers.set("Content-Type", ContentType.html.mimeType)
          ..add(html);
        await request.response.close();
        //await stopServer();
       await _server.close(force: true);
      }).catchError((e) {
        print("failed to open html $e");
        throw e;
      });
    });
    });
  }

  Future<void> stopServer() async {
    await _server.close(force: true);
  }

  @override
  initState() {
    super.initState();
    selectedUrl = "http://localhost:8080?lat=${widget.lat}&lon=${widget.lon}&to_lat=${widget.toLat}&to_lon=${widget.toLon}";

    //initServer();
    
  }

  @override
  void dispose() async{
    super.dispose();
    await stopServer();
  }

  Future<List<int>> loadHtml() async {
    return await rootBundle
        .load('www/index.html')
        .then((res) => res.buffer.asUint8List());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text('Maps'),
        // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
        actions: <Widget>[
         
        ],
      ),
      body:FutureBuilder(
        future: initServer(),
        builder: (context,snapshot){
          if((snapshot.connectionState==ConnectionState.done)&&!snapshot.hasError){
          return WebView(
        initialUrl: selectedUrl,
        javaScriptMode: JavaScriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },
      );
      }
      if((snapshot.connectionState==ConnectionState.done)&&snapshot.hasError){
        return Center(child: Icon(Icons.error_outline,size: 50.0,));
      }
      else{
        return LinearProgressIndicator();
      }
        },
      ) 
     
    );
    
    
    
    /* WebviewScaffold(
      key: _scaffoldKey,
      withJavascript: true,
      withLocalStorage: true,
      enableAppScheme: true,
      url: selectedUrl,
      appBar: AppBar(
        title: Text("Web view app"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () async {
              await flutterWebviewPlugin.close();
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    ); */
  }
}

/* new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text('Plugin example app'),
      ),
      body: new ListView(
        
       // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          new Container(
            padding: const EdgeInsets.all(24.0),
            child: new TextField(controller: _urlCtrl),
          ),
          new RaisedButton(
            onPressed: () {
              flutterWebviewPlugin.launch(selectedUrl,
                  rect: new Rect.fromLTWH(
                      0.0, 0.0, MediaQuery.of(context).size.width, 300.0),
                  userAgent: kAndroidUserAgent);
            },
            child: new Text("Open Webview (rect)"),
          ),
          new RaisedButton(
            onPressed: () {
              flutterWebviewPlugin.launch(selectedUrl, hidden: true);
            },
            child: new Text("Open 'hidden' Webview"),
          ),
          new RaisedButton(
            onPressed: () {
              //flutterWebviewPlugin.launch(selectedUrl);
              loadHtml().then((html){
               String ht='<html><body><h1>hello world world world<br>Home world</h1></body></html>';
               
                String url=new Uri.dataFromString(ht, mimeType: 'text/html',base64: true).toString();
                 print("html loaded with length ${html.length} \n$html");
                flutterWebviewPlugin.launch(url,
               
                withJavascript: true,
                withLocalStorage: true,
                rect: new Rect.fromLTWH(
                      0.0, 50.0, MediaQuery.of(context).size.width, 300.0),
                );
                //setState(() {
                             //_history.add(html);     
                               // });
                
              }).catchError((e){
                print("failed to load asset html $e");
              });
            },
            child: new Text("load asset html"),
          ),
          new RaisedButton(
            onPressed: () {
              Navigator.of(context).pushNamed("/widget");
            },
            child: new Text("Open widget webview"),
          ),
          new Container(
            padding: const EdgeInsets.all(24.0),
            child: new TextField(controller: _codeCtrl),
          ),
          new RaisedButton(
            onPressed: () {
              Future<String> future =
                  flutterWebviewPlugin.evalJavascript(_codeCtrl.text);
              future.then((String result) {
                setState(() {
                  _history.add("eval: $result");
                });
              });
            },
            child: new Text("Eval some javascript"),
          ),
          new RaisedButton(
            onPressed: () {
              setState(() {
                _history.clear();
              });
              flutterWebviewPlugin.close();
            },
            child: new Text("Close"),
          ),
          new RaisedButton(
            onPressed: () {
              flutterWebviewPlugin.getCookies().then((m) {
                setState(() {
                  _history.add("cookies: $m");
                });
              });
            },
            child: new Text("Cookies"),
          ),
         new Container(
                  // height: 300.0,
                   constraints: BoxConstraints(minHeight: 100.0,maxHeight: 200.0),
           child:new ListView.builder(
            // itemExtent: 5.0,
            cacheExtent: 5.0,
            reverse: false,
            itemCount: _history.length,
            itemBuilder: (BuildContext context,int i){
              return new ListTile(
                leading:new Text(i.toString()),
                title: new Text(_history[i]),
              );
            },)
          ) 
        ],
      ),
    ); */

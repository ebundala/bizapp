import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
//controller

//login screen
class LoginScreen extends StatefulWidget {
  static String tag = 'login-page';
  LoginScreen({this.title = "Login"});
  final String title;
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends BizappState<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final formkey = GlobalKey<FormState>();
  final scaffoldKey=GlobalKey<ScaffoldState>();
  String validatePassword(String text) {
    if (isNull(text)) {
      return "Password cant be empty";
    } 
    if (!isLength(text, 4)) {
      return "Password must be 4 or more characters";
    }
  }

  String validateUsername(String text) {
    if (isNull(text)) {
      return "Username cant be empty";
    } else if (!isLength(text, 3)) {
      return "Username must be longer than 3 characters";
    }
    else if(!isAlphanumeric(text)){
      return "Username cannot contain special characters";
    }
    
  }

  submitLogin(context) {
    if (formkey.currentState.validate()) {
      OUser user = new OUser();
      loading(true);
      showLoadingDialog(true, context,msg: "Loging in as ${_emailController.text}");
       user.login(
          password: _passwordController.text, username: _emailController.text).then((res){
          showLoadingDialog(false, context);
          loading(false);
          if (user.rid != null)
          {
            user.initialDataLoad();
            Navigator.of(context)
                .pushNamedAndRemoveUntil("/home", (route) => false);
          }
              }).catchError((e){
            showLoadingDialog(false, context);
            loading(false);
            
            var msg=getLastError().message;
            //print(e);
            print(msg);
            //showSnackbar(content: Text("$msg"));
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("$msg"),));
          });
          
         // changeLoadingText("failed to login please try again");
      
    }
  }

  Widget buildUserName(){
     return TextFormField(
      style: Theme.of(context).textTheme.title,
      enabled: loadingState != true,
      keyboardType: TextInputType.emailAddress,
      controller: _emailController,
      autocorrect: true,
      autofocus: false,
      textAlign: TextAlign.center,
      //initialValue: 'someone@example.com',
      validator: validateUsername,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.account_circle),
        hintText: 'Username',
        hintStyle: Theme.of(context).textTheme.subtitle,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
   }

  Widget buildPassword(){
     return TextFormField(
      style:Theme.of(context).textTheme.title,
      enabled: loadingState != true,
      autofocus: false,
      textAlign: TextAlign.center,
      validator: validatePassword,
      //initialValue: 'some password',
      controller: _passwordController,
      autocorrect: false,
      obscureText: true,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.lock,),
        hintText: 'Password',
        hintStyle: Theme.of(context).textTheme.subtitle,
        contentPadding: const EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
   }

  Widget buildSubmit(){
     return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child:Material(
              borderRadius: BorderRadius.circular(30.0),
              shadowColor: Theme.of(context).accentColor,
              elevation: 5.0,
              child: RaisedButton(
                  padding: const EdgeInsets.fromLTRB(40.0, 16.0, 40.0, 16.0),
                  elevation: 3.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0)),
                  onPressed: () async {
                     submitLogin(context);
                  },
                  color: Theme.of(context).primaryColor, //Colors.deepOrange,
                  child:
                      Text('Log In', style: TextStyle(color: Colors.white)))),
    );
   }
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('images/logo.png'),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: Theme.of(context).textTheme.body1,
      ),
      onPressed: loadingState
          ? null
          : () {
              Navigator.of(context).pushNamed("login/reset");
            },
    );

    final heading = Center(
        child: Text(
      widget.title,
      style:TextStyle(color: Colors.black, fontSize: 16.0, fontWeight: FontWeight.bold),
    ));

    return Scaffold(
      key: scaffoldKey,
        backgroundColor: Colors.white,
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 24.0),
            decoration: BoxDecoration(
               /* gradient: LinearGradient(
                  // Where the linear gradient begins and ends
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  // Add one stop for each color. Stops should increase from 0 to 1
                  stops: [0.5,0.9],
                  colors: [
                    Colors.white,
                    Colors.black
                  ],
                ),
                backgroundBlendMode: BlendMode.darken,*/
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("images/profile_header_background.png"))
            ),
            child: Form(
              key: formkey,
              autovalidate: false,
              child: ListView(
                //mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 80.0),
                  logo,
                  heading,
                  SizedBox(height: 24.0),
                  buildUserName(),
                  SizedBox(height: 8.0),
                  buildPassword(),
                  SizedBox(height: 24.0),
                  buildSubmit(),
                  forgotLabel
                ],
              ),
            )));
  }
}

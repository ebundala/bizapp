import 'dart:async';

import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
//import 'package:firebase_auth/firebase_auth.dart';

final analytics = FirebaseAnalytics();

class ChatsListScreen extends StatefulWidget {
  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<ChatsListScreen> {
  OUser user;
  final convRef = FirebaseDatabase.instance.reference().child("chats");
  final userRef = FirebaseDatabase.instance.reference().child("users");
  @override
  void initState() {
    super.initState();
  }

  Future<Null> createRoom() async {
    //Todo async get participants ids
    if (user != null) {
      String key = convRef.push().key;
      await convRef.child(key).set({"${user?.rid}": true, "chatId": key});
      print("new chat room created with id $key");
      Navigator.of(context).pushNamed("/chat/$key");
    }
  }

  @override
  Widget build(BuildContext context) {
    //_user = StoreProvider.of<AppState>(context).state.currentUser;
    return Scaffold(
        appBar: AppBar(
          title: Text("Chats"),
          // elevation: Theme.of(context).platform==TargetPlatform.iOS?0.0:4.0,
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.message),
          onPressed: () {
            createRoom();
          },
        ),
        body: StoreConnector<AppState, OUser>(
            converter: (store) => store.state.user,
            builder: (context, user) {
              return user != null
                  ? Column(
                      children: <Widget>[
                        Flexible(
                            child: FirebaseAnimatedList(
                                query: convRef
                                    .orderByChild(user.rid)
                                    .equalTo(true),
                                sort: (a, b) => b.key.compareTo(a.key),
                                padding: EdgeInsets.all(8.0),
                                defaultChild:
                                    Center(child: CircularProgressIndicator()),
                                reverse: false,
                                itemBuilder: (BuildContext context,
                                    DataSnapshot snapshot,
                                    Animation<double> animation,
                                    int i) {
                                  return Container(
                                      margin: const EdgeInsets.all(16.0),
                                      child: Center(
                                          child: ChatItem(
                                              user: user,
                                              snapshot: snapshot,
                                              animation: animation)
                                          /* RaisedButton(child: Text(snapshot.key),onPressed: (){

                          Navigator.pushNamed(context,"/chat/${snapshot.key}");
                        },)*/
                                          ));
                                })),
                        Divider(height: 1.0),
                      ],
                    )
                  : Center(
                      child: RaisedButton(
                          child: Text("Login"),
                          onPressed: () {
                            Navigator.of(context).pushNamed("/login/signin");
                          }),
                    );
            }));
  }
}

class ChatItem extends StatelessWidget {
  ChatItem({this.user, this.snapshot, this.animation});

  final DataSnapshot snapshot;
  final Animation animation;
  final OUser user;
  Future<Null> getChatDetails() async {}

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          Navigator.of(context).pushNamed("/chat/${snapshot.key}");
        },
        child: SizeTransition(
          sizeFactor: CurvedAnimation(parent: animation, curve: Curves.easeOut),
          axisAlignment: 0.0,
          child: Container(
            margin: const EdgeInsets.symmetric(vertical: 10.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 16.0),
                  child: CircleAvatar(
                      backgroundImage:
                          NetworkImage(user?.getField("avator") ?? "")),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(user?.getField("name") ?? "a user",
                          style: Theme.of(context).textTheme.subhead),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: Text(snapshot.key),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}

import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:flutter/material.dart';

//splash screen
class SplashScreen extends StatelessBizappWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        decoration: BoxDecoration(
            //gradient: Gradient(colors:<Color>[] ),
            image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("images/profile_header_background.png"))),
        child: Center(child: CircularProgressIndicator()));
  }
}

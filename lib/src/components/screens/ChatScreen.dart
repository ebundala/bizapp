import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
//import 'package:firebase_auth/firebase_auth.dart';

final analytics = FirebaseAnalytics();

class ChatScreen extends StatefulWidget {
  ChatScreen({this.chatId});
  final String chatId;
  @override
  State createState() => ChatScreenState();
}

class ChatScreenState extends BizappState<ChatScreen> {
  final TextEditingController _textController = TextEditingController();
  final messageRef = FirebaseDatabase.instance.reference().child('messages');
  final userRef = FirebaseDatabase.instance.reference().child("users");
  var _user;
  bool _isComposing = false;

  @override
  void initState() {
    super.initState();
  }

  Widget _buildTextComposer() {
    print("chat id ${widget.chatId}");
    return IconTheme(
      data: IconThemeData(color: Theme.of(context).accentColor),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: IconButton(
                icon: Icon(Icons.photo_camera),
                onPressed: () async {
                  File imageFile = await ImagePicker.pickImage(
                      source: ImageSource.gallery,
                      maxHeight: 480.0,
                      maxWidth: 640.00);
                  int random = Random().nextInt(1000000);
                  StorageReference ref = FirebaseStorage.instance
                      .ref()
                      .child(_user.uid)
                      .child("image_$random.jpg");
                 // StorageUploadTask uploadTask = ref.put(imageFile);
                 // Uri downloadUrl = (await uploadTask.resume().downloadUrl;
                  //_sendMessage(imageUrl: downloadUrl.toString());
                },
              ),
            ),
            Flexible(
              child: TextField(
                controller: _textController,
                onSubmitted: _handleSubmitted,
                //maxLines: 9,
                onChanged: (String text) {
                  setState(() {
                    _isComposing = text.length > 0;
                  });
                },
                decoration:
                    InputDecoration.collapsed(hintText: "send a message"),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 4.0),
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: _isComposing
                    ? () => _handleSubmitted(_textController.text)
                    : null,
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<Null> _handleSubmitted(String text) async {
    _textController.clear();
    setState(() {
      _isComposing = false;
    });
    _sendMessage(text: text);
  }

  void _sendMessage({String text, String imageUrl}) {
    if (_user != null) {
      String key = messageRef.push().key;
      messageRef.child(key).set({
        'authorId': _user.uid,
        'chatId': widget.chatId,
        'messageId': key,
        'text': text,
        'imageUrl': imageUrl,
        'senderName': _user.displayName,
        'senderPhotoUrl': _user.photoUrl
      });
      analytics.logEvent(name: 'send_message');
    } else {
      print("Error user not logged in");
    }
  }

  @override
  Widget build(BuildContext context) {
    //_user = StoreProvider.of<AppState>(context).state.currentUser;
    return StoreConnector<AppState, OUser>(
      converter: (store) => store.state.user,
      builder: (context, user) => Scaffold(
          appBar: AppBar(
            title: Text(user.getField("name") ?? "a user"),
            elevation:
                Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
          ),
          body: user != null
              ? Column(
                  children: <Widget>[
                    Flexible(
                        child: FirebaseAnimatedList(
                            query: messageRef
                                .orderByChild("chatId")
                                .equalTo(widget.chatId),
                            sort: (a, b) => b.key.compareTo(a.key),
                            padding: EdgeInsets.all(8.0),
                            reverse: true,
                            defaultChild:
                                Center(child: CircularProgressIndicator()),
                            itemBuilder: (BuildContext cx,
                                DataSnapshot snapshot,
                                Animation<double> animation,
                                int i) {
                              return ChatMessage(
                                  snapshot: snapshot, animation: animation);
                            })),
                    Divider(height: 1.0),
                    Container(
                      decoration:
                          BoxDecoration(color: Theme.of(context).cardColor),
                      child: _buildTextComposer(),
                    )
                  ],
                )
              : Center(
                  child: RaisedButton(
                      child: Text("Login"),
                      onPressed: () {
                        Navigator.of(context).pushNamed("/login");
                      }),
                )),
    );
  }
}

class ChatMessage extends StatelessWidget {
  ChatMessage({this.snapshot, this.animation});

  final DataSnapshot snapshot;
  final Animation animation;

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
        sizeFactor: CurvedAnimation(parent: animation, curve: Curves.easeOut),
        axisAlignment: 0.0,
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(right: 16.0),
                child: CircleAvatar(
                    backgroundImage:
                        NetworkImage(snapshot.value['senderPhotoUrl'] ?? "")),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(snapshot.value['senderName'] ?? "a user",
                        style: Theme.of(context).textTheme.subhead),
                    Container(
                      margin: const EdgeInsets.only(top: 5.0),
                      child: snapshot.value['imageUrl'] != null
                          ? Image.network(snapshot.value['imageUrl'],
                              width: 250.0)
                          : Text(snapshot.value['text']),
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }
}

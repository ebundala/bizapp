import 'dart:io';
import 'dart:async';
//import 'dart:isolate';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image/image.dart' as ImageFormater;
import 'package:uuid/uuid.dart';
import 'package:path_provider/path_provider.dart';
import 'package:validators/validators.dart';






class ImagePickerForm extends StatefulWidget{
  final format;
  final maxCount;
  final minCount;
  static const PNG="png";
  static const JPEG="jpeg";
  static const Gif="gif";
  static const TIF="tif";

  ImagePickerForm({Key key,this.format=ImagePickerForm.PNG,this.maxCount:4,this.minCount:0}):super(key:key);
  @override
  ImagePickerFormState createState()=>ImagePickerFormState();
}

class ImagePickerFormState extends BizappState<ImagePickerForm>{
   List<File> images=[ ];
   String formError="";
  bool validate(){
    var count=images.length;
    var res=count>=widget.minCount&&count<=widget.maxCount;
    if(!res){setState(() {
          formError=" add atleast ${widget.minCount} and not more than ${widget.maxCount} photos";
        });}else{
          setState(() {
                      formError="";
                    });
        }
    return res;
  }

  Future<File> pickImage({ImageSource source:ImageSource.gallery}){
    return ImagePicker.pickImage(source: source).then((file){
      return cropImage(file);
    }).then((file){
      return changeFormat(file);
    });
  }
  Future<File> cropImage(File file){ {
    return ImageCropper.cropImage(
      sourcePath: file.path,
      ratioX: 16.0,
      ratioY: 9.0,
     // maxWidth: 1280 ,
      maxHeight: 480,
      toolbarTitle: "Crop image",
      toolbarColor: Theme.of(context).primaryColor
    );
}
  }
  Future<File> changeFormat(File file){
    showLoadingDialog(true, context,msg: "converting image..");
    return getTemporaryDirectory().then((tmp)async{
      var data=await file.readAsBytes();
      //ImageFormater.Image image = ImageFormater.decodeImage(data);
      var encodedImage= await compute<List<int>,List<int>>(convertImage,data);//convertImage(data);
      //ImageFormater.encodePng(image);
        
     // Resize the image to a 120x? thumbnail (maintaining the aspect ratio).
     //ImageFormater.Image thumbnail = ImageFormater.copyResize(image, 800);

      var uuid = new Uuid();
      var fileName=uuid.v4();
     var imagefile=new File('${tmp.path}/$fileName.${widget.format}');
     return imagefile.writeAsBytes(encodedImage);
     
     }).then((file){
       showLoadingDialog(false, context,msg: "converting image..");
       return file;
     }).catchError((e){
       showLoadingDialog(false,context);
       //showSnackBar()
     });

  }
  List<File> save(){
    
    return images;
  }
  bool isDisabled(){
    var count=images.length;
    return count>=widget.maxCount;
  }
  @override
  Widget build(BuildContext context){
    return Column(
      children:<Widget>[
    Container(
      height: 300.0,
      
      child:GridView.custom(
       
        primary: true,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 160.0),
        childrenDelegate: SliverChildBuilderDelegate((context,i){
          if(i<images.length){
          return Container(
            width: 160.0,
            height: 160.0,
            child: Card(elevation: 8.0,
            child:Stack(
              children: <Widget>[ 
                Image.file(images[i]),               
                Positioned( 
                  left: 0.0,
                  top: 0.0,
                  child: IconButton(
                    alignment:Alignment.topLeft,
                  padding: EdgeInsets.all(0.0),
                  iconSize: 30.0,
                  icon: Icon(Icons.delete_forever,color: Colors.white,),onPressed: (){
                    
                    setState(() {
                        images.removeAt(i);             
                        });
                  //print("delete $i");
                },),
                ),
                Positioned(
                  bottom: 8.0,
                  left: 70.0,
                  child: Container(child:Row(                    
                    children: <Widget>[Center(child:Text("${i+1}"))],))
                )
                
              ],
            )
            // ,
            )
            );
        }
        //return null;
        },childCount: images.length),
        

      ) 

          
    )
    ,formError.isNotEmpty?Row(
      children: <Widget>[
        Center(child: Text(formError,style: TextStyle(color: Colors.red,fontSize: 12.0),),
        )
        ],):Container(width: 0,height: 0,)
    ,Padding(
      padding: EdgeInsets.only(top:16.0),
      child:Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[   
      
      FlatButton(        
        child: Icon(Icons.image),
        onPressed: !isDisabled()?()async{
         var file =await pickImage(source: ImageSource.gallery);
         if(file is File){
           setState(() {
                     images.add(file);  
                      });
         }else{
          // showSnackbar(content: )
         }
        }:null,
      ),
      FlatButton(        
        child: Icon(Icons.camera_alt),
        onPressed: !isDisabled()?()async{
         var file =await pickImage(source: ImageSource.camera);
         if(file is File){
           setState(() {
                     images.add(file);  
                      });
         }
        }:null,
      ),
      
      ]
    ))
    
    ]
    );
  }
}


List<int> convertImage(List<int> img){
 ImageFormater.Image image = ImageFormater.decodeImage(img);
return ImageFormater.encodePng(image);
}
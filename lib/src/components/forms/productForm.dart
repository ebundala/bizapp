import 'dart:io';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/bizapp/controllers/OProduct.dart';
//import 'package:bizapp/src/bizapp/controllers/OUser.dart';
import 'package:bizapp/src/bizapp/controllers/OLocation.dart';
import 'package:bizapp/src/bizapp/controllers/OCategory.dart';
import 'package:bizapp/src/components/forms/locationForm.dart';
import 'package:bizapp/src/components/forms/imagePickerForm.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:uuid/uuid.dart';
import 'package:bizapp/src/bizapp/states/state.dart';
import 'package:flutter_redux/flutter_redux.dart';

final FirebaseStorage storage = FirebaseStorage();
class ProductForm extends StatefulWidget{
  OProduct product;
  OLocation location;
  OCategory category;
  ProductForm({Key key,this.product,this.category,this.location}):super(key:key){
   if(product==null){
     product=OProduct();
   }
   if(location==null){
     location=OLocation();
   }
   if(category==null){
     category=OCategory();
   }
  }
  @override
  ProductFormState createState()=>ProductFormState();

}

class ProductFormState extends BizappState<ProductForm>{
    final formKey=GlobalKey<FormState>();
    final locationKey=GlobalKey<LocationFormState>();
    final imagePickerKey=GlobalKey<ImagePickerFormState>();
    List<StorageUploadTask> _tasks = <StorageUploadTask>[];

    OCategory categoryValue;
  int currentStep=0;
  List<File> photos=[];
  ProductFormState();
  void onStepContinue()async{
              bool res=false;
              switch(currentStep){
                case 0://products details
                res=formKey.currentState.validate();
                if(res){formKey.currentState.save();}
                break;
                case 1://photos of product
                res=imagePickerKey.currentState.validate();
                print("photos validate $res");
                if(res){
                  photos=imagePickerKey.currentState.save();}
                break;
                case 2://location details
                res=locationKey.currentState.validate();
                if(res){
                  widget.location=locationKey.currentState.save();}
                break;
                
                default:

              }
              if(res&&currentStep<2){
                
                setState(() {
                   currentStep++;
                                });
              }else if(currentStep>=2&&res){
                
                loading(true);
                showLoadingDialog(true, context,msg: "uploading...");
                //todo remove loop after testing
                
                submit().then((res){
                  showLoadingDialog(false, context,msg: "uploading...");
                  loading(false);
                }).catchError((e){
                  showLoadingDialog(false, context,msg: "uploading...");
                  loading(false);
                });
                
              }
            }
  void onStepCancel(){
              if(currentStep>0)
              {
              setState(() {
                 currentStep--;
                            });
                            }
            }
  void onStepTapped(int i){
    setState(() {
                 currentStep=i;
            });}
 Future<dynamic> uploadImage(File image){
    final String uuid = Uuid().v1();
     final StorageReference ref = storage.ref().child('products').child("photos").child("$uuid.png");
    final StorageUploadTask uploadTask = ref.putFile(
      image,
      StorageMetadata(
        contentLanguage: 'en',
        customMetadata: <String, String>{'activity': 'test'},
      ),
    );
    //setState(() {
     // _tasks.add(uploadTask);
   // });
  return uploadTask.onComplete.then((snapshot){
     return ref.getDownloadURL();
   });
  }
  
  Future<List> submit(){
    var user= getUser();
    print(widget.product.toJSON());
    print(widget.location.toJSON());
    print(widget.category.toJSON());
    /*return Future.wait(photos.map((item)=>uploadImage(item))).then((images){
      //print(images);  
      widget.product.addFields({"photos":images});
      return user.sell_product(product:widget.product);
      })
      .then((res){
     return widget.product.locatedAt(location:widget.location);
    }).then((res){
      return widget.product.belongTo(category: widget.category);
    }).catchError((e){
      print(e);

    });*/
   
  }
  
  
  @override
  Widget build(BuildContext context) {

    var theme=Theme.of(context);
     final info= Step(
       title:Text("product details",style: TextStyle(color: currentStep==0?theme.primaryColor:theme.textTheme.subhead.color),),
     content:Form(
        key: formKey,
        child:Column(
          children: <Widget>[
            TextFormField(
             // autovalidate: true,
              initialValue: widget.product.getField("name"),
              onSaved: (String name){
                widget.product.addFields({"name":name});
              },
              validator: (String name){if(isNull(name)){
                return "name cant be empty";}
                else if(!isLength(name, 3,80)){
                  return "item name must be 3 and 80 charaters long";

                }
                },
              decoration: InputDecoration(
                  icon: Icon(Icons.title),
                      labelText: "item name",
              ),),
            TextFormField(
              keyboardType: TextInputType.numberWithOptions(signed: false,decimal: true),

             // autovalidate: true,
              initialValue: toString(widget.product.getField("quantity")),
              onSaved: (String quantity){
                widget.product.addFields({"quantity":quantity});
              },
              validator: (String quantity){
                if(isNull(quantity)){
                return "quantity cant be empty";}
                else if(!isNumeric(quantity)){
                  return "quantity must be a numeric value";
                }},
              decoration: InputDecoration(
                icon: Icon(Icons.show_chart),
                labelText: "quantity",
                suffix: DropdownButton(
                  //iconSize: 38.0,
                 value: widget.product.getField("unit")??"PCS",
                  elevation: 8,
                  onChanged: (val){
                    setState(() {
                              widget.product.addFields({"unit":val});            
                                        });
                    
                  },
                  items: <DropdownMenuItem<String>>[
                    DropdownMenuItem(
                      value: "PCS",
                      child: Text("PCS"),
                    ),
                     DropdownMenuItem(
                      value: "PAIRS",
                      child: Text("PAIRS")),
                       DropdownMenuItem(
                      value: "KG",
                      child: Text("KG"),),
                       DropdownMenuItem(
                      value: "LTR",
                      child: Text("LTR"),)
                  ],
                )
              ),),
            TextFormField(
              keyboardType: TextInputType.numberWithOptions(signed: false,decimal: true),
             // autovalidate: true,
              initialValue: toString(widget.product.getField("price")),
              onSaved: (String price){
                widget.product.addFields({"price":price});
              },
              validator: (String price){
                if(isNull(price))
                {return "price cant be empty";
                }
                 if((!isNumeric(price))&&(!isFloat(price))){return "invalid price value";}
                },
              decoration: InputDecoration(
                icon: Icon(Icons.payment),
                labelText: "price",
                suffixText:"TZS"
              ),),

               StoreConnector<AppState,List<OCategory>>(
                converter:(store)=>store.state.categories,
                builder:(context,categories)=>DropdownButtonFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.category)
                  ),
                hint: Text("Category"),
                value: categoryValue??categories.first,
                validator: (value){
                  if(value==null){
                    return "Category cant be empty";
                  }
                },
                onSaved: (value){
                  widget.category=value;
                },
                onChanged: (value){                  
                  setState(() {                    
                    categoryValue=value;
                                    });
                },
                items: categories.map<DropdownMenuItem<OCategory>>((item)=>
                DropdownMenuItem<OCategory>(
                  value: item,
                  child: Text("${item.getField("categoryName")}"),
                  )).toList()                
              ),
              ),
            TextFormField(
              maxLines: 5,
              maxLength: 250,
              maxLengthEnforced: true,
              
              
             // autovalidate: true,
              initialValue: widget.product.getField("description"),
              onSaved: (String description){
                widget.product.addFields({"description":description});
              },
              validator: (String description){
                if(isNull(description)){return "description cant be empty";}
                else if(!isLength(description, 25,250)){
                  return "Description must be 25 and 250 characters long";
                }
                },
              decoration: InputDecoration(
                icon: Icon(Icons.description),
                labelText: "description",
              ),),
              
             
          
          ]
    )
    )
    );
    
  var locationInfo=Step(
    title:Text("Product location",style: TextStyle(color: currentStep==2?theme.primaryColor:theme.textTheme.subhead.color)),
    content:LocationForm(key:locationKey,location:widget.location)
    );
    
    var photoPicker=Step(
      title: Text("Product photos",style: TextStyle(color: currentStep==1?theme.primaryColor:theme.textTheme.subhead.color)),
      content: ImagePickerForm(key:imagePickerKey,maxCount: 4,minCount: 1,format: "png",)
    );
    
      return  SingleChildScrollView(  
        padding: EdgeInsets.symmetric(vertical:16.0),      
        child:Container(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height-50.0),
          child:Stepper(
            type: StepperType.vertical,
            onStepContinue: onStepContinue,
            onStepCancel: onStepCancel,
            //onStepTapped: onStepTapped,
            currentStep: currentStep,
            controlsBuilder: (BuildContext context, 
            {VoidCallback onStepContinue, VoidCallback onStepCancel}) 
            {
              //var th=Theme.of(context).primaryTextTheme;
                return Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child:Row(
                  children: <Widget>[
                    FlatButton(
                      onPressed: onStepContinue,
                      child: currentStep<2?Text('Next'):Text("Save"),
                    ),
                    Flexible(child: Container(),),
                    currentStep>0?FlatButton(
                      onPressed: onStepCancel,
                      child:  Text('Back'),
                    ):Container()])
                    );
           },
            steps: <Step>[
            info,
            photoPicker,
            locationInfo
          ],)
        )
      );
    }
}
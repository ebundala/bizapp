import 'package:flutter/material.dart';
import 'package:validators/validators.dart';
import 'package:bizapp/src/components/common/BizappBaseWidget.dart';
import 'package:bizapp/src/bizapp/controllers/OLocation.dart';
import 'package:geolocator/geolocator.dart';

class LocationForm extends StatefulWidget{
  OLocation location;
  //Position position;
  LocationForm({Key key,this.location}):super(key:key);
  @override
  State<StatefulWidget> createState() =>LocationFormState();
}

class LocationFormState extends BizappState<LocationForm> {
  final locationKey=GlobalKey<FormState>();
  get form{
   return locationKey;
  }
  get location{
    return widget.location;
  }
  bool validate(){
    return locationKey.currentState.validate();
  }
  OLocation save([OLocation loc]){
    locationKey.currentState.save();
    //loc=location;
    return location;
  }
  handlePermissionCheck(status){

    if (status == GeolocationStatus.denied) {

        }
        // Take user to permission settings
      else if (status == GeolocationStatus.disabled) {

      }
        // Take user to location page
      else if (status == GeolocationStatus.restricted) {

      }
        // Restricted
      else if (status == GeolocationStatus.unknown) {

      }
        // Unknown
      else if (status == GeolocationStatus.granted) {

      }
  }
  Future<Position> getLastKnownPosition(){
    return Geolocator().getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
  }
  Future<List<Placemark>>reverseGeocode(address){
    return Geolocator().placemarkFromAddress(address);
  }
  Future<List<Placemark>> geocode(Position pos){
    if(pos!=null)
   return Geolocator().placemarkFromCoordinates(pos.latitude, pos.longitude);
  }
  Future<GeolocationStatus> getGeoLocationPermission() {
     return Geolocator().checkGeolocationPermissionStatus();
  }
  Future<Position> getGeoLocation(){
    return Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }
  Map placemarkToLocation(List<Placemark> place){
    if(place!=null){
      return{
          "country":place.first.country,
          "region":place.first.administrativeArea,
          "district":place.first.subAdministratieArea,
          "ward":place.first.locality,
          "street":place.first.subLocality,
         // "lat":place.first.position.latitude,
          //"lon":place.first.position.longitude,
         // "alt":place.first.position.altitude,
        };
    }
    return null;
  }
  @override
  Widget build(BuildContext context){
    return FutureBuilder(
     future: getLastKnownPosition().then((pos)async{
       if(widget.location.rid==null){
        var place=await geocode(pos);
        if(place!=null){

        var loc=placemarkToLocation(place);
        loc["lat"]=pos.latitude;
        loc["lon"]=pos.longitude;
        loc["alt"]=pos.altitude;
        widget.location.addFields(loc);
        }
       }
       

     }),
     builder: (context,snapshot){
       if(snapshot.connectionState==ConnectionState.done){
        return Form(
      key: locationKey, 
      child:
        Column(children:
         <Widget>[
              TextFormField(
             // autovalidate: true,
              initialValue: widget.location.getField("region"),
              onSaved: (String region){
                widget.location.addFields({"region":region});
              },
              validator: (String region){if(isNull(region)){return "Region cant be empty";}},
              decoration: InputDecoration(
                icon: Icon(Icons.location_city),
                labelText: "Region",
               
              ),),
              TextFormField(
            //  autovalidate: true,
              initialValue: widget.location.getField("district"),
              onSaved: (String district){
                widget.location.addFields({"district":district});
              },
              validator: (String district){if(isNull(district)){return "District cant be empty";}},
              decoration: InputDecoration(
                icon: Icon(Icons.location_city),
                labelText: "District",
              ),),
              TextFormField(
             // autovalidate: true,
              initialValue: widget.location.getField("ward"),
              onSaved: (String ward){
                widget.location.addFields({"ward":ward});
              },
              validator: (String ward){if(isNull(ward)){return "Ward cant be empty";}},
              decoration: InputDecoration(
                icon: Icon(Icons.location_city),
                labelText: "Ward",
              ),),
              TextFormField(
             // autovalidate: true,
              initialValue: widget.location.getField("street"),
              onSaved: (String street){
                widget.location.addFields({"street":street});
              },
              validator: (String street){if(isNull(street)){return "Street cant be empty";}},
              decoration: InputDecoration(
                icon: Icon(Icons.location_searching),
                labelText: "street",
              ),),
              RaisedButton.icon(
                icon: Icon(Icons.my_location,),
                label: Text("Not my location"),
                onPressed: ()async{
                  showLoadingDialog(true, context,msg: "getting location...");
                getGeoLocation().then((pos)async{
                  print(pos);
                  var place=await geocode(pos);
                  var loc=placemarkToLocation(place);
                  loc["lat"]=pos.latitude;
                  loc["lon"]=pos.longitude;
                  loc["alt"]=pos.altitude;
                  showLoadingDialog(false, context);
                  setState(() {
                                
                               widget.location.addFields(loc);
                               print(widget.location.toJSON());

                            });
                            }).catchError((e){
                              showLoadingDialog(false, context);
                              showSnackbar(content: Text("${e.message}"),
                              action: SnackBarAction(label: "OK",onPressed: (){
                                print("snackbar action clicked");
                              },));
                            });
                },
                ),
                Row(children: <Widget>[
                  RaisedButton(
                    onPressed: ()async{
                      var lat=widget.location.getField("lat");
                      var lon=widget.location.getField("lon");
                      var tolat=widget.location.getField("lat");
                      var tolon=widget.location.getField("lon");
                  var loc= await Navigator.of(context).pushNamed("map/$lat/$lon/$tolat/$tolon");
                    },
                    child: Text("Map"),
                  )
                  //Text("lat:${widget.location.getField("lat")} lon:${widget.location.getField("lon")} alt:${widget.location.getField("alt")}")
                ],
                )            
          ],
        )); 
      }
      else{
        return Column(
          children: <Widget>[
            Text("Getting your location...pls wait"),
            SizedBox(height: 16.0,),
            LinearProgressIndicator()
          ],
        ); 
      }
    }
   );
     
   
  }
}
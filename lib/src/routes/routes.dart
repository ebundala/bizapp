import 'package:fluro/fluro.dart';
import 'routeHandlers.dart';

class Routes {
  static String chats = "/chats";
  static String chat = "/chat/:chatId";
  static String settings = "/settings";
  static String help = "/help";
  static String credits = "/credits";
  static String product = "/product/:productId";
  static String category = "/category/:categoryId";
  static String account = "/account";
  static String login = "/login/:screenName";
  static String create = "/create";
  static String edit = "/edit/:productId";
  static String map = "/map/:lat/:lon/:to_lat/:to_lon";
  static String posts = "/posts/:categoryId";
  static String post = "/post/:postId";
  static String editpost = "/editpost/:postId";
  static String shoppingCart = "cart/:view";
  static String home = "/home";

  static void defineRoutes(Router router) {
    router.define(chats, handler: chatsHandler);
    router.define(chat, handler: messagesHandler);
    router.define(settings, handler: settingsHandler);
    router.define(help, handler: helpHandler);
    router.define(credits, handler: creditsHandler);
    router.define(product, handler: productHandler);
    router.define(category, handler: categoryHandler);
    router.define(account, handler: accountHandler);
    router.define(login, handler: loginHandler);
    router.define(create, handler: createHandler);
    router.define(edit, handler: editHandler);
    router.define(shoppingCart, handler: shoppingCartHandler);
    router.define(home, handler: homeHandler);
     router.define(map, handler: mapHandler);
  }
}

import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:bizapp/src/components/screens/ChatScreen.dart';
import 'package:bizapp/src/components/screens/ConversationsScreen.dart';
import 'package:bizapp/src/components/screens/HelpScreen.dart';
import 'package:bizapp/src/components/screens/CreateProductScreen.dart';
import 'package:bizapp/src/components/screens/SettingScreen.dart';
import 'package:bizapp/src/components/screens/AccountScreen.dart';
import 'package:bizapp/src/components/screens/ProductListScreen.dart';
import 'package:bizapp/src/components/screens/SingleProductScreen.dart';
import 'package:bizapp/src/components/screens/CreditsScreen.dart';
import 'package:bizapp/src/components/screens/LoginScreen.dart';
import 'package:bizapp/src/components//screens/SignUpScreen.dart';
import 'package:bizapp/src/components/screens/WelcomeScreen.dart';
import 'package:bizapp/src/components/screens/PasswordRessetScreen.dart';
import 'package:bizapp/src/components/screens/TermsOfServiceScreen.dart';
//import 'package:bizapp/src/components/screens/SplashScreen.dart';
import 'package:bizapp/src/components/screens/ShoppingCartScreen.dart';
import 'package:bizapp/src/components/screens/HomeScreen.dart';
import 'package:bizapp/src/components/screens/MapScreen.dart';

var chatsHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new ChatsListScreen();
});

var messagesHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  var id = params['chatId'][0];
  return new ChatScreen(
    chatId: id,
  );
});

var settingsHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new SettingsScreen(
    title: "App preferences",
  );
});

var helpHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new HelpScreen(title: "App Help");
});

var creditsHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new CreditsScreen(title: "App credits");
});

var productHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new SingleProductScreen(
    title: "A single product",
  );
});

var accountHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  // return new AccountScreen(title: "User Account");
  var fr = new Friend(
      avatar: "somepic.jpg",
      email: "some@email.com",
      location: "some location",
      name: "some name");
  return new FriendDetailsPage(friend: fr, avatarTag: "logo");
});

var loginHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  var page = params["screenName"];
  // print(page[0]);
  switch (page[0]) {
    case "signin":
      return new LoginScreen(
        title: "Welcome back!",
      );
    case "signup":
      return new SignUpScreen(title: "Hello!, Sign Up:");
    case "reset":
      return new PasswordResetScreen(title: "Enter email to reset password");
    case "terms":
      return new TermsOfService(title: "Terms of services");
    case "welcome":
    default:
      return new WelcomeScreen(title: "Sell anything. Try Bizapp");
  }
});

var createHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new CreateProductsScreen(
    title: "Create new Product",
  );
});
var editHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new CreateProductsScreen(
    title: "Edit product",
  );
});
var categoryHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new ProductsListScreen(
    title: "All products",
  );
});

var shoppingCartHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new ShoppingCartScreen();
});

var homeHandler = new Handler(
    handlerFunc: (BuildContext context, Map<String, dynamic> params) {
  return new HomeScreen(title: "Home");
});

var mapHandler= new Handler(
  handlerFunc: (BuildContext context,Map<String,List<String>> params){
   double lat= double.tryParse(params['lat'][0]);
   double lon= double.tryParse(params['lon'][0]);
   double tolat= double.tryParse(params['to_lat'][0]);
   double tolon= double.tryParse(params['to_lon'][0]);
   
    return MapScreen(lat:lat,lon:lon,toLat: tolat,toLon: tolon ,);
  }
);